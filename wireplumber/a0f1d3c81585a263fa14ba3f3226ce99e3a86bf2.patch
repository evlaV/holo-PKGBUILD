From a0f1d3c81585a263fa14ba3f3226ce99e3a86bf2 Mon Sep 17 00:00:00 2001
From: Julian Bouzas <julian.bouzas@collabora.com>
Date: Mon, 31 Jan 2022 13:48:44 -0500
Subject: [PATCH] HACK: hide echo-cancel nodes from default nodes

Since echo-cancel-sink and echo-cancel-source virtual nodes appear as device
nodes in the pulse layer, it is not possible to know what real devices they are
using. This HACK hides the echo cancel nodes so pulseaudio applications don't
need to change their behaviour if echo cancel module is enabled.

This hack won't be needed once applications use the pipewire API to check what
real device the echo cancel virtual nodes are using.
---
 modules/module-default-nodes.c               |   4 +-
 modules/module-default-nodes/common.h        |   6 +-
 src/config/main.lua.d/40-device-defaults.lua |   3 +
 src/scripts/expose-default-nodes.lua         | 157 +++++++++++++++++++
 src/scripts/policy-node.lua                  |  36 +++--
 5 files changed, 192 insertions(+), 14 deletions(-)
 create mode 100644 src/scripts/expose-default-nodes.lua

diff --git a/modules/module-default-nodes.c b/modules/module-default-nodes.c
index 3c3abc0d..80ff95ac 100644
--- a/modules/module-default-nodes.c
+++ b/modules/module-default-nodes.c
@@ -238,10 +238,10 @@ find_best_media_class_node (WpDefaultNodes * self, const gchar *media_class,
         continue;
 
       if (self->auto_echo_cancel && is_echo_cancel_node (self, node, direction))
-        prio += 10000;
+        prio += 20000;
 
       if (name && node_name && g_strcmp0 (name, node_name) == 0)
-        prio += 20000;
+        prio += 10000;
 
       if (prio > highest_prio || res == NULL) {
         highest_prio = prio;
diff --git a/modules/module-default-nodes/common.h b/modules/module-default-nodes/common.h
index d9338326..63ae3e1b 100644
--- a/modules/module-default-nodes/common.h
+++ b/modules/module-default-nodes/common.h
@@ -18,9 +18,9 @@ enum {
 };
 
 static const gchar * DEFAULT_KEY[N_DEFAULT_NODES] = {
-  [AUDIO_SINK] = "default.audio.sink",
-  [AUDIO_SOURCE] = "default.audio.source",
-  [VIDEO_SOURCE] = "default.video.source",
+  [AUDIO_SINK] = "wireplumber.default.audio.sink",
+  [AUDIO_SOURCE] = "wireplumber.default.audio.source",
+  [VIDEO_SOURCE] = "wireplumber.default.video.source",
 };
 
 static const gchar * NODE_TYPE_STR[N_DEFAULT_NODES] = {
diff --git a/src/config/main.lua.d/40-device-defaults.lua b/src/config/main.lua.d/40-device-defaults.lua
index 23438a9e..549f5658 100644
--- a/src/config/main.lua.d/40-device-defaults.lua
+++ b/src/config/main.lua.d/40-device-defaults.lua
@@ -23,6 +23,9 @@ function device_defaults.enable()
   -- Selects appropriate default nodes and enables saving and restoring them
   load_module("default-nodes", device_defaults.properties)
 
+  -- Expose the actual default nodes to applications
+  load_script("expose-default-nodes.lua", device_defaults.properties)
+
   -- Selects appropriate default routes ("ports" in pulseaudio terminology)
   -- and enables saving and restoring them together with
   -- their properties (per-route/port volume levels, channel maps, etc)
diff --git a/src/scripts/expose-default-nodes.lua b/src/scripts/expose-default-nodes.lua
new file mode 100644
index 00000000..747ac6e6
--- /dev/null
+++ b/src/scripts/expose-default-nodes.lua
@@ -0,0 +1,157 @@
+-- WirePlumber
+--
+-- Copyright © 2020 Collabora Ltd.
+--    @author Julian Bouzas <julian.bouzas@collabora.com>
+--
+-- SPDX-License-Identifier: MIT
+
+local self = {}
+self.config = ... or {}
+self.scanning = false
+self.pending_rescan = false
+self.default_nodes = Plugin.find("default-nodes-api")
+self.metadata_om = ObjectManager {
+  Interest {
+    type = "metadata",
+    Constraint { "metadata.name", "=", "default" },
+  }
+}
+self.linkables_om = ObjectManager {
+  Interest {
+    type = "SiLinkable",
+  }
+}
+self.links_om = ObjectManager {
+  Interest {
+    type = "SiLink",
+    Constraint { "is.policy.item.link", "=", true },
+  }
+}
+self.devices_om = ObjectManager { Interest { type = "device" } }
+
+function rescan()
+  for si in self.linkables_om:iterate() do
+    handleLinkable (si)
+  end
+end
+
+function scheduleRescan ()
+  if self.scanning then
+    self.pending_rescan = true
+    return
+  end
+
+  self.scanning = true
+  rescan ()
+  self.scanning = false
+
+  if self.pending_rescan then
+    self.pending_rescan = false
+    Core.sync(function ()
+      scheduleRescan ()
+    end)
+  end
+end
+
+function getSiTarget (node_name)
+  local si = self.linkables_om:lookup {
+      Constraint { "node.name", "=", node_name } }
+  if si == nil then
+    return nil
+  end
+
+  for silink in self.links_om:iterate() do
+    local out_id = tonumber (silink.properties["out.item.id"])
+    local in_id = tonumber (silink.properties["in.item.id"])
+    local si_target = nil
+    if out_id == si.id then
+       si_target = self.linkables_om:lookup {
+          Constraint { "id", "=", in_id, type = "gobject" } }
+    elseif in_id == si.id then
+      si_target = self.linkables_om:lookup {
+          Constraint { "id", "=", out_id, type = "gobject" } }
+    end
+    if si_target ~= nil and
+        si_target.properties["item.node.type"] ~= "stream" then
+      return si_target
+    end
+  end
+  return nil
+end
+
+function handleDefaultNode (si, id, name, media_class)
+  local metadata = self.metadata_om:lookup()
+  if metadata == nil then
+    return
+  end
+
+  local def_name = name
+  if media_class == "Audio/Sink" then
+    if name == "echo-cancel-sink" then
+      local si_target = getSiTarget ("echo-cancel-playback")
+      if si_target == nil then
+        return
+      end
+      local target_node = si_target:get_associated_proxy ("node")
+      def_name = target_node.properties["node.name"]
+      Log.info ("Echo cancel playback target " .. def_name)
+    end
+    Log.info ("Setting default.audio.sink to " .. def_name)
+    metadata:set(0, "default.audio.sink", "Spa:String:JSON",
+        "{ \"name\": \"" .. def_name .. "\" }")
+  elseif media_class == "Audio/Source" then
+    if name == "echo-cancel-source" then
+      local si_target = getSiTarget ("echo-cancel-capture")
+      if si_target == nil then
+        return
+      end
+      local target_node = si_target:get_associated_proxy ("node")
+      def_name = target_node.properties["node.name"]
+      Log.info ("Echo cancel capture target " .. def_name)
+    end
+    Log.info ("Setting default.audio.source to " .. def_name)
+    metadata:set(0, "default.audio.source", "Spa:String:JSON",
+        "{ \"name\": \"" .. def_name .. "\" }")
+  elseif media_class == "Video/Source" then
+    Log.info ("Setting default.video.source to " .. def_name)
+    metadata:set(0, "default.video.source", "Spa:String:JSON",
+        "{ \"name\": \"" .. def_name .. "\" }")
+  end
+end
+
+function handleLinkable (si)
+  local node = si:get_associated_proxy ("node")
+  if node == nil then
+    return
+  end
+
+  local node_id = node["bound-id"]
+  local media_classes = { "Audio/Sink", "Audio/Source", "Video/Source" }
+  for _, media_class in ipairs(media_classes) do
+    local def_id = self.default_nodes:call("get-default-node", media_class)
+    if def_id ~= Id.INVALID and def_id == node_id then
+      local node_name = node.properties["node.name"]
+      handleDefaultNode (si, node_id, node_name, media_class)
+      break
+    end
+  end
+end
+
+self.linkables_om:connect("objects-changed", function (om, si)
+  scheduleRescan ()
+end)
+
+self.links_om:connect("objects-changed", function (om, si)
+  scheduleRescan ()
+end)
+
+self.devices_om:connect("object-added", function (om, device)
+  device:connect("params-changed", function (d, param_name)
+    scheduleRescan ()
+  end)
+end)
+
+self.metadata_om:activate()
+self.linkables_om:activate()
+self.links_om:activate()
+self.devices_om:activate()
diff --git a/src/scripts/policy-node.lua b/src/scripts/policy-node.lua
index 65d8185a..15a3132b 100644
--- a/src/scripts/policy-node.lua
+++ b/src/scripts/policy-node.lua
@@ -256,6 +256,14 @@ function getDefaultNode(properties, target_direction)
   return default_nodes:call("get-default-node", target_media_class)
 end
 
+function getDefaultConfiguredNodeName(properties, target_direction)
+  local target_media_class =
+        properties["media.type"] ..
+        (target_direction == "input" and "/Sink" or "/Source")
+  return default_nodes:call("get-default-configured-node-name",
+      target_media_class)
+end
+
 -- Try to locate a valid target node that was explicitly requsted by the
 -- client(node.target) or by the user(target.node)
 -- Use the target.node metadata, if config.move is enabled,
@@ -405,6 +413,12 @@ function findBestLinkable (si)
   local target_priority = 0
   local target_plugged = 0
 
+  -- get default configured node name
+  local def_conf_node_name = nil
+  if default_nodes ~= nil then
+    def_conf_node_name = getDefaultConfiguredNodeName(si_props, target_direction)
+  end
+
   for si_target in linkables_om:iterate {
     Constraint { "item.node.type", "=", "device" },
     Constraint { "item.node.direction", "=", target_direction },
@@ -418,6 +432,13 @@ function findBestLinkable (si)
         tostring(si_target_props["node.name"]),
         tostring(si_target_node_id)))
 
+    -- increase priority if default configured node name matches
+    local target_node = si_target:get_associated_proxy ("node")
+    if def_conf_node_name ~= nil and
+        def_conf_node_name == target_node.properties["node.name"] then
+      priority = priority + 10000
+    end
+
     if not canLink (si_props, si_target) then
       Log.debug("... cannot link, skip linkable")
       goto skip_linkable
@@ -428,6 +449,8 @@ function findBestLinkable (si)
       goto skip_linkable
     end
 
+
+
     -- todo:check if this linkable(node/device) have valid routes.
 
     -- Is passthrough feasible between these linkables(nodes)?
@@ -751,16 +774,11 @@ if config.follow and default_nodes ~= nil then
   end)
 end
 
--- listen for target.node metadata changes if config.move is enabled
-if config.move then
-  metadata_om:connect("object-added", function (om, metadata)
-    metadata:connect("changed", function (m, subject, key, t, value)
-      if key == "target.node" then
-        scheduleRescan ()
-      end
-    end)
+metadata_om:connect("object-added", function (om, metadata)
+  metadata:connect("changed", function (m, subject, key, t, value)
+    scheduleRescan ()
   end)
-end
+end)
 
 linkables_om:connect("object-added", function (om, si)
   if si.properties["item.node.type"] ~= "stream" then
-- 
GitLab

