# GRUB boot loader configuration

GRUB_DEFAULT=0
GRUB_TIMEOUT=0
GRUB_DISTRIBUTOR="SteamOS"
# SteamOS:
# plymouth tweaks
# - add explicit splash
# - plymouth.ignore-serial-consoles otherwise plymouth will never trigger
GRUB_CMDLINE_LINUX_DEFAULT="loglevel=3 quiet splash plymouth.ignore-serial-consoles"
GRUB_CMDLINE_LINUX="console=tty1 rd.luks=0 rd.lvm=0 rd.md=0 rd.dm=0 rd.systemd.gpt_auto=no"

# Uncomment to disable the use of the chainloader environment to boot linux
# normally
#GRUB_DISABLE_STEAMCL_ENVIRONMENT=true

# Preload both GPT and MBR modules so that they are not missed
GRUB_PRELOAD_MODULES="part_gpt part_msdos"

# Uncomment to enable booting from LUKS encrypted devices
#GRUB_ENABLE_CRYPTODISK=y

# Set to 'countdown' or 'hidden' to change timeout behavior,
# press ESC key to display menu.
GRUB_TIMEOUT_STYLE=menu

# Uncomment to use basic console
GRUB_TERMINAL_INPUT=console

# Uncomment to disable graphical terminal
#GRUB_TERMINAL_OUTPUT=console

# The resolution used on graphical terminal
# note that you can use only modes which your graphic card supports via VBE
# you can see them in real GRUB with the command `vbeinfo'
# >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> * <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
# NOTE: on the deck the only mode which is the right way round is 1280x800
#
# However several other considerations also apply:
#  - cannot set GFXMODE to 'auto' if we want to display a menu
#    - this is because the mode prepared by the firmware isn't quite to spec,
#      so when grub tries to _un_set it before handing over to the kernel
#      we can get a segfault/crash/boot-failure
#  - cannot unconditionally set the mode to 1280x800 if we _don't_ want a menu
#    - as soon as we touch the graphics mode we break seamless boot
#  - the kernel rotates its framebuffer output, so we need to select 800x1280
#    if we want console output during boot
#
# Solution:
#  - set the mode to 'auto'/'keep' (the default)
#  - override it in the steamenv module _if_ we require the menu
#
# This needs a tweak to the 00_header grub config fragment generator
# as that is where the graphics mode is initialised.
# >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> * <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
GRUB_GFXMODE=auto

# We have our kernel set up to rotate the framebuffer so we need to
# pick the 'sideways' orientation if GRUB_GFXMODE was not 800x1280:
GRUB_GFXPAYLOAD_LINUX=keep

# Uncomment if you want GRUB to pass to the Linux kernel the old parameter
# format "root=/dev/xxx" instead of "root=/dev/disk/by-uuid/xxx"
#GRUB_DISABLE_LINUX_UUID=true

# Uncomment to disable generation of recovery mode menu entries
GRUB_DISABLE_RECOVERY=true

# Uncomment and set to the desired menu colors.  Used by normal and wallpaper
# modes only.  Entries specified as foreground/background.
# SteamOS: TODO: Add comments/reference
# Do we need this even if there's a theme?
GRUB_COLOR_NORMAL="light-gray/black"
GRUB_COLOR_HIGHLIGHT="light-magenta/black"

# Uncomment one of them for the gfx desired, a image background or a gfxtheme
#GRUB_BACKGROUND="/path/to/wallpaper"
GRUB_THEME="/usr/share/grub/themes/breeze/theme.txt"

# Uncomment to get a beep at GRUB start
#GRUB_INIT_TUNE="480 440 1"

# Uncomment to make GRUB remember the last selection. This requires
# setting 'GRUB_DEFAULT=saved' above.
#GRUB_SAVEDEFAULT=true

# Uncomment to disable submenus in boot menu
#GRUB_DISABLE_SUBMENU=y

# Probing for other operating systems is disabled for security reasons. Read
# documentation on GRUB_DISABLE_OS_PROBER, if still want to enable this
# functionality install os-prober and uncomment to detect and include other
# operating systems.
#GRUB_DISABLE_OS_PROBER=false
