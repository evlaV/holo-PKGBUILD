From 3f63eb938fba3d96527e82e44cfc3fe6612a256c Mon Sep 17 00:00:00 2001
From: Friedrich Vock <friedrich.vock@gmx.de>
Date: Sun, 26 Jan 2025 13:29:39 +0100
Subject: [PATCH 1/2] Skip processing IBs if stack is exhausted

Otherwise we start indexing OOB and stomping random other things.
---
 src/app/gui/commands.c      |  3 ++-
 src/app/gui/rings_panel.cpp |  3 ++-
 src/app/ring_stream_read.c  | 33 ++++++++++++++++++++++++++-------
 src/umr.h                   |  3 ++-
 4 files changed, 32 insertions(+), 10 deletions(-)

diff --git a/src/app/gui/commands.c b/src/app/gui/commands.c
index 9477eee..c2b9d7a 100644
--- a/src/app/gui/commands.c
+++ b/src/app/gui/commands.c
@@ -1537,8 +1537,9 @@ static void _ring_done(struct ring_decoding_data *data) {
 #pragma GCC diagnostic ignored "-Wunused-parameter"
 
 
-static void ring_start_ib(struct umr_stream_decode_ui *ui, uint64_t ib_addr, uint32_t ib_vmid, uint64_t from_addr, uint32_t from_vmid, uint32_t size, int type) {
+static int ring_start_ib(struct umr_stream_decode_ui *ui, uint64_t ib_addr, uint32_t ib_vmid, uint64_t from_addr, uint32_t from_vmid, uint32_t size, int type) {
 	_ring_start_ib((struct ring_decoding_data*) ui->data, ib_addr, ib_vmid);
+	return 0;
 }
 
 static void ring_start_opcode(struct umr_stream_decode_ui *ui, uint64_t ib_addr, uint32_t ib_vmid, int pkttype, uint32_t opcode, uint32_t subop, uint32_t nwords, const char *opcode_name, uint32_t header, const uint32_t* raw_data) {
diff --git a/src/app/gui/rings_panel.cpp b/src/app/gui/rings_panel.cpp
index 8ccec7b..8258649 100644
--- a/src/app/gui/rings_panel.cpp
+++ b/src/app/gui/rings_panel.cpp
@@ -33,8 +33,9 @@ static bool get_ring_name(void *data, int idx, const char **out) {
 	return false;
 }
 
-static void _start_ib(struct umr_stream_decode_ui *, uint64_t, uint32_t, uint64_t, uint32_t, uint32_t, int) {
+static int _start_ib(struct umr_stream_decode_ui *, uint64_t, uint32_t, uint64_t, uint32_t, uint32_t, int) {
 	/* TODO: mouse over link? */
+	return -1;
 }
 
 static void _add_shader(struct umr_stream_decode_ui *ui, struct umr_asic *asic, uint64_t ib_addr, uint32_t ib_vmid, struct umr_shaders_pgm *shader) {
diff --git a/src/app/ring_stream_read.c b/src/app/ring_stream_read.c
index 9447e8e..820675c 100644
--- a/src/app/ring_stream_read.c
+++ b/src/app/ring_stream_read.c
@@ -30,32 +30,42 @@
  * tedious and a waste of time.
  */
 
+#define RING_STREAM_STACK_SIZE 32
+
 struct ui_data {
 	struct {
 		uint64_t ib_addr, f_addr, b_addr;
 		const uint32_t *rawdata;
 		uint32_t off;
 		FILE *f;
-	} stack[32];
+	} stack[RING_STREAM_STACK_SIZE];
 	int sp, no;
 	struct umr_asic *asic;
 };
 
-static void next_level(struct umr_stream_decode_ui *ui)
+static int next_level(struct umr_stream_decode_ui *ui)
 {
 	struct ui_data *data = ui->data;
+	if (data->sp >= RING_STREAM_STACK_SIZE)
+		return -1;
+
 	char tmpname[64];
 	sprintf(tmpname, "/tmp/umr_ring_out.%d", (data->no)++);
 	++(data->sp);
 	data->stack[data->sp].f = fopen(tmpname, "w");
+	return 0;
 }
 
-static void start_ib(struct umr_stream_decode_ui *ui, uint64_t ib_addr, uint32_t ib_vmid, uint64_t from_addr, uint32_t from_vmid, uint32_t size, int type)
+static int start_ib(struct umr_stream_decode_ui *ui, uint64_t ib_addr, uint32_t ib_vmid, uint64_t from_addr, uint32_t from_vmid, uint32_t size, int type)
 {
 	struct ui_data *data = ui->data;
 	struct umr_asic *asic = data->asic;
+	int res;
+
+	res = next_level(ui);
+	if (res)
+		return res;
 
-	next_level(ui);
 	data->stack[data->sp].ib_addr = ib_addr;
 	fprintf(data->stack[data->sp].f, "Decoding IB at %s%lu%s@%s0x%"PRIx64"%s from %s%lu%s@%s0x%"PRIx64"%s of %s%lu%s words (type %s%d%s)",
 	BLUE, (unsigned long)ib_vmid, RST,
@@ -64,6 +74,8 @@ static void start_ib(struct umr_stream_decode_ui *ui, uint64_t ib_addr, uint32_t
 	YELLOW, from_addr, RST,
 	BLUE, (unsigned long)size, RST,
 	BLUE, type, RST);
+
+	return 0;
 }
 
 static void start_opcode(struct umr_stream_decode_ui *ui, uint64_t ib_addr, uint32_t ib_vmid, int pkttype, uint32_t opcode, uint32_t subop, uint32_t nwords, const char *opcode_name, uint32_t header, const uint32_t* raw_data)
@@ -182,9 +194,12 @@ static void add_shader(struct umr_stream_decode_ui *ui, struct umr_asic *asic, u
 {
 	struct ui_data *data = ui->data;
 	char **str;
-	int x;
+	int x, res;
+
+	res = next_level(ui);
+	if (res)
+		return;
 
-	next_level(ui);
 	fprintf(data->stack[data->sp].f, "Shader from %lu@[0x%"PRIx64" + 0x%"PRIx64"] at %lu@0x%"PRIx64", type %d, size %lu\n", (unsigned long)ib_vmid, data->stack[data->sp-1].ib_addr, ib_addr - data->stack[data->sp-1].ib_addr, (unsigned long)shader->vmid, shader->addr, shader->type, (unsigned long)shader->size);
 	umr_vm_disasm_to_str(asic, asic->options.vm_partition, shader->vmid, shader->addr, 0, shader->size, 0, &str);
 	x = 0;
@@ -202,12 +217,16 @@ static void add_data(struct umr_stream_decode_ui *ui, struct umr_asic *asic, uin
 {
 	struct ui_data *data = ui->data;
 	char **txt;
+	int res;
 
 	// don't fetch data blocks if no_follow is enabled
 	if (asic->options.no_follow_ib)
 		return;
 
-	next_level(ui);
+	res = next_level(ui);
+	if (res)
+		return;
+
 	fprintf(data->stack[data->sp].f, "Data block from %"PRIu32"@[0x%"PRIx64" + 0x%"PRIx64"] at %"PRIu32"@0x%"PRIx64", type %d, ", ib_vmid, data->stack[data->sp-1].ib_addr, ib_addr - data->stack[data->sp-1].ib_addr, buf_vmid, buf_addr, type);
 
 	if (type == UMR_DATABLOCK_MQD_VI || type == UMR_DATABLOCK_MQD_NV) {
diff --git a/src/umr.h b/src/umr.h
index 9aada59..e4ebf5a 100644
--- a/src/umr.h
+++ b/src/umr.h
@@ -1718,8 +1718,9 @@ struct umr_stream_decode_ui {
 	 * from_addr/from_vmid: Where does this reference come from?
 	 * size: size of IB in DWORDs
 	 * type: type of IB (which type of packets)
+	 * Returns 0 if IB should be decoded further, non-zero return values skip processing
 	 */
-	void (*start_ib)(struct umr_stream_decode_ui *ui, uint64_t ib_addr, uint32_t ib_vmid, uint64_t from_addr, uint32_t from_vmid, uint32_t size, int type);
+	int (*start_ib)(struct umr_stream_decode_ui *ui, uint64_t ib_addr, uint32_t ib_vmid, uint64_t from_addr, uint32_t from_vmid, uint32_t size, int type);
 
 	/** unhandled_dword -- Print out a dword that doesn't match a valid packet header
 	 * ib_addr/ib_vmid: address of dword
-- 
2.48.1

