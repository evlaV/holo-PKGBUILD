# <div align="center">![holo](images/holo-logo.png)</div>

# Table of Contents

- [SteamOS 3.x (holo) PKGBUILD (Source Code) Public Mirror 🪞 (Introduction)](#-steamos-3x-holo--pkgbuild-source-code-public-mirror-)
- [Shallow Clone Recommended for Submodules (Submodule Shallow Clone)](#shallow-clone-recommended-for-submodules-submodule-shallow-clone)
- [Clone Complete Repository (include Submodules)](#clone-complete-repository-include-submodules)
- [SteamOS 3.x (holo) PKGBUILD (holo-PKGBUILD) Rebase](#steamos-3x-holo-pkgbuild-holo-pkgbuild-rebase)
- ➕ [Additional (Included) Software/Packages](#-additional-included-softwarepackages)
- 📦 [Build/Make SteamOS 3.x (holo) Package](#-buildmake-steamos-3x-holo-package)
  - 📦 [Automatically](#-automatically)
  - 📦 [Manually](#-manually)
- [SteamOS 3.x (holo) | Steam Deck (jupiter) PKGBUILD (Source Code) Public Mirror 🪞 Portal](#-steamos-3x-holo--steam-deck-jupiter--pkgbuild-source-code-public-mirror--portal-)
- [Is Valve's Steam/SteamOS End User License Agreement (EULA) Valid or Void?](#is-valves-steamsteamos-end-user-license-agreement-eula-valid-or-void-) 👍👎
- 👍 [Valve (Official) SteamOS 3.x (holo) and Steam Deck (jupiter) Public Repositories](#-valve-official-steamos-3x-holo-and-steam-deck-jupiter-public-repositories-) 👍
- 🙅 [Packages/Repositories Valve, Collabora, and Igalia are in Violation of GNU License](#-packagesrepositories-valve-collabora-and-igalia-are-in-violation-of-gnu-license-) 👎
  - [GPL Package Research](#gpl-package-research)
  - [Automatically Deobfuscate Source Package](#automatically-deobfuscate-source-package)
  - [Manually Deobfuscate Source Package](#manually-deobfuscate-source-package)
- 😉 [GitLab Repository Private-Public How-to](#-gitlab-repository-private-public-how-to-) 😂
- 💭 [Thoughts/Opinions](#-thoughtsopinions-) 💡
- 📜 [License](#-license-) 📜

# <img align="center" alt="holo-PKGBUILD" src="images/steamos-logo.png" width="48"> SteamOS 3.x (holo) <img align="center" alt="holo-PKGBUILD" src="images/steamos-logo-flipped.png" width="48"> PKGBUILD (Source Code) Public Mirror 🪞

<div align="center">

***[These humble public repositories (@gitlab.com/evlaV)](https://gitlab.com/users/evlaV/projects) (especially the following (elaborate/expansive) [Markdown](https://en.wikipedia.org/wiki/Markdown) documentation) are dedicated to the memory of:***

🫶 ***[Aaron Swartz](https://en.wikipedia.org/wiki/Aaron_Swartz)*** 🫶

</div>

---

Before you ask/assume (*ass-u-me*), no, this is not some [parallel universe](https://en.wikipedia.org/wiki/Multiverse) or [April Fools' Day](https://en.wikipedia.org/wiki/April_Fools'_Day) joke. [These public repositories (@gitlab.com/evlaV)](https://gitlab.com/users/evlaV/projects) are an **unmodified 1:1 public copy/mirror** of Valve's **latest** (currently private) [SteamOS 3.x (holo) GitLab repositories](https://gitlab.steamos.cloud/holo) ([secondary](https://gitlab.steamos.cloud/steam)), i.e., **SteamOS 3.x (holo) source code**.

[This repository (holo-PKGBUILD)](https://gitlab.com/evlaV/holo-PKGBUILD) and [jupiter-PKGBUILD](https://gitlab.com/evlaV/jupiter-PKGBUILD) have exclusively (and respectively) been modified (when necessary) to source from this public copy/mirror, i.e., [these public repositories (@gitlab.com/evlaV)](https://gitlab.com/users/evlaV/projects) - let's call them **public mirrors**. ***To clarify/elucidate, the ONLY (necessary) modifications made are to specific PKGBUILD - pointing/redirecting to [these public mirrors (@gitlab.com/evlaV)](https://gitlab.com/users/evlaV/projects) instead of Valve's (currently private) [SteamOS 3.x (holo) GitLab repositories](https://gitlab.steamos.cloud/holo) ([secondary](https://gitlab.steamos.cloud/steam)).***

These ~~public repositories~~ (*cough*) [**public mirrors** (@gitlab.com/evlaV)](https://gitlab.com/users/evlaV/projects) (including [this repository (holo-PKGBUILD)](https://gitlab.com/evlaV/holo-PKGBUILD)) are automatically maintained via a bot (powered by [renewable](https://en.wikipedia.org/wiki/Renewable_energy) solar ☀️); using a series of bespoke tools (e.g., [**srcpkg2git**](https://gitlab.com/evlaV/srcpkg2git)). I started developing these tools on *March 3, 2022*, (after *[Steam Deck (jupiter) Recovery Image 1](https://steamdeck-images.steamos.cloud/recovery/steamdeck-recovery-1.img.bz2)* ([zip](https://steamdeck-images.steamos.cloud/recovery/steamdeck-recovery-1.img.zip)) release date) while reverse engineering the (aforementioned) [Steam Deck (jupiter) Recovery Image](https://steamdeck-images.steamos.cloud/recovery/).

[These **public mirrors** (@gitlab.com/evlaV)](https://gitlab.com/users/evlaV/projects) launched *April 1, 2022*, and are automatically maintained (**updated daily**) until Valve officially publicizes their (currently private) [SteamOS 3.x (holo) GitLab repositories](https://gitlab.steamos.cloud/holo) ([secondary](https://gitlab.steamos.cloud/steam)), i.e., **SteamOS 3.x (holo) source code**. All projects are sourced from Valve's latest [official (main) source packages](https://steamdeck-packages.steamos.cloud/archlinux-mirror/sources/).

---

<img align="left" alt="holo-PKGBUILD" src="images/steamos-logo.png" width="48"><img align="right" alt="holo-PKGBUILD" src="images/steamos-logo-flipped.png" width="48"><img align="left" alt="jupiter-PKGBUILD" src="images/steam-deck-logo-black.png" width="48"><img align="right" alt="jupiter-PKGBUILD" src="images/steam-deck-logo-black-flipped.png" width="48">

<div align="center">

***I hope the publication and maintenance of [these public mirrors (@gitlab.com/evlaV)](https://gitlab.com/users/evlaV/projects) provide some (much needed) transparency as well as aid, encourage, and inspire SteamOS 3.x / Steam Deck contributors/developers/tinkerers.***

</div>

---

**(April 1, 2024):** After over **2 years** (and **2 Steam Deck model releases - LCD and OLED**) Valve [still hasn't publicized](#-gitlab-repository-private-public-how-to-) their [private GitLab repositories](https://gitlab.steamos.cloud/) nor [fully complied with the GPL](#-packagesrepositories-valve-collabora-and-igalia-are-in-violation-of-gnu-license-). I decided to (finally) release the relevant portion of my automated "bot" project, aptly titled [**srcpkg2git**](https://gitlab.com/evlaV/srcpkg2git). These tools haven't been updated/modified much since 2022; but should allow users to easily access and even mirror Valve's private repositories (as I've demonstrated with [these public mirrors (@gitlab.com/evlaV)](https://gitlab.com/users/evlaV/projects) the past over **2 years**).

**(October 23, 2024): Please take the following with a grain of salt**: *I believe Valve has (already) started withholding source package updates; it's possible this means Valve is (primarily) working on the next release of SteamOS; likely for their new (upcoming) hardware... I predict scant to mild future updates in the meantime...*

---

## Shallow Clone Recommended for Submodules (Submodule Shallow Clone)

Due to the (increasingly) redundant nature of the included submodules, a **submodule shallow clone** is recommended to reduce local storage consumption and improve repository performance. A shallow clone will (locally) trim and rebase (graft) repository:

```sh
git clone --recurse-submodules --shallow-submodules https://gitlab.com/evlaV/holo-PKGBUILD.git
```

To shallow clone [this repository (holo-PKGBUILD)](https://gitlab.com/evlaV/holo-PKGBUILD) and submodules:

```sh
git clone --depth=1 --recurse-submodules --shallow-submodules https://gitlab.com/evlaV/holo-PKGBUILD.git
```

To shallow clone [this repository (holo-PKGBUILD)](https://gitlab.com/evlaV/holo-PKGBUILD) and exclude submodules (not recommended):

```sh
git clone --depth=1 https://gitlab.com/evlaV/holo-PKGBUILD.git
```

or alternatively:

```sh
git clone --depth 1 https://gitlab.com/evlaV/holo-PKGBUILD.git
```

You may consider increasing the depth option parameter/value (**1**) to extend/increase commit history (at the cost of local storage consumption). Set **d**epth **(d=number/integer)** to **30** to retain the **29 (d-1)** latest commits and graft the **30th ((d)th)** commit (variably the commit history of the past month; if repository averages one (1) commit per day). Set depth to **1** for minimum local storage consumption, i.e., the latest (head) commit (grafted); no commit history (recommended).

To (re)shallow existing repository or retain shallow clone depth during/after Git fetch/pull (merge) operations, use option(s) `--depth=n` or `--depth=n --rebase` respectively:

```sh
git fetch --depth=1
git rebase
```

or alternatively:

```sh
git pull --depth=1 --rebase
```

- Optionally prune (potential) loose objects (trim) after (re)shallow:

   ```sh
   git gc --prune=now
   ```

---

To unshallow repository, i.e., restore complete/full clone depth (be prepared for a large download):

```sh
git fetch --unshallow
```

or alternatively:

```sh
git pull --unshallow
```

## Clone Complete Repository (include Submodules)

Use option `--recurse-submodules` during Git clone to include submodules:

```sh
git clone --recurse-submodules https://gitlab.com/evlaV/holo-PKGBUILD.git
```

or alternatively:

```sh
git clone https://gitlab.com/evlaV/holo-PKGBUILD.git
git submodule update --init --recursive
```

or alternatively:

```sh
git clone https://gitlab.com/evlaV/holo-PKGBUILD.git
git submodule init --recursive
git submodule update --recursive
```

---

Use option `--recurse-submodules` during Git fetch/pull (merge) operations to include submodules:

```sh
git fetch --recurse-submodules
```

or alternatively:

```sh
git pull --recurse-submodules
```

## SteamOS 3.x (holo) PKGBUILD (holo-PKGBUILD) Rebase

> **Latest Rebase: ~~July 26, 2022, December 2, 2022,~~ January 11, 2023**

[This repository (holo-PKGBUILD)](https://gitlab.com/evlaV/holo-PKGBUILD) may occasionally be rebased, which will (destructively) remove obsolete commit history and data (to reduce repository size) and improve repository performance, but may disrupt normal Git fetch/pull (merge) operations. If you receive a branch diverged error message upon Git fetch/pull (merge):

```sh
git fetch
git rebase
```

or alternatively:

```sh
git pull --rebase
```

**This will result in (local) commit history and data loss**; *commit history and data loss is composed/consisting completely of obsolete data which has since been depreciated or replaced by (a) newer/later revision(s).*

## ➕ Additional (Included) Software/Packages

- [map](map.sh) ([build.sh](build.sh)) | [documentation](#-automatically) | [PKGBUILD](map/PKGBUILD) - *Make(pkg) Arch Package* - make all (found or specified) pacman packages (PKGBUILD)

**Steam Deck (jupiter) | [jupiter-PKGBUILD](https://gitlab.com/evlaV/jupiter-PKGBUILD):**

- [jupiter-bios-tool.py](https://gitlab.com/evlaV/jupiter-PKGBUILD/-/blob/master/jupiter-bios-tool.py) | [documentation](https://gitlab.com/evlaV/jupiter-PKGBUILD#-steam-deck-jupiter-bios-tool-jupiter-bios-tool-) | [PKGBUILD](https://gitlab.com/evlaV/jupiter-PKGBUILD/-/blob/master/jupiter-bios-tool/PKGBUILD) - analyze, verify, backup/generate/inject UID, and dynamically trim ✂️ any/all Steam Deck (jupiter) BACKUP and RELEASE BIOS (*_sign.fd)
  - [jupiter-detect-unsupported-hw.sh](https://gitlab.com/evlaV/jupiter-PKGBUILD/-/blob/master/jupiter-detect-unsupported-hw.sh) | [documentation](https://gitlab.com/evlaV/jupiter-PKGBUILD#detect-unsupported-steam-deck-jupiter-hardware-revision--board-revision-2-ev2) | [PKGBUILD](https://gitlab.com/evlaV/jupiter-PKGBUILD/-/blob/master/jupiter-bios-tool/PKGBUILD) - detect unsupported Steam Deck (jupiter) hardware revision (EV2)
- **jupiter-bios-unlock**
  - [jupiter-bios-unlock.c](https://gitlab.com/evlaV/jupiter-PKGBUILD/-/blob/master/jupiter-bios-unlock.c) | [x86_64 prebuilt C binary](https://gitlab.com/evlaV/jupiter-PKGBUILD/-/blob/master/bin/jupiter-bios-unlock) | [jupiter-bios-unlock.cc](https://gitlab.com/evlaV/jupiter-PKGBUILD/-/blob/master/jupiter-bios-unlock.cc) | [documentation](https://gitlab.com/evlaV/jupiter-PKGBUILD#-cc-binary-jupiter-bios-unlockcjupiter-bios-unlockcc--preferredprimary-) | [PKGBUILD](https://gitlab.com/evlaV/jupiter-PKGBUILD/-/blob/master/jupiter-bios-unlock/PKGBUILD) | [PKGBUILD (bin)](https://gitlab.com/evlaV/jupiter-PKGBUILD/-/blob/master/jupiter-bios-unlock-bin/PKGBUILD) - enhanced/extended edition (e.g., unlock+lock support) based on [SD_Unlocker.c](https://gist.github.com/SmokelessCPUv2/8c1e6559031e199d9a678c9fe2ebf7d4) (preferred/primary)
  - [jupiter-bios-unlock.py](https://gitlab.com/evlaV/jupiter-PKGBUILD/-/blob/master/jupiter-bios-unlock.py) | [documentation](https://gitlab.com/evlaV/jupiter-PKGBUILD#-python-jupiter-bios-unlockpy-alternativesecondary-) | [PKGBUILD](https://gitlab.com/evlaV/jupiter-PKGBUILD/-/blob/master/jupiter-bios-unlock-python/PKGBUILD) - Python (wrapper) equivalent (alternative/secondary)

## 📦 Build/Make SteamOS 3.x (holo) Package

### 📦 Automatically

```sh
  usage: ./build.sh [package_dirname ...] [option ...] [makepkg_option ...]
example: ./build.sh steamos-efi linux holo-keyring --srcinfo -f

options:
  -h, --help            show this help message and exit
  -c, --clean           clean up work files after build
  -C, --Clean           remove $srcdir/ dir before building the package
  -f, --force           overwrite existing package
  -i, --install         install package after successful build
  -s, --syncdeps        install missing dependencies with pacman
  -si, --srcinfo        enable (AUR) .SRCINFO generation (before make package)
  -sio, --srcinfo-only  ONLY generate (AUR) .SRCINFO (disable/skip make package)
  -v, --version         show version information and exit

make all (found) package(s) if package (directory name (dirname)) not specified

see makepkg help ('makepkg -h', 'makepkg --help') for makepkg options
```

Run ([included](build.sh)) `./build.sh` (i.e., `./map.sh`) to automatically make all (found or specified) pacman packages (PKGBUILD); optionally specify package (directory name (dirname)) as argument(s) to make instead of all (found). Use option `-si` or `--srcinfo` to enable (AUR) [.SRCINFO](https://wiki.archlinux.org/title/.SRCINFO) generation (before make package). Use option `-sio` or `--srcinfo-only` to ONLY generate (AUR) [.SRCINFO](https://wiki.archlinux.org/title/.SRCINFO) (disable/skip make package).

Run ([included](build.sh)) `./build.sh -h` (i.e., `./map.sh -h`) for usage instructions.

### 📦 Manually

- Change directory to respective pacman package(s) (PKGBUILD) e.g., `cd steamos-efi`

Make respective pacman package (PKGBUILD):

```sh
makepkg
```

Install (potentially) missing dependencies (with pacman) then make respective pacman package (PKGBUILD):

```sh
makepkg -s
```

Install (potentially) missing dependencies (with pacman) then make and install respective pacman package (PKGBUILD):

```sh
makepkg -is
```

Install (potentially) missing dependencies (with pacman) then clean, make, and install respective pacman package (PKGBUILD):

```sh
makepkg -Cis
```

Run `makepkg -h` or see: [makepkg usage](https://wiki.archlinux.org/title/Makepkg#Usage) for detailed usage instructions.

---

## <a href="https://gitlab.com/evlaV/holo-PKGBUILD"><img align="center" alt="holo-PKGBUILD" src="images/steamos-logo.png" width="32"></a> <a href="https://gitlab.com/evlaV/holo-PKGBUILD">SteamOS 3.x (holo)</a> <a href="https://gitlab.com/evlaV/holo-PKGBUILD"><img align="center" alt="holo-PKGBUILD" src="images/steamos-logo-flipped.png" width="32"></a><a href="https://gitlab.com/evlaV/holo-PKGBUILD"><img align="center" alt="portal" src="images/portal-3.png" height="32" width="4"></a><a href="https://gitlab.com/evlaV/jupiter-PKGBUILD"><img align="center" alt="portal" src="images/portal-4.png" height="32" width="4"></a><a href="https://gitlab.com/evlaV/jupiter-PKGBUILD"><img align="center" alt="jupiter-PKGBUILD" src="images/steam-deck-logo-black.png" width="32"></a> <a href="https://gitlab.com/evlaV/jupiter-PKGBUILD">Steam Deck (jupiter)</a> <a href="https://gitlab.com/evlaV/jupiter-PKGBUILD"><img align="center" alt="jupiter-PKGBUILD" src="images/steam-deck-logo-black-flipped.png" width="32"></a> PKGBUILD (Source Code) Public Mirror 🪞 Portal <a href="https://gitlab.com/evlaV/holo-PKGBUILD"><img align="center" alt="portal" src="images/portal-5.png" height="32" width="4"></a><a href="https://gitlab.com/evlaV/jupiter-PKGBUILD"><img align="center" alt="portal" src="images/portal-6.png" height="32" width="4"></a>

<div align="center"><a href="https://gitlab.com/evlaV/holo-PKGBUILD"><img alt="holo" src="images/holo-logo.png" height="128"></a><a href="https://gitlab.com/evlaV/holo-PKGBUILD"><img alt="portal" src="images/portal-1.png" height="128" width="6"></a><a href="https://gitlab.com/evlaV/jupiter-PKGBUILD"><img alt="portal" src="images/portal-2.png" height="128" width="6"></a><a href="https://gitlab.com/evlaV/jupiter-PKGBUILD"><img alt="jupiter" src="images/jupiter-logo-black.png" height="128"></a></div>

---

## Is Valve's Steam/SteamOS End User License Agreement (EULA) Valid or Void? 👍👎

> **I've been debating on the publication of this section since I started this project on *March 3, 2022***

[![Steam/SteamOS EULA](images/steam-steamos-eula.png)](https://store.steampowered.com/steamos/download/?ver=steamdeck)

[Notice how Valve stipulates](https://store.steampowered.com/steamos/download/?ver=steamdeck) their FOSS packages are available on their [GitLab (https://gitlab.steamos.cloud/)](https://gitlab.steamos.cloud/). The very GitLab repositories which are (still) private and I've been advocating for publication for over **2 years** now! There's nothing of the sort publicly available there, and the [Steam Deck has been released for over **2 years**](https://www.timeanddate.com/date/durationresult.html?m1=2&d1=25&y1=2022&ti=on) now.

I believe this would be considered a misrepresentation. In contract law, a misrepresentation is a false statement of fact made by one party that has the effect of inducing the other party to enter into a contract. Misrepresentation can void a contract and in some cases allow the misled party to seek damages.

---

## 👍 Valve (Official) SteamOS 3.x (holo) and Steam Deck (jupiter) Public Repositories 👍

(Pitiful) Table of Valve's officially public SteamOS 3.x (holo) and Steam Deck (jupiter) repositories:

| Repository | Branch | Release Date |
| :--- | :---: | ---: |
| [wireplumber](https://gitlab.steamos.cloud/jupiter/wireplumber) | jupiter | June 30, 2022 |
| [steamos-devkit-service](https://gitlab.steamos.cloud/devkit/steamos-devkit-service) | holo* / jupiter | March 30, 2022 |
| [steamos-devkit-client](https://gitlab.steamos.cloud/devkit/steamos-devkit) | - | March 9, 2022 |
| [steam-im-modules](https://github.com/valve-project/steam-qt-keyboard-plugin) | holo* / jupiter | February 23, 2022 |
| [gamescope](https://github.com/ValveSoftware/gamescope) ([@Plagman](https://github.com/Plagman/gamescope)) | holo* / jupiter | predates SteamOS 3.x / Steam Deck |

***Table contains ONLY software developed (or modified) by Valve, i.e., excludes unmodified public software.***

> *Denotes no respective PKGBUILD currently exists (i.e., no dedicated build/package).

---

## 🙅 Packages/Repositories Valve, Collabora, and Igalia are in Violation of GNU License 👎

- **[What comes after open source?](https://www.theregister.com/2023/12/27/bruce_perens_post_open/) [Post-Open Source?](https://lu.is/blog/2013/01/27/taking-post-open-source-seriously-as-a-statement-about-copyright-law/)**

> [There are hundreds of missing GPL packages' source code](#gpl-package-research) - notable examples: `pacman` and `pacman-mirrorlist` - **I believe the latter may have been intentionally omitted to abscond Valve's source package location ([smoking gun](https://en.wikipedia.org/wiki/Smoking_gun) perhaps?)**

[GPL Violators](https://www.gnu.org/licenses/gpl-violation.html) (e.g., [**Valve**](https://www.valvesoftware.com/en/about), [**Collabora**](https://www.collabora.com/about-us/), and [**Igalia**](https://www.igalia.com/about/)) are those who **distribute products based on GPL software without complying with the license conditions**; such as **providing the *complete* source code**, a copy of the license, and **a (written) offer for source code**. **GPL violations are considered a form of infringing use of the software and can be legally pursued by the copyright holders.**

These [GPL Violators](https://www.gnu.org/licenses/gpl-violation.html) (e.g., [**Valve**](https://www.valvesoftware.com/en/about), [**Collabora**](https://www.collabora.com/about-us/), and [**Igalia**](https://www.igalia.com/about/)) are unfair and disrespectful to the free software community and the authors of the respective software. They are taking advantage of the benefits of GPL software without honoring the obligations and the spirit of the license. They deprive users of their freedom to study, modify, and share the software. They should be educated about the meaning and importance of GPL and other free licenses, and be encouraged to adopt ethical/sound practices for the incorporation of GPL components into their products. They should also be held accountable for their actions and be required to remedy their violations by complying with the license terms or ceasing distribution of the infringing products. They should additionally be deterred from continuing and/or repeating violations via public awareness campaigns and legal actions. **I'm endeavoring on both fronts (public awareness and legal action) to right (and *write*) these wrongs.**

[**Valve**](https://www.valvesoftware.com/en/about), [**Collabora**](https://www.collabora.com/about-us/), and [**Igalia**](https://www.igalia.com/about/) should respect the GPL and comply with its terms as soon as possible (it's been over **2 years** now). They should communicate clearly and transparently about their use of GPL software and their "plans" to adequately offer/release the source code. They should also collaborate with the free software community and contribute to the development and improvement of GPL software. A good/healthy start would be (finally) opening dialogue with me. In case you hadn't noticed, I'm quite proficient and have been **doing your job** for over **2 years** now. It's about time we had a **real discussion**. I would greatly appreciate if someone from [**Valve**](https://www.valvesoftware.com/en/about), [**Collabora**](https://www.collabora.com/about-us/), and/or [**Igalia**](https://www.igalia.com/about/) would (finally) respond to me and address these grievances.

A very bad trend has been set and the status quo (creatively) continued regarding 🄯 [copyleft](https://en.wikipedia.org/wiki/Copyleft). Violations are frequent, and violators are infrequently held accountable. I begrudgingly feel copyleft is seen as an [honor system](https://en.wikipedia.org/wiki/Honor_system), and you should (realistically) presume all your copyleft projects and contributions will be interpreted/used with a [permissive license](https://en.wikipedia.org/wiki/Permissive_software_license). So you might as well pick a "good/decent" permissive (or [middle ground](https://www.mozilla.org/en-US/MPL/2.0/FAQ/)) license (since that's unfortunately the (capitalistic/corporate) world we live in) or [release into the public domain](https://unlicense.org/). I've personally demonstrated both with this project ([LICENSE](LICENSE), [UNLICENSE](UNLICENSE), and [0BSD](LICENSE.0BSD)).

I've privately informed (and now publicly informed and condemned) both [**Collabora**](https://www.collabora.com/about-us/) and [**Igalia**](https://www.igalia.com/about/) of the following (neither have responded):

`"There's a conflict of interest working and/or consulting with Valve on this particular project (SteamOS 3.x / Steam Deck)."`

**(March 25, 2023):** The [FSF](https://www.fsf.org/about/) (`rms`) has demonstrated the following to me (in the case of Valve) regarding [defending their GPL](https://www.gnu.org/licenses/):

[rms](https://en.wikipedia.org/wiki/Richard_Stallman):

`"The FSF is short-handed in the legal area. Given how little it could hope to achieve, I tend to think that this would not be an efficient choice of priorities (though I'm not the one who decides nowadays). I don't think I could get a staff person to verify the origin of the copied code in less than months."`

This interaction has (sadly) not instilled much faith in the FSF (or the GPL) and only further solidifies my license recommendations above. Regardless, thank you `rms`, for your time, honesty/transparency, inquiries/interest, and recommendation. You've been the most communicative and inquisitive person I've interacted with regarding this. I'm sorry our discussions were centered around this deplorable injustice.

~~I'm presently moving forward with the [Software Freedom Conservancy](https://sfconservancy.org/about/) (with `rms`'s recommendation/referral).~~

**Come along with me as I foolishly try and take the pulse of a piece of the corporate software/technology world in 2022, 2023, 2024 ... It will be fun, they said.**

I'm well aware that many people don't technically fully comprehend/understand the GPL. This is a far bigger (systemic) issue than simply Valve, Collabora, and Igalia. I'd argue most people are ignorant and/or misinformed regarding the GPL. I **highly recommend** ALL Linux/OSS users/enthusiasts, especially if you work for a company which may utilize (and also potentially violate) GPL software, **THOROUGHLY READ THE GPL**. The following is pretty well documented (shocker) in the GPL (if only people could be bothered enough to actually read it).

The GPL (GPL2, GPL3, AGPL, and LGPL3) stipulates that sources must accompany binaries, otherwise a (written) offer for source code (or equivalent stipulated offer) is required. See GPL2 Section 3, or GPL3 Section 6 for accepted methods of distributing sources. If one or more of these methods is not followed by Valve, then a GPL violation has occurred. I presently count [~~dozens~~](#third-party-packagesrepositories-valve-collabora-and-igalia-are-in-violation-of-gnu-license) [hundreds](#gpl-package-research) of GPL violations which have been unresolved for over **2 years** now.

See: [GPL FAQ](https://www.gnu.org/licenses/gpl-faq.html#WhatDoesWrittenOfferValid)

*"If you choose to provide source through a written offer, then anybody who requests the source from you is entitled to receive it."*

*"If you commercially distribute binaries not accompanied with source code, the GPL says you must provide a written offer to distribute the source code later."*

Thank you for taking the time to learn a bit about proper copyleft (GPL) software distribution. I hope you've learned not just to comprehend/understand the GPL, but to also value and respect the free/libre license. Could the community do us all a favor and help inform Valve, Collabora, and Igalia of this fact?

But don't take my word for it, the following is a excerpt from my aforementioned discussion with `rms`:

`"Selling the computer is distribution of the preinstalled binaries of GPL-covered programs. GPL 2 and GPL 3 (and AGPL also, and LGPL 3) all require making the source available along with the binaries.`

`[Section] 6 of GPL 3 states the accepted ways of making the source available.`

`If Valve does not do any of those ways, it is violating the GPL."`

---

> **SteamOS 3.x (holo) / Steam Deck (jupiter) Release Date: [February 25, 2022](https://www.timeanddate.com/date/durationresult.html?m1=2&d1=25&y1=2022&ti=on)**
>
> [**Number of Days Valve, Collabora, and Igalia are in Violation of GNU License (i.e., since Steam Deck release)**](https://www.timeanddate.com/date/durationresult.html?m1=2&d1=25&y1=2022&ti=on)

**"If it looks like a duck, swims like a duck, and quacks like a duck, then it probably is a duck."**

<img alt="the cake is a lie." src="images/the-cake-is-a-lie.jpg" width="512">

**the cake is a lie.**

**the cake is a lie.**

**the cake is a lie.**

**the cake is a lie.**

***But not this cake! Happy Cake Day Steam Deck! Thank you Valve for showing the world GNU/Linux is the true home of PC gaming! Please support [free software](https://www.gnu.org/software/). (February 25, 2023)***

<img alt="the cake is not a lie." src="images/the-cake-is-not-a-lie.jpg" width="512">

---

***(December 5, 2023):** After nearly **2 years** with **no response** and **no resolution** from **Valve**, **Gabe Newell**, **Collabora**, or **Igalia** I have ~~only one thing~~ **two things** to publicly say to you, and I hope I speak for the majority of the (free/libre) open source community when I say:*

**Valve, Gabe Newell, Collabora, and Igalia, 🖕 FUCK YOU! 🖕**

<img alt="🖕 FUCK YOU! 🖕" src="images/fuck-you-1.jpg" width="512"><img alt="🖕 FUCK YOU! 🖕" src="images/fuck-you-2.jpg" width="512">

***(December 8, 2023):** In the same vein, I'd also like to **thank you**, **Valve**, **Gabe Newell**, **Collabora**, and **Igalia** (and **rms** and the **Software Freedom Conservancy**) for giving me this **prolonged** and **challenging opportunity** to **continuously/repeatedly fail to (fully) achieve my goal** (in numerous ways). I've spent the past over **2 years** channelling this **continuous/repeated failure** into very **priceless**, **creative**, **diverse**, and **beautiful growth/development** (as well as **engage with** and **help some interesting people** along the way). You've helped give me a **platform**, and my creative efforts **purpose** and **perspective** the past over **2 years**, and for that, I cannot **thank you** enough.* 🙏

---

### Third Party Packages/Repositories Valve, Collabora, and Igalia are in Violation of GNU License

Table of packages/repositories **Valve, Collabora, and Igalia are in Violation of respective GNU License(s):**

| Package/Repository | Branch | License | Reason |
| :--- | :---: | ---: | :---: |
| [archlinux-keyring](https://gitlab.archlinux.org/archlinux/archlinux-keyring) | holo, jupiter | [GPLv3](https://gitlab.archlinux.org/archlinux/archlinux-keyring/-/blob/master/LICENSE) | **obfuscated** source code* |
| [bluez](https://git.kernel.org/pub/scm/bluetooth/bluez.git) | holo, jupiter | [GPLv2](https://git.kernel.org/pub/scm/bluetooth/bluez.git/tree/COPYING) | **obfuscated** source code* |
| [bmap-tools](https://github.com/intel/bmap-tools) | holo | [GPLv2](https://github.com/intel/bmap-tools/blob/master/LICENSE) | **obfuscated** source code* |
| [calamares](https://github.com/calamares/calamares) | holo | [GPLv3](https://github.com/calamares/calamares/blob/calamares/LICENSES/GPL-3.0-or-later.txt), [LGPLv3](https://github.com/calamares/calamares/blob/calamares/LICENSES/LGPL-3.0-or-later.txt), [LGPLv2.1](https://github.com/calamares/calamares/blob/calamares/LICENSES/LGPL-2.1-only.txt) | **obfuscated** source code* |
| [casync](https://github.com/systemd/casync) | holo | [LGPLv2.1](https://github.com/systemd/casync/blob/main/LICENSE.LGPL2.1) | **obfuscated** source code* |
| [ckbcomp](https://salsa.debian.org/installer-team/console-setup) | holo | [GPLv2](https://salsa.debian.org/installer-team/console-setup/-/blob/master/GPL-2) | **obfuscated** source code* |
| [dkms](https://github.com/dell/dkms) | holo | [GPLv2](https://github.com/dell/dkms/blob/master/COPYING) | **obfuscated** source code* |
| [f3](https://github.com/AltraMayor/f3) | jupiter | [GPLv3](https://github.com/AltraMayor/f3/blob/master/LICENSE) | **obfuscated** source code* |
| [ffmpeg](https://git.ffmpeg.org/gitweb/ffmpeg) | holo | [GPLv3](https://git.ffmpeg.org/gitweb/ffmpeg.git/blob/HEAD:/COPYING.GPLv3), [GPLv2](https://git.ffmpeg.org/gitweb/ffmpeg.git/blob/HEAD:/COPYING.GPLv2), [LGPLv3](https://git.ffmpeg.org/gitweb/ffmpeg.git/blob/HEAD:/COPYING.LGPLv3), [LGPLv2.1](https://git.ffmpeg.org/gitweb/ffmpeg.git/blob/HEAD:/COPYING.LGPLv2.1) | **obfuscated** source code* |
| [flatpak](https://github.com/flatpak/flatpak) | jupiter | [LGPLv2.1](https://github.com/flatpak/flatpak/blob/main/COPYING) | **obfuscated** source code* |
| [grub](https://www.gnu.org/software/grub/) | holo, jupiter | [GPLv3](https://www.gnu.org/licenses/gpl-3.0.txt) | **obfuscated** source code* |
| [holo-keyring](https://gitlab.com/evlaV/holo-keyring) (fork of [archlinux-keyring](https://gitlab.archlinux.org/archlinux/archlinux-keyring)) | holo | [GPLv3](https://gitlab.com/evlaV/holo-keyring/-/blob/master/LICENSE) | **obfuscated** source code* |
| [ibus-anthy](https://github.com/ibus/ibus-anthy) | jupiter | [GPLv2](https://github.com/ibus/ibus-anthy/blob/main/COPYING) | **obfuscated** source code* |
| [ibus-table-cangjie-lite](https://github.com/mike-fabian/ibus-table-chinese) | jupiter | [GPLv3](https://github.com/mike-fabian/ibus-table-chinese/blob/main/COPYING) | **obfuscated** source code* |
| [iwd](https://git.kernel.org/pub/scm/network/wireless/iwd.git) | holo | [LGPLv2.1](https://git.kernel.org/pub/scm/network/wireless/iwd.git/tree/COPYING) | **obfuscated** source code* |
| [kscreen](https://invent.kde.org/plasma/kscreen) | jupiter | [GPLv3](https://invent.kde.org/plasma/kscreen/-/blob/master/LICENSES/GPL-3.0-only.txt), [GPLv2](https://invent.kde.org/plasma/kscreen/-/blob/master/LICENSES/GPL-2.0-or-later.txt), [LGPLv2](https://invent.kde.org/plasma/kscreen/-/blob/master/LICENSES/LGPL-2.0-or-later.txt) | **obfuscated** source code* |
| [kmod](https://git.kernel.org/pub/scm/utils/kernel/kmod/kmod.git) | holo | [LGPLv2.1](https://git.kernel.org/pub/scm/utils/kernel/kmod/kmod.git/tree/COPYING) | **obfuscated** source code* |
| [kupdate-notifier](https://gitlab.com/evlaV/kupdate-notifier) | holo | [LGPLv2.1](https://gitlab.com/evlaV/kupdate-notifier/-/blob/master/LICENSE) | **obfuscated** source code* |
| [lib32-alsa-lib](https://github.com/alsa-project/alsa-lib) | jupiter | [LGPLv2.1](https://github.com/alsa-project/alsa-lib/blob/master/COPYING) | **obfuscated** source code* |
| [lib32-libnm](https://gitlab.freedesktop.org/NetworkManager/NetworkManager) | jupiter | [GPLv3](https://gitlab.freedesktop.org/NetworkManager/NetworkManager/-/blob/main/COPYING), [LGPLv2.1](https://gitlab.freedesktop.org/NetworkManager/NetworkManager/-/blob/main/COPYING.LGPL) | **obfuscated** source code* |
| [libevdi](https://github.com/DisplayLink/evdi) | jupiter | [GPLv2](https://github.com/DisplayLink/evdi/blob/devel/module/LICENSE), [LGPLv2.1](https://github.com/DisplayLink/evdi/blob/devel/library/LICENSE) | **obfuscated** source code* |
| [libxcrypt-compat](https://github.com/besser82/libxcrypt) | holo | [LGPLv2.1](https://github.com/besser82/libxcrypt/blob/develop/COPYING.LIB) | **obfuscated** source code* |
| [linux](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git) | holo | [GPLv2](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/tree/LICENSES/preferred/GPL-2.0) | **obfuscated** source code* |
| [linux-firmware-neptune](https://git.kernel.org/pub/scm/linux/kernel/git/firmware/linux-firmware.git) | jupiter | [GPLv3](https://git.kernel.org/pub/scm/linux/kernel/git/firmware/linux-firmware.git/tree/GPL-3), [GPLv2](https://git.kernel.org/pub/scm/linux/kernel/git/firmware/linux-firmware.git/tree/GPL-2) | **obfuscated** source code* |
| [linux-lts](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git) | holo | [GPLv2](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/tree/LICENSES/preferred/GPL-2.0) | **obfuscated** source code* |
| [linux-neptune-61](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git) | jupiter | [GPLv2](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/tree/LICENSES/preferred/GPL-2.0) | **obfuscated** source code* |
| [linux-neptune-60](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git) | jupiter | [GPLv2](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/tree/LICENSES/preferred/GPL-2.0) | **obfuscated** source code* |
| [linux-neptune](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git) | jupiter | [GPLv2](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/tree/LICENSES/preferred/GPL-2.0) | **obfuscated** source code* |
| [linux-neptune-rtw](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git) | jupiter | [GPLv2](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/tree/LICENSES/preferred/GPL-2.0) | **obfuscated** source code* |
| [makedumpfile](https://github.com/makedumpfile/makedumpfile) | holo | [GPLv2](https://github.com/makedumpfile/makedumpfile/blob/master/COPYING) | **obfuscated** source code* |
| [networkmanager](https://gitlab.freedesktop.org/NetworkManager/NetworkManager) | jupiter | [GPLv3](https://gitlab.freedesktop.org/NetworkManager/NetworkManager/-/blob/main/COPYING), [LGPLv2.1](https://gitlab.freedesktop.org/NetworkManager/NetworkManager/-/blob/main/COPYING.LGPL) | **obfuscated** source code* |
| [noise-suppression-for-voice](https://github.com/werman/noise-suppression-for-voice) | holo | [GPLv3](https://github.com/werman/noise-suppression-for-voice/blob/master/LICENSE) | **obfuscated** source code* |
| [pacman-system-update](https://github.com/gportay/pacman-system-update) | holo | [LGPLv2.1](https://github.com/gportay/pacman-system-update/blob/master/LICENSE) | **obfuscated** source code* |
| [paru](https://github.com/Morganamilo/paru) | holo | [GPLv3](https://github.com/Morganamilo/paru/blob/master/LICENSE) | **obfuscated** source code* |
| [pipewire](https://gitlab.freedesktop.org/pipewire/pipewire) | holo | [GPLv2](https://gitlab.freedesktop.org/pipewire/pipewire/-/blob/master/LICENSE), [LGPL](https://gitlab.freedesktop.org/pipewire/pipewire/-/blob/master/LICENSE) | **obfuscated** source code* |
| [plasma-nm](https://invent.kde.org/plasma/plasma-nm) | jupiter | [GPLv3](https://invent.kde.org/plasma/plasma-nm/-/blob/master/LICENSES/GPL-3.0-only.txt), [GPLv2](https://invent.kde.org/plasma/plasma-nm/-/blob/master/LICENSES/GPL-2.0-or-later.txt), [LGPLv3](https://invent.kde.org/plasma/plasma-nm/-/blob/master/LICENSES/LGPL-3.0-only.txt), [LGPLv2.1](https://invent.kde.org/plasma/plasma-nm/-/blob/master/LICENSES/LGPL-2.1-only.txt), [LGPLv2](https://invent.kde.org/plasma/plasma-nm/-/blob/master/LICENSES/LGPL-2.0-or-later.txt) | **obfuscated** source code* |
| [plasma-remotecontrollers](https://invent.kde.org/plasma-bigscreen/plasma-remotecontrollers) | jupiter | [GPLv3](https://invent.kde.org/plasma-bigscreen/plasma-remotecontrollers/-/blob/master/LICENSES/GPL-3.0-only.txt), [GPLv2](https://invent.kde.org/plasma-bigscreen/plasma-remotecontrollers/-/blob/master/LICENSES/GPL-2.0-only.txt) | **obfuscated** source code* |
| [plasma-workspace](https://invent.kde.org/plasma/plasma-workspace) | jupiter | [GPLv3](https://invent.kde.org/plasma/plasma-workspace/-/blob/master/LICENSES/GPL-3.0-only.txt), [GPLv2](https://invent.kde.org/plasma/plasma-workspace/-/blob/master/LICENSES/GPL-2.0-or-later.txt), [LGPLv3](https://invent.kde.org/plasma/plasma-workspace/-/blob/master/LICENSES/LGPL-3.0-or-later.txt), [LGPLv2.1](https://invent.kde.org/plasma/plasma-workspace/-/blob/master/LICENSES/LGPL-2.1-or-later.txt), [LGPLv2](https://invent.kde.org/plasma/plasma-workspace/-/blob/master/LICENSES/LGPL-2.0-or-later.txt) | **obfuscated** source code* |
| [plymouth](https://gitlab.freedesktop.org/plymouth/plymouth) | holo | [GPLv2](https://gitlab.freedesktop.org/plymouth/plymouth/-/blob/main/COPYING) | **obfuscated** source code* |
| [pyzy](https://github.com/pyzy/pyzy) | holo | [LGPLv2.1](https://github.com/pyzy/pyzy/blob/master/COPYING) | **obfuscated** source code* |
| [rauc](https://github.com/rauc/rauc) | holo | [LGPLv2.1](https://github.com/rauc/rauc/blob/master/COPYING) | **obfuscated** source code* |
| [rtl88x2ce-dkms](https://gitlab.com/evlaV/jupiter-PKGBUILD/-/tree/master/rtl88x2ce-dkms) | jupiter | GPLv? | **obfuscated** source code* |
| [sddm-wayland](https://github.com/sddm/sddm) | jupiter | [GPLv2](https://github.com/sddm/sddm/blob/develop/LICENSE) | **obfuscated** source code* |
| [steamos-efi](https://gitlab.com/evlaV/steamos-efi) (fork of gnu-efi) | holo, jupiter | [GPLv2](https://gitlab.com/evlaV/steamos-efi/-/blob/master/COPYING) | **obfuscated** source code* |
| [udisks2](https://github.com/storaged-project/udisks) | jupiter | [GPLv2](https://github.com/storaged-project/udisks/blob/master/COPYING), [LGPLv2](https://github.com/storaged-project/udisks/blob/master/COPYING) | **obfuscated** source code* |
| [upower](https://gitlab.freedesktop.org/upower/upower) | jupiter | [GPLv2](https://gitlab.freedesktop.org/upower/upower/-/blob/master/COPYING) | **obfuscated** source code* |
| [vkmark](https://github.com/vkmark/vkmark) | holo | [LGPLv2.1](https://github.com/vkmark/vkmark/blob/master/COPYING-LGPL2.1) | **obfuscated** source code* |
| [xdg-desktop-portal](https://github.com/flatpak/xdg-desktop-portal) | jupiter | [LGPLv2.1](https://github.com/flatpak/xdg-desktop-portal/blob/main/COPYING) | **obfuscated** source code* |
| [xone-dkms](https://github.com/medusalix/xone) | holo | [GPLv2](https://github.com/medusalix/xone/blob/master/LICENSE) | **obfuscated** source code* |
| [xow](https://github.com/medusalix/xow) | holo | [GPLv2](https://github.com/medusalix/xow/blob/master/LICENSE) | **obfuscated** source code* |
| [zenity-light](https://gitlab.gnome.org/GNOME/zenity) | jupiter | [LGPLv2.1](https://gitlab.gnome.org/GNOME/zenity/-/blob/master/COPYING) | **obfuscated** source code* |

### First/Second Party (Valve, Collabora, and Igalia) Packages/Repositories in Violation of GNU License

| Package/Repository | Branch | License | Reason |
| :--- | :---: | ---: | :---: |
| [kdump-steamos](https://gitlab.com/evlaV/kdump-steamos) | holo | [LGPLv2.1](https://gitlab.com/evlaV/kdump-steamos/-/blob/main/README.md) | **obfuscated** source code* |
| [steamdeck-kde-presets](https://gitlab.com/evlaV/steamdeck-kde-presets) | jupiter | [GPLv2](https://www.gnu.org/licenses/old-licenses/gpl-2.0.txt) | **obfuscated** source code* |
| [steamos-atomupd-client](https://gitlab.com/evlaV/steamos-atomupd) | holo | [LGPLv2](https://gitlab.com/evlaV/steamos-atomupd/-/blob/master/COPYING) | **obfuscated** source code* |
| [steamos-customizations](https://gitlab.com/evlaV/steamos-customizations) | holo | [LGPLv2.1](https://gitlab.com/evlaV/steamos-customizations/-/blob/master/COPYING.LGPL-2.1) | **obfuscated** source code* |
| [steamos-customizations-jupiter](https://gitlab.com/evlaV/steamos-customizations) | jupiter | [LGPLv2.1](https://gitlab.com/evlaV/steamos-customizations/-/blob/master/COPYING.LGPL-2.1) | **obfuscated** source code* |
| [steamos-media-creation](https://gitlab.com/evlaV/steamos-media-creation) | holo | [LGPLv2.1](https://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt) | **obfuscated** source code* |
| [steamos-netrc](https://gitlab.com/evlaV/holo-PKGBUILD/-/tree/master/steamos-netrc) | holo | GPLv? | **obfuscated** source code* |
| [steamos-repair-backend](https://gitlab.com/evlaV/steamos-repair-backend) | holo | [GPLv2](https://gitlab.com/evlaV/steamos-repair-backend/-/blob/main/COPYING) | **obfuscated** source code* |
| [steamos-repair-tool](https://gitlab.com/evlaV/steamos-repair-tool) | holo | [GPLv2](https://gitlab.com/evlaV/steamos-repair-tool/-/blob/master/COPYING) | **obfuscated** source code* |

***obfuscated** by one or both of the following:

1. ~~**no (written) offer for source code** - no official documentation or link(s) to source code (i.e., officially undocumented) - e.g., [Oracle](https://www.oracle.com/downloads/opensource/software-components-source-code.html) and [Sony PS5](https://doc.dl.playstation.net/doc/ps5-oss/index.html)~~ **(September 21, 2023) [574 days after Steam Deck release](https://www.timeanddate.com/date/durationresult.html?m1=2&d1=25&y1=2022&m2=9&d2=21&y2=2023&ti=on)**
2. source code requires (officially undocumented) user alteration/modification/deobfuscation to be accessible/legible/usable/buildable (i.e., obfuscated) - see: [Automatically Deobfuscate Source Package](#automatically-deobfuscate-source-package) or [Manually Deobfuscate Source Package](#manually-deobfuscate-source-package)
3. official (unmodified) PKGBUILD sources from / depends on [Valve's official private GitLab repositories](https://gitlab.steamos.cloud/) (i.e., unbuildable); PKGBUILD provided here are modified to source from / depend on [these unofficial public mirrors](https://gitlab.com/evlaV) (i.e., buildable)

[**These public mirrors (@gitlab.com/evlaV)**](https://gitlab.com/users/evlaV/projects) aim to (unofficially) correct these violations.

***missing** source code:

- see: [GPL Package Research](#gpl-package-research)

---

**(September 21, 2023):** The following notice/offer was (finally) added [574 days after Steam Deck release](https://www.timeanddate.com/date/durationresult.html?m1=2&d1=25&y1=2022&m2=9&d2=21&y2=2023&ti=on) to SteamOS (**Steam Version 1695334486**):

> **This was done purely in response to some back-and-forth between the [Software Freedom Conservancy](https://sfconservancy.org/about/) and [Valve's (reclusive) general/chief counsel (CLO) Liam Lavery](mailto:liaml@valvesoftware.com)**

<img alt="SteamOS Notice" src="images/steamos-notice-1.jpg" width="512">

<img alt="SteamOS Notice" src="images/steamos-notice-2.jpg" width="512">

> *Excuse me, I'm a bit of a stickler [Meeseeks], but **Valve, go home, you're drunk**; your operating system is [officially titled](https://repo.steampowered.com/steamos/README.txt) **SteamOS**, not **Steam OS***

---

### GPL Package Research

[Latest Steam Deck (jupiter) Recovery Image ("SteamOS Deck Image")](https://store.steampowered.com/steamos/download/?ver=steamdeck)

[Steam Deck (jupiter) Recovery Download Directory](https://steamdeck-images.steamos.cloud/recovery/)

---

**(February 28, 2022):** There were [542 GPL packages of 871 total packages (62.227%)](research/steam-deck-recovery-1-package-info.txt) distributed in [Steam Deck Recovery Image 1](https://steamdeck-images.steamos.cloud/recovery/steamdeck-recovery-1.img.bz2) ("SteamOS Deck Image") ([zip](https://steamdeck-images.steamos.cloud/recovery/steamdeck-recovery-1.img.zip))

> b2sum: `30453a4edab260d6a7af7c74b6287f6361e86d19e4d6d3b45a14c620d0bf2540ac3f281b5947e9541844db41c79681b18156e7db96a9ce95a23e52ba6409e2c9 steamdeck-recovery-1.img.bz2`

---

**(May 15, 2022):** There were [543 GPL packages of 871 total packages (62.342%)](research/steam-deck-recovery-4-package-info.txt) distributed in [Steam Deck Recovery Image 4](https://steamdeck-images.steamos.cloud/recovery/steamdeck-recovery-4.img.bz2) ("SteamOS Deck Image") ([zip](https://steamdeck-images.steamos.cloud/recovery/steamdeck-recovery-4.img.zip))

> b2sum: `ab806fb2374db4135b1a706425c81d367aeeb8508b4417ba652438e52772b88462f029a257f193d6827df8e526bbad9b10c532ef1a437b7393312e5fd4ad9c0e steamdeck-recovery-4.img.bz2`

---

**(as of June 17, 2023):** There were [593 GPL packages of 944 total packages (62.817%)](research/steam-deck-jupiter-main-package-info-20230617.txt) distributed in Steam Deck (jupiter) main (jupiter-main)

**(as of September 21, 2023):** There were [595 GPL packages of 971 total packages (61.277%)](research/steam-deck-jupiter-main-package-info.txt) distributed in Steam Deck (jupiter) main (jupiter-main)

---

### Automatically Deobfuscate Source Package

Using [srcpkg2git](https://gitlab.com/evlaV/srcpkg2git):

```sh
usage:
  srcpkg2git local/remote source package (src.tar[.gz|.xz]) PATH/URL [...]

 e.g.:
  srcpkg2git /path/to/package1.src.tar[.gz|.xz] file:///path/to/package2.src.tar[.gz|.xz]
  srcpkg2git https://example.com/package3.src.tar[.gz|.xz]
```

```sh
srcpkg2git https://steamdeck-packages.steamos.cloud/archlinux-mirror/sources/jupiter-main/zenity-light-3.32.0%2B55%2Bgd7bedff6-1.src.tar.gz
```

```sh
./srcpkg2git.sh https://steamdeck-packages.steamos.cloud/archlinux-mirror/sources/jupiter-main/zenity-light-3.32.0%2B55%2Bgd7bedff6-1.src.tar.gz
bash srcpkg2git.sh https://steamdeck-packages.steamos.cloud/archlinux-mirror/sources/jupiter-main/zenity-light-3.32.0%2B55%2Bgd7bedff6-1.src.tar.gz
```

See: [srcpkg2git](https://gitlab.com/evlaV/srcpkg2git) for detailed usage instructions.

### Manually Deobfuscate Source Package

| Path | Description |
| :--- | :--- |
| `$pkgname_dir_1`/ | Main package directory |
| ┣━.SRCINFO | Pacman package (AUR) metadata |
| ┣━PKGBUILD | Pacman package build information |
| **┗━`$pkgname_dir_2`/** | **Obfuscated package git** (directory name (`$pkgname_dir_2`) may be **identical to** or **differ from** (i.e., a derivative of) **main package directory** name (`$pkgname_dir_1`)) |

```sh
# (at your discretion) proceed to download and extract respective source package to current working directory ($PWD) (e.g., zenity-light)
curl https://steamdeck-packages.steamos.cloud/archlinux-mirror/sources/jupiter-main/zenity-light-3.32.0%2B55%2Bgd7bedff6-1.src.tar.gz | bsdtar -xvf-

# respective package directory name(s) (e.g., zenity-light and zenity)
pkgname_dir_1=zenity-light
pkgname_dir_2=zenity

# deobfuscate
cd "$pkgname_dir_1/$pkgname_dir_2"
mkdir -p ".git/"
mv * ".git/"
git init
git checkout -f
```

---

*Thank you [**Valve**](https://www.valvesoftware.com/en/about), [**Collabora**](https://www.collabora.com/about-us/), and [**Igalia**](https://www.igalia.com/about/)*; *I know how truly **difficult** and **lengthy/time-consuming** GitLab repository publication can be*; [the following how-to](#-gitlab-repository-private-public-how-to-) was made specifically for you:

## 😉 GitLab Repository Private-Public How-to 😂

The following must be conducted per [GitLab repository](https://gitlab.steamos.cloud/):

1. Left click `Settings` or mouseover `Settings` then left click `General` (`Settings > General`)
2. Expand `Visibility, project features, permissions` (left click `Expand`)
3. Change `Project visibility` dropdown menu option from `Private` to `Public` (left click `Private` then left click `Public`)
4. Left click `Save changes`

Here's a visual example using `jupiter-hw-support`:

<img alt="GitLab Private-Public How-to" src="images/gitlab-private-public-how-to.gif" width="512">

Maybe [**Valve**](https://www.valvesoftware.com/en/about), [**Collabora**](https://www.collabora.com/about-us/), and [**Igalia**](https://www.igalia.com/about/) will perform this *daunting* task in ~~2022~~ ~~2023~~ **2024** (?) 🤞

## 💭 Thoughts/Opinions 💡

**[We Become What We Behold](https://ncase.itch.io/wbwwb) | [Deutsch (German)](http://ncase.me/wbwwb-de/) | [한국어 (Korean)](https://game.hyeon.me/wbwwb/) | [Italiano (Italian)](http://ncase.me/sccv/) | [العربية (Arabic)](https://alexclay.itch.io/wbwwb-ar) | [Русский (Russian)](http://sila.media/game/) | [Hrvatski (Croatian)](http://www.varljiv.org/postajemo-sto-gledamo) | [Polski (Polish)](https://mleko-forks.github.io/wbwwb/) | [中文 (Chinese)](https://claycoffee.github.io/wbwwb/) | [中文（臺灣）(Taiwanese Chinese)](https://io.cloudxact.com/ncase/wbwwb/) | [Français (French)](https://samuelhackwill.github.io/wbwwb/) | [עברית (Hebrew)](https://ozomer.github.io/wbwwb/)**

<img alt="Wage Slavery" src="images/wage-slavery.jpg" width="512">

> [**When the entirety of your earnings are exhausted on food and shelter, your labors are no longer viewed as an opportunity for economic advancement, but rather as an act of self-preservation. In the real world, that's called SLAVERY.**](https://en.wikipedia.org/wiki/Wage_slavery)

---

[![My Dinner With Andre (1981) - We Are Bored](https://img.youtube.com/vi/j8v_XqFO8Bc/0.jpg)](https://www.youtube.com/watch?v=j8v_XqFO8Bc)

[**My Dinner With Andre (1981) - We Are Bored**](https://www.youtube.com/watch?v=j8v_XqFO8Bc)

[![My Dinner With Andre (1981) - Full Movie](https://img.youtube.com/vi/M_YyCVIXmig/0.jpg)](https://www.youtube.com/watch?v=M_YyCVIXmig) [![My Dinner With Andre (1981) - Full Movie](https://img.youtube.com/vi/O4lvOjiHFw0/0.jpg)](https://www.youtube.com/watch?v=O4lvOjiHFw0)

[**My Dinner With Andre (1981) - Full Movie**](https://www.youtube.com/watch?v=M_YyCVIXmig) ([**secondary**](https://www.youtube.com/watch?v=O4lvOjiHFw0))

---

[![Manufacturing Consent: Noam Chomsky and the Media](https://img.youtube.com/vi/BQXsPU25B60/0.jpg)](https://www.youtube.com/watch?v=BQXsPU25B60)

[**Manufacturing Consent: Noam Chomsky and the Media**](https://www.youtube.com/watch?v=BQXsPU25B60)

---

[![The Corporation](https://img.youtube.com/vi/6v8e7dUwq_Q/0.jpg)](https://www.youtube.com/watch?v=6v8e7dUwq_Q) [![The Corporation](https://img.youtube.com/vi/dpjypnxnS4U/0.jpg)](https://www.youtube.com/watch?v=dpjypnxnS4U)

[**The Corporation**](https://www.youtube.com/watch?v=6v8e7dUwq_Q) ([**secondary**](https://www.youtube.com/watch?v=dpjypnxnS4U))

[![The New Corporation: The Unfortunately Necessary Sequel](https://img.youtube.com/vi/qorqTW-BKdI/0.jpg)](https://www.youtube.com/watch?v=qorqTW-BKdI)

[**The New Corporation: The Unfortunately Necessary Sequel**](https://www.youtube.com/watch?v=qorqTW-BKdI)

---

[![A Perfect Circle - So Long, And Thanks For All The Fish](https://img.youtube.com/vi/UkHSmDxX1t4/0.jpg)](https://www.youtube.com/watch?v=UkHSmDxX1t4)

[**A Perfect Circle - So Long, And Thanks For All The Fish**](https://www.youtube.com/watch?v=UkHSmDxX1t4) | [**Download (webm)**](https://www.mediafire.com/file/evgx0vuog0lycvd/A_Perfect_Circle_-_So_Long%252C_And_Thanks_For_All_The_Fish_%2528Official_Video%2529.webm)

---

[![Propaganda (The West Through The Eyes of North Korea)](https://img.youtube.com/vi/OyFH19nm2e4/0.jpg)](https://www.youtube.com/watch?v=OyFH19nm2e4)

[**Propaganda (The West Through The Eyes of North Korea)**](https://www.youtube.com/watch?v=OyFH19nm2e4) | [**Download (webm)**](https://www.mediafire.com/file/rss5h34vmczdznf/The_West_Through_The_Eyes_of_North_Korea.webm)

---

[![HyperNormalisation](https://img.youtube.com/vi/to72IJzQT5k/0.jpg)](https://www.youtube.com/watch?v=to72IJzQT5k)

[**HyperNormalisation**](https://www.youtube.com/watch?v=to72IJzQT5k)

---

[![Everything is a Rich Man's Trick](https://img.youtube.com/vi/4oVpt_I9iQQ/0.jpg)](https://www.youtube.com/watch?v=4oVpt_I9iQQ)

[**Everything is a Rich Man's Trick**](https://www.youtube.com/watch?v=4oVpt_I9iQQ)

---

[![Zeitgeist: The Movie](https://img.youtube.com/vi/XVYlxHteUMs/0.jpg)](https://www.youtube.com/watch?v=XVYlxHteUMs) [![Zeitgeist: The Movie](https://img.youtube.com/vi/LPhANpsR1gM/0.jpg)](https://www.youtube.com/watch?v=LPhANpsR1gM)

[**Zeitgeist: The Movie**](https://www.youtube.com/watch?v=XVYlxHteUMs) ([**secondary**](https://www.youtube.com/watch?v=LPhANpsR1gM))

[![Zeitgeist: Addendum](https://img.youtube.com/vi/xNaGy_jRGZo/0.jpg)](https://www.youtube.com/watch?v=xNaGy_jRGZo) [![Zeitgeist: Addendum](https://img.youtube.com/vi/6nSwT_5GoJk/0.jpg)](https://www.youtube.com/watch?v=6nSwT_5GoJk)

[**Zeitgeist: Addendum**](https://www.youtube.com/watch?v=xNaGy_jRGZo) ([**secondary**](https://www.youtube.com/watch?v=6nSwT_5GoJk))

[![Zeitgeist: Moving Forward](https://img.youtube.com/vi/5uUA2wTBblo/0.jpg)](https://www.youtube.com/watch?v=5uUA2wTBblo) [![Zeitgeist: Moving Forward](https://img.youtube.com/vi/mboDCYyFxW0/0.jpg)](https://www.youtube.com/watch?v=mboDCYyFxW0)

[**Zeitgeist: Moving Forward**](https://www.youtube.com/watch?v=5uUA2wTBblo) ([**secondary**](https://www.youtube.com/watch?v=mboDCYyFxW0))

---

[![Eating Our Way to Extinction](https://img.youtube.com/vi/LaPge01NQTQ/0.jpg)](https://www.youtube.com/watch?v=LaPge01NQTQ)

[**Eating Our Way to Extinction**](https://www.youtube.com/watch?v=LaPge01NQTQ)

[![King Corn](https://img.youtube.com/vi/tbRHGHYMGpU/0.jpg)](https://www.youtube.com/watch?v=tbRHGHYMGpU)

[**King Corn**](https://www.youtube.com/watch?v=tbRHGHYMGpU)

---

[![Indoctrinate U](https://img.youtube.com/vi/WHyvRHrYYBA/0.jpg)](https://www.youtube.com/watch?v=WHyvRHrYYBA)

[**Indoctrinate U**](https://www.youtube.com/watch?v=WHyvRHrYYBA)

---

[![Dumb Americans | George Carlin | Life is Worth Losing (2005)](https://img.youtube.com/vi/KLODGhEyLvk/0.jpg)](https://www.youtube.com/watch?v=KLODGhEyLvk)

[**Dumb Americans | George Carlin | Life is Worth Losing (2005)**](https://www.youtube.com/watch?v=KLODGhEyLvk)

---

[![Ren - Eden](https://img.youtube.com/vi/NdSLsRKnafI/0.jpg)](https://www.youtube.com/watch?v=NdSLsRKnafI)

[**Ren - Eden**](https://www.youtube.com/watch?v=NdSLsRKnafI) | [**Download (webm)**](https://www.mediafire.com/file/f50ma3n0h4pmvdf/Ren+-+Eden.webm)

[![Ren - Money Game](https://img.youtube.com/vi/0ivQwwgW4OY/0.jpg)](https://www.youtube.com/watch?v=0ivQwwgW4OY)

[**Ren - Money Game**](https://www.youtube.com/watch?v=0ivQwwgW4OY)

[![Ren - Money Game Part 2](https://img.youtube.com/vi/YonS9_QJbp8/0.jpg)](https://www.youtube.com/watch?v=YonS9_QJbp8)

[**Ren - Money Game Part 2**](https://www.youtube.com/watch?v=YonS9_QJbp8)

[![Ren - Money Game Part 3](https://img.youtube.com/vi/nyWbun_PbTc/0.jpg)](https://www.youtube.com/watch?v=nyWbun_PbTc)

[**Ren - Money Game Part 3**](https://www.youtube.com/watch?v=nyWbun_PbTc)

---

[![Cream by David Firth](https://img.youtube.com/vi/0UgiJPnwtQU/0.jpg)](https://www.youtube.com/watch?v=0UgiJPnwtQU)

[**Cream by David Firth**](https://www.youtube.com/watch?v=0UgiJPnwtQU)

---

[![In The Fall](https://img.youtube.com/vi/A-rEb0KuopI/0.jpg)](https://www.youtube.com/watch?v=A-rEb0KuopI)

[**In The Fall**](https://www.youtube.com/watch?v=A-rEb0KuopI)

[![MAN](https://img.youtube.com/vi/WfGMYdalClU/0.jpg)](https://www.youtube.com/watch?v=WfGMYdalClU)

[**MAN**](https://www.youtube.com/watch?v=WfGMYdalClU)

[![Happiness](https://img.youtube.com/vi/e9dZQelULDk/0.jpg)](https://www.youtube.com/watch?v=e9dZQelULDk)

[**Happiness**](https://www.youtube.com/watch?v=e9dZQelULDk)

[![The Turning Point](https://img.youtube.com/vi/p7LDk4D3Q3U/0.jpg)](https://www.youtube.com/watch?v=p7LDk4D3Q3U)

[**The Turning Point**](https://www.youtube.com/watch?v=p7LDk4D3Q3U)

[![Are You Lost In The World Like Me - Steve Cutts](https://img.youtube.com/vi/4yBrW0zG8y8/0.jpg)](https://www.youtube.com/watch?v=4yBrW0zG8y8)

[**Are You Lost In The World Like Me - Steve Cutts**](https://www.youtube.com/watch?v=4yBrW0zG8y8) | [**Download (webm)**](https://www.mediafire.com/file/z0h6k5pz58c0axq/Are_You_Lost_In_The_World_Like_Me_-_Steve_Cutts.webm)

---

[![Stress, Portrait of a Killer](https://img.youtube.com/vi/eYG0ZuTv5rs/0.jpg)](https://www.youtube.com/watch?v=eYG0ZuTv5rs)

[**Stress, Portrait of a Killer**](https://www.youtube.com/watch?v=eYG0ZuTv5rs)

---

[**Daily Sea Surface Temperature**](https://climatereanalyzer.org/clim/sst_daily/) | [**Daily Surface Air Temperature**](https://climatereanalyzer.org/clim/t2_daily/?dm_id=world)

[**U.S. Field Production of Crude Oil**](https://www.eia.gov/dnav/pet/hist/LeafHandler.ashx?n=PET&s=MCRFPUS2&f=M) | [**U.S. Crude Oil Production**](https://www.eia.gov/dnav/pet/PET_CRD_CRPDN_ADC_MBBLPD_M.htm) | [**U.S. Dry Natural Gas Production**](https://www.eia.gov/dnav/ng/hist/n9070us2m.htm) | [**U.S. Supply and Disposition**](https://www.eia.gov/dnav/pet/PET_SUM_SND_D_NUS_MBBLPD_M_CUR.htm)

[![Natural Gas Is Scamming America | Climate Town](https://img.youtube.com/vi/K2oL4SFwkkw/0.jpg)](https://www.youtube.com/watch?v=K2oL4SFwkkw)

[**Natural Gas Is Scamming America | Climate Town**](https://www.youtube.com/watch?v=K2oL4SFwkkw)

---

[![The largest campaign ever to stop publishers destroying games](https://img.youtube.com/vi/w70Xc9CStoE/0.jpg)](https://www.youtube.com/watch?v=w70Xc9CStoE)

[**The largest campaign ever to stop publishers destroying games**](https://www.youtube.com/watch?v=w70Xc9CStoE)

[**TAKE ACTION - STOP KILLING GAMES (STOPKILLINGGAMES.COM)**](https://www.stopkillinggames.com/)

1. File complaints (if you can)
2. Sign petitions (if you can)
3. Get others to do 1 and 2

---

[![Algorithms are breaking how we think](https://img.youtube.com/vi/QEJpZjg8GuA/0.jpg)](https://www.youtube.com/watch?v=QEJpZjg8GuA)

[**Algorithms are breaking how we think**](https://www.youtube.com/watch?v=QEJpZjg8GuA)

---

**The streets** (i.e., **the people**) are **littered with dynamite**; look no further than **within yourself** for the **perpetrators/oppressors** and **solutions**.

To all the **soulful individuals/people**:

- **value yourself**
  - **withhold your labor**
    - **["lying flat" (躺平, tǎng píng), "a chive lying flat is difficult to reap" (躺平的韭菜不好割, tǎng píng de jiǔcài bù hǎo gē)](https://en.wikipedia.org/wiki/Tang_ping) | [Great Resignation](https://en.wikipedia.org/wiki/Great_Resignation)**
    - the cynic in me resonates with **"let it rot" (摆烂, bǎi làn), "to actively embrace a deteriorating situation, rather than trying to turn it around" - it's a difficult and delicate balance/dance of hopefulness and despair, optimism and cynicism**
  - **limit/reduce your consumption/reliances**
  - **recognize and resist (the endless barrage of) distractions/delusions**
  - **promote local engagements (i.e., locally exchange and organize)**
  - **challenge/resist conformity**
- **become fearless**
  - **free yourself from apathy and dogmatism**
  - **breed/cultivate community, empathy, and solidarity**
  - **question all forms of "authority"**
- ***abstain from procreation*** - *contrary to what people like [Elon Musk and Jeff Bezos](https://fortune.com/2023/12/16/jeff-bezos-elon-musk-human-population-outer-space-mars-spacex-blue-origin/) say...*
  - in reality; if **we had a trillion humans (1,000,000,000,000)**, we would have **more of the status quo**; **more competition**, **more inequality**, **more war and violence**, **more poverty and precariousness**, **more corruption and exploitation**, an **even greater (insurmountable) negative impact on the people and environment/planet**, and **further limited resources**; but the **rich would be richer!**
  - **if the rich would like humanity to procreate, (allow humanity to) improve humanities conditions and humanity will happily/naturally procreate! The fact that humanity isn't as interested in procreating says a lot about humanities current (and future) conditions!**
  - **I can't comprehend such an ignorant and disconnected statement/barometer - there's only one (1) Mozart, one (1) Einstein, and that's all there ever will be - I presume these disconnected/distorted views develop when you spend too much time "above us" up in the stars - it's about time they come back down to reality, to mediocrity with the rest of us! I often wonder how many creative, brilliant, and talented individuals formerly/presently exist who were/are unfortunately too preoccupied with basic survival instead of nurturing and contributing their individual creative beauty to the betterment of society.**

To all the **soulless corporations—our "leaders"—our owners**, **look out! A storm is brewing** in the **hearts and minds** of **those you supposedly serve**; **collapse is coming!** [**There's nowhere to run**](https://www.wired.com/story/mark-zuckerberg-inside-hawaii-compound/), [**nowhere to hide!**](https://www.theguardian.com/commentisfree/2023/dec/21/mark-zuckerberg-apocalypse-bunker-hawaii)

---

*We need to **wake up** and **stop asking**, **stop expecting** **soulless corporations—our "leaders"—our owners** for things we should be **doing** and **providing** ourselves. **Stop sleeping, start doing! Stop asking, start doing! Stop expecting, start doing! Stop hoping, start doing!** **Stop voting for and supporting the status quo** with your **labor**, with your **money**, and at the **ballot box**, **start doing!***

***Real change comes from us; from the bottom up, not from the top down.** These **soulless corporations—our "leaders"—our owners** **don't care about any of us (the enslaved public)**, they only value the **resources they pillage**, our **labor**, and our **money**; they only value what privately benefits themselves, not the public. **And we don't disappoint; we senselessly divide, compete, and sell ourselves (for far less than we're worth) rather than collaborate.***

**[These public mirrors (@gitlab.com/evlaV)](https://gitlab.com/users/evlaV/projects) are a small example of doing; I took it upon myself to do for the public what Valve couldn't be bothered to do themselves (despite my prolonged protest the past over 2 years). There's no money in benefitting the public, so to Valve (like our "leaders"—our owners), it's not worth doing. Valve only cares that their consumer market keeps buying [DRM](https://en.wikipedia.org/wiki/Digital_rights_management) riddled digital games which nobody owns. [You'll own nothing and (you'll) be happy](https://en.wikipedia.org/wiki/You%27ll_own_nothing_and_be_happy). Welcome to the world of tomorrow, today! Valve would rather waste their time, talent, and money making [a fucking useless, self promoting orb of Steam Deck OLEDs](https://www.youtube.com/watch?v=XpExNYMxALE), meanwhile the global crisis continues/progresses. [VALVE IS BAD, ACTUALLY!](https://www.youtube.com/watch?v=3r0a7-qyjss) [Why Valve Are Not The Good Guys](https://www.youtube.com/watch?v=eq8zjrsNK_U)**

*Thank you [Jacqui Irwin](https://a42.asmdc.org/press-releases/20240916-assemblymember-irwin-urges-governor-sign-legislation-increasing) for [AB 2426](https://leginfo.legislature.ca.gov/faces/billTextClient.xhtml?bill_id=202320240AB2426), which (finally) **forces digital storefronts (e.g., Valve/Steam, Nintendo, Sony/PlayStation, and Microsoft/Xbox) to be more transparent (no more false advertising to consumers claiming they actually buy/purchase or own a thing from your digital storefront; in reality they only purchase an expirable/temporary and easily revokable license).** Someone who steals ("pirates") or [cracks](https://en.wikipedia.org/wiki/Software_cracking) digital games has more ownership than a digital storefront consumer (e.g., Valve/Steam, Nintendo, Sony/PlayStation, and Microsoft/Xbox), and that's pretty pathetic. **Stop buying temporary and easily revokable digital licenses from [DRM](https://en.wikipedia.org/wiki/Digital_rights_management) riddled digital storefronts (e.g., Valve/Steam, Nintendo, Sony/PlayStation, and Microsoft/Xbox). Valve/Steam has (unfortunately) already massively profited from and (mostly unfettered) contributed to killing physical games for the PC market ([He's dead, Jim](https://en.wikipedia.org/wiki/Leonard_McCoy#%22He's_dead,_Jim.%22)); Nintendo, Sony/PlayStation, and Microsoft/Xbox aren't far behind...***

---

> **[In 2018, Valve was raking in, at minimum, over $780,400 net income per employee](https://www.pcgamer.com/gaming-industry/in-2018-a-group-of-valve-staff-tried-to-figure-out-just-how-efficient-they-were-beingand-found-they-were-making-more-money-per-head-than-apple-facebook-and-nearly-every-tech-giant-out-there/)**

> **[Here’s how much Valve pays its staff — and how few people it employs](https://www.theverge.com/2024/7/13/24197477/valve-employs-few-hundred-people-payroll-redacted)**

Valve has commercialized a product (SteamOS 3.x / Steam Deck) which almost exclusively relies on and utilizes (free/libre) open source software. Software myself and countless others have contributed to for decades. Valve has capitalized/profited immensely from (free/libre) open source software, and in turn has not been very friendly to the (free/libre) open source software community. This is why (in protest) I set up these public mirrors on April 1, 2022. **I chose April Fools' Day as a nod to the foolishness of Valve regarding publication.**

Valve likes to distance themselves from proprietary technology companies (e.g., Microsoft Windows) but Valve SteamOS isn't much different; SteamOS is a privately developed OS, with proprietary software and [DRM](https://en.wikipedia.org/wiki/Digital_rights_management) (Steam) front and center, consisting of mostly public sources ([the majority of which are GPL](#gpl-package-research)). The Steam Deck additionally contains proprietary hardware, which includes additional proprietary software (this is also true for the [Steam Deck Docking Station](https://www.steamdeck.com/en/dock)). This can thankfully be (mostly) alleviated/liberated by [OpenSD](https://codeberg.org/OpenSD/opensd) (thank you ~~@seek-dev~~ [@evilichi (Daniel Nguyen)](https://codeberg.org/evilichi)).

Paired with Valve's deliberate privatization of their GitLab repositories and poor interactions regarding publication (over **2 years** later) makes it comparatively easy to recommend the [ASUS ROG Ally](https://rog.asus.com/gaming-handhelds/rog-ally/rog-ally-2023/) (June 13, 2023). Who doesn't love a global launch? Moreover, the similar nature of [publication date](https://www.theverge.com/2023/4/1/23666084/asus-rog-ally-handheld-windows-gaming-portable) (April Fools' Day). And like the Steam Deck, you're free to install **YOUR CHOICE of OS** (or use pre-installed Windows 11). Microsoft Windows and Valve SteamOS are pretty comparable to me, though I'd argue Windows 11 behaves as expected outside of gaming (desktop use). SteamOS 3.x / Steam Deck does not behave or perform anything like Arch / GNU/Linux. It's tragic knowing some people's first/early interactions with GNU/Linux will be via SteamOS. What a terrible/inaccurate representation of GNU/Linux (to say the least). Regardless, I'd recommend the same for both (Steam Deck and ROG Ally): take advantage of the hardware and install something (anything) else.

I'd advocate the openness of Valve and the Steam Deck, but I unfortunately cannot attest to that. SteamOS 3.x is a terrible (and I honestly think worthy of the title "worst") distribution of Linux (e.g., SteamOS 3.x doesn't comply with [Arch Linux](https://archlinux.org/about/) or [GNU/Linux](https://www.gnu.org/philosophy/philosophy.html) philosophies), with Steam (proprietary and [DRM](https://en.wikipedia.org/wiki/Digital_rights_management)) front and center, all else is secondary (or nonexistent); this unfortunately includes the (free/libre) open source software community, who are the very foundation which SteamOS is built upon. Look at the polarity between SteamOS 3.x / Steam Deck Gaming mode versus Desktop (and Recovery) mode to see where Valve's focus lies. The majority of Steam Deck updates enhance Steam / Gaming mode. 'Switch to Desktop' is the bottom/last Power option (excluding 'Cancel'). 'Return to Gaming Mode' (the mode the system (always) boots to) is the first (primary) Desktop option. SteamOS 3.x / Steam Deck is buttery smooth and pleasant in Steam / Gaming mode, compared to rough/painful (to say the least) in Desktop mode.

SteamOS is rather easy to [soft brick](https://en.wikipedia.org/wiki/Brick_(electronics)#Soft_brick) (I've soft bricked SteamOS more times than I'd like to admit). SteamOS (jupiter) lacks a C/C++ compiler (e.g., GCC / glibc or Clang), doesn't have a very expansive/functional instance of Python (e.g., no Python package / pip support), can't traditionally install packages/software or retain modifications (immutable), and has a weird bootloader (e.g., dual booting). Dual booting can be elegantly resolved by [SteamDeck-Clover-Dualboot](https://github.com/ryanrudolfoba/SteamDeck-Clover-Dualboot) (thank you [@ryanrudolfoba](https://github.com/ryanrudolfoba)).

It's apparent Valve doesn't expect (or prioritize) the use of Desktop mode. A huge and completely missed opportunity for Valve, SteamOS, Steam Deck, GNU/Linux, and the entire community. I think GNOME would be a much better desktop environment, especially for touch screen devices like the Steam Deck (or any portable for that matter), but I believe Valve chose KDE due to their [special (financial) interests](https://www.phoronix.com/news/Valve-Funding-KWin-Work). So the Steam Deck and the community suffer only to "justify" Valve's investments in KDE development. Money talks. Why not embrace the many nuances of Arch / GNU/Linux (and its users) and enable the use of any (Arch supported) desktop environment? ["Be together. Not the same."](https://nexusstudios.com/work/be-together-not-the-same/)

Many of Valve's commercial/hardware projects/products fail (e.g., SteamOS 1 and 2, Steam Link, and Steam Controller); the truly sad thing is Valve's ideas are generally good (on paper), but (as Valve has repeatedly demonstrated) are extremely poorly managed and executed by uncoordinated, unmotivated, ignorant, incompetent, and **unethical** people. I believe this is a side effect of Valve's ['flat hierarchy' facade](https://www.theguardian.com/commentisfree/2018/jul/30/no-bosses-managers-flat-hierachy-workplace-tech-hollywood). I predict SteamOS 3.x will have many of the same issues (and more) SteamOS 1 and 2 had. This is further compounded by Valve following/projecting a similar canonical/conventional (milestone) ideology (from Debian) to Arch. **This concept/implementation is deeply flawed, incompatible, and conflicts with Arch's bleeding edge rolling release! Asinine!**

**SteamOS is technically Linux; oriented to directly benefit Valve (primarily/solely promoting itself (Steam) and [DRM](https://en.wikipedia.org/wiki/Digital_rights_management)); Valve is very much like any other corporation who markets proprietary/non-free/closed-source software/OS (e.g., Apple macOS and Microsoft Windows). SteamOS possesses none of the [philosophy of GNU](https://www.gnu.org/philosophy/philosophy.html). And deserves a proper (up-to-date) [GNU unendorsement](https://www.gnu.org/distros/common-distros.html).**

Valve clearly aren't interested in public access to their code or contributions. I'm honestly questioning if Valve will ever publicize their GitLab repositories or open SteamOS development to the public (contributions and issues). I'll likely be maintaining these public mirrors for the foreseeable future...

This is not ideal, as I (an unaffiliated third party) should not be trusted to host Valve's entire SteamOS 3.x / Steam Deck [codebase](https://en.wikipedia.org/wiki/Codebase). I haven't made any alterations, except (when necessary) pointing to [these unofficial public mirrors](https://gitlab.com/evlaV) instead of [Valve's official private GitLab repositories](https://gitlab.steamos.cloud/). Users of these unofficial public mirrors have to trust me, and there isn't an easy or intuitive way to verify the code on these public mirrors hasn't been tampered with (because Valve's official GitLab repositories are still private). With skepticism, I hope these public mirrors serve as the de facto place to source, verify, and follow official SteamOS 3.x / Steam Deck software/updates.

I've graciously done Valve's ("last mile") source code distribution job/work duty (free of charge) for over **2 years** now, and will continue to do so for the foreseeable future, hopefully filling the void left from Valve. Through this void filling effort, I've interacted with several developers of projects who have benefited from these public mirrors. From individual/add-on projects like [SteamOS Btrfs](https://gitlab.com/popsulfr/steamos-btrfs) (thank you [@popsulfr (Philipp Richter)](https://gitlab.com/popsulfr)) to entire replacement Linux distributions like [batocera](https://batocera.org/), [bazzite](https://github.com/ublue-os/bazzite), and [ChimeraOS](https://chimeraos.org/). That's precisely the purpose of this project; to aid, encourage, and inspire SteamOS 3.x / Steam Deck contributors/developers/tinkerers. If you have any questions or comments, [write to me](mailto:valve.evlav@proton.me).

---

[**Larry Sanger** (co-founder of Wikipedia)](https://en.wikipedia.org/wiki/Larry_Sanger)

---

[**Externalism**](https://en.wikipedia.org/wiki/Externalism)

[**Externality**](https://en.wikipedia.org/wiki/Externality)

[**Social murder**](https://en.wikipedia.org/wiki/Social_murder) | [**Delay, Deny, Defend**](https://en.wikipedia.org/wiki/Delay,_Deny,_Defend) | *"Delay, Deny, Depose"* ― [**Luigi Mangione**](https://en.wikipedia.org/wiki/Luigi_Mangione)

[**Propaganda of/by the deed**](https://en.wikipedia.org/wiki/Propaganda_of_the_deed)

---

[**Neoliberalism**](https://en.wikipedia.org/wiki/Neoliberalism) - [**Post/Anti-Neoliberalism**](https://en.wikipedia.org/wiki/Socialism_of_the_21st_century#Post-neoliberalism) | [**Neoconservatism**](https://en.wikipedia.org/wiki/Neoconservatism)

---

[**Imperialism**](https://en.wikipedia.org/wiki/Imperialism) | [**Vanguardism**](https://en.wikipedia.org/wiki/Vanguardism)

---

[**Feudalism**](https://en.wikipedia.org/wiki/Feudalism)

[**Capitalist state**](https://en.wikipedia.org/wiki/Capitalist_state)

[**Pyramid of Capitalist System**](https://en.wikipedia.org/wiki/Pyramid_of_Capitalist_System) | [**Vanguardism**](https://en.wikipedia.org/wiki/Vanguardism)

[**Criticism of capitalism**](https://en.wikipedia.org/wiki/Criticism_of_capitalism)

---

[**Accelerationism**](https://en.wikipedia.org/wiki/Accelerationism)

---

[**George Washington's Farewell Address (1796)**](https://www.georgewashington.org/farewell-address.jsp) ([**wiki**](https://en.wikipedia.org/wiki/George_Washington's_Farewell_Address))

---

[**Plutocracy**](https://en.wikipedia.org/wiki/Plutocracy)

[**Colonialism**](https://en.wikipedia.org/wiki/Colonialism)

[**Settler Colonialism**](https://en.wikipedia.org/wiki/Settler_colonialism)

---

[**Bank War**](https://en.wikipedia.org/wiki/Bank_War)

[**Fiat money**](https://en.wikipedia.org/wiki/Fiat_money)

[**Federal Reserve**](https://en.wikipedia.org/wiki/Federal_Reserve) - [**Federal Reserve Act**](https://en.wikipedia.org/wiki/Federal_Reserve_Act)

[**World Bank**](https://en.wikipedia.org/wiki/World_Bank) - [**International Monetary Fund**](https://en.wikipedia.org/wiki/International_Monetary_Fund)

---

[**Operation Northwoods**](https://en.wikipedia.org/wiki/Operation_Northwoods) | [**False flag**](https://en.wikipedia.org/wiki/False_flag)

---

[**Perception management**](https://en.wikipedia.org/wiki/Perception_management) | [**Truth projection (Propaganda)**](https://en.wikipedia.org/wiki/Propaganda) | [**(Military) deception**](https://en.wikipedia.org/wiki/Military_deception) | [**Psychological warfare**](https://en.wikipedia.org/wiki/Psychological_warfare)

---

[**Politics and technology**](https://en.wikipedia.org/wiki/Politics_and_technology)

---

[**ELIZA**](https://en.wikipedia.org/wiki/ELIZA) | [**Artificial Intelligence (AI)**](https://en.wikipedia.org/wiki/Artificial_intelligence)

---

[**Monroe Doctrine**](https://en.wikipedia.org/wiki/Monroe_Doctrine)

---

[**Alien and Sedition Acts**](https://en.wikipedia.org/wiki/Alien_and_Sedition_Acts)

---

[**Operation Paperclip**](https://en.wikipedia.org/wiki/Operation_Paperclip) | [**Molotov–Ribbentrop Pact**](https://en.wikipedia.org/wiki/Molotov%E2%80%93Ribbentrop_Pact) - [**Horseshoe theory**](https://en.wikipedia.org/wiki/Horseshoe_theory)

---

[**Horace Mann**](https://en.wikipedia.org/wiki/Horace_Mann)

*"Arguing that universal public education was the best way to turn unruly American children into disciplined, judicious republican citizens, Mann won widespread approval from modernizers, especially in the Whig Party, for building public schools. Most U.S. states adopted a version of the system Mann established in Massachusetts, especially the program for normal schools to train professional teachers. Educational historians credit Horace Mann, along with Henry Barnard and Catharine Beecher, as one of the major advocates of the Common School Movement."*

[**Horace Mann - Emulation of the Prussian system**](https://en.wikipedia.org/wiki/Horace_Mann#Emulation_of_the_Prussian_system)

*"Upon becoming the secretary of education of Massachusetts in 1837, Mann worked to create a statewide system of professional teachers, based on the Prussian model of "common schools." Prussia was attempting to develop a system of education by which all students were entitled to the same content in their public classes. Mann initially focused on elementary education and on training teachers. The common-school movement quickly gained strength across the North. Connecticut adopted a similar system in 1849, and Massachusetts passed a compulsory attendance law in 1852. Mann's crusading style attracted wide middle-class support."*

*"An important technique which Mann had learned in Prussia and introduced in Massachusetts in 1848 was to place students in grades by age. They were assigned by age to different grades and progressed through them, regardless of differences of aptitude. In addition, he used the lecture method common in European universities, which required students to receive professional instruction rather than teach one another. Previously, schools had often had groups of students who ranged in age from 6 to 14 years. With the introduction of age grading, multi-aged classrooms all but disappeared. Some students progressed with their grade and completed all courses the secondary school had to offer. These were "graduated," and were awarded a certificate of completion. This was increasingly done at a ceremony imitating college graduation rituals."*

*"Arguing that universal public education was the best way to turn the nation's unruly children into disciplined, judicious republican citizens, Mann won widespread approval for building public schools from modernizers, especially among fellow Whigs. Most states adopted one version or another of the system he established in Massachusetts, especially the program for "normal schools" to train professional teachers. This quickly developed into a widespread form of school which later became known as the factory model school."*

---

[**Julius Martov Archive**](https://www.marxists.org/archive/martov/index.htm)

---

*"The truly intelligent person is one who can pretend to be a fool in front of a fool who pretends to be intelligent."* ―

---

*"Amicus meus, inimicus inimici mei (my friend, the enemy of my enemy). i.e., The enemy of my enemy is my friend."* ― [**Ancient (Latin) Proverb**](https://en.wikipedia.org/wiki/The_enemy_of_my_enemy_is_my_friend)

---

*"Until the lion learns to write, every story will glorify the hunter."* ― **African Proverb**

---

*"Not everything that is faced can be changed; but nothing can be changed until it is faced."*  ― **James Baldwin, [As Much Truth As One Can Bear (January 14, 1962)](https://tweetsofanativeson.com/pdf/As-Much-Truth-As-One-Can-Bear.pdf)**

*"I think we're more panic stricken. I walk through these cities, the cities of the western world. London, New York, and sometimes it resembles the most poverty stricken cities of the East. There's been a breakdown, a betrayal of the social contract in western life. And people are grabbing for things and holding onto what they think they can get and stepping all over their neighbors because they are panic stricken. When people do that, they are reacting, something is beginning to crack. People hoard all this because they don't have anything else, but they don't really believe in it. And they kill to get it. That proves the moral bankrupcy, which translates itself into actual bankruptcy of the world in which we live. We've yet to understand that if I'm starving, you are in danger. When people think that my danger makes them safe, they're in trouble."*  ― **James Baldwin**

*"The most dangerous creation of any society is the man who has nothing to lose."*  ― **James Baldwin, The Fire Next Time**

*"Life is tragic simply because the earth turns and the sun inexorably rises and sets, and one day, for each of us, the sun will go down for the last, last time. Perhaps the whole root of our trouble, the human trouble, is that we will sacrifice all the beauty of our lives, will imprison ourselves in totems, taboos, crosses, blood sacrifices, steeples, mosques, races, armies, flags, nations, in order to deny the fact of death, the only fact we have. It seems to me that one ought to rejoice in the fact of death—ought to decide, indeed, to earn one's death by confronting with passion the conundrum of life. One is responsible for life: It is the small beacon in that terrifying darkness from which we come and to which we shall return."*  ― **James Baldwin, The Fire Next Time**

*"I have met only a very few people—and most of these were not Americans—who had any real desire to be free. Freedom is hard to bear. It can be objected that I am speaking of political freedom in spiritual terms, but the political institutions of any nation are always menaced and are ultimately controlled by the spiritual state of that nation. We are controlled here by our confusion, far more than we know, and the American dream has therefore become something much more closely resembling a nightmare, on the private, domestic, and international levels. Privately, we cannot stand our lives and dare not examine them; domestically, we take no responsibility for (and no pride in) what goes on in our country; and, internationally, for many millions of people, we are an unmitigated disaster. Whoever doubts this last statement has only to open his ears, his heart, his mind, to the testimony of—for example—any Cuban peasant or any Spanish poet, and ask himself what he would feel about us if he were the victim of our performance in pre-Castro Cuba or in Spain. We defend our curious role in Spain by referring to the Russian menace and the necessity of protecting the free world. It has not occurred to us that we have simply been mesmerized by Russia, and that the only real advantage Russia has in what we think of as a struggle between the East and the West is the moral history of the Western world. Russia's secret weapon is the bewilderment and despair and hunger of millions of people of whose existence we are scarecely aware. The Russian Communists are not in the least concerned about these people. But our ignorance and indecision have had the effect, if not of delivering them into Russian hands, of plunging them very deeply in the Russian shadow, for which effect—and it is hard to blame them—the most articulate among them, and the most oppressed as well, distrust us all the more... We are capable of bearing a great burden, once we discover that the burden is reality and arrive where reality is. Anyway, the point here is that we are living in an age of revolution, whether we will or no, and that America is the only Western nation with both the power, and, as I hope to suggest, the experience that may help to make these revolutions real and minimize the human damage."*  ― **James Baldwin, The Fire Next Time**

---

*"Your Honor, years ago I recognized my kinship with all living beings, and I made up my mind that I was not one bit better than the meanest on earth. I said then, and I say now, that while there is a lower class, I am in it, and while there is a criminal element, I am of it, and while there is a soul in prison, I am not free."* ― **Eugene V. Debs, [Upon Being Convicted of Violating the Sedition Act (September 18, 1918)](https://www.marxists.org/archive/debs/works/1918/court.htm)**

*"If you go to the city of Washington, and you examine the pages of the Congressional Directory, you will find that almost all of those corporation lawyers and cowardly politicians, members of Congress, and misrepresentatives of the masses—you will find that almost all of them claim, in glowing terms, that they have risen from the ranks to places of eminence and distinction. I am very glad I cannot make that claim for myself. I would be ashamed to admit that I had risen from the ranks. When I rise it will be with the ranks, and not from the ranks."* ― **Eugene V. Debs, [The Canton, Ohio Speech, Anti-War Speech (June 16, 1918)](https://www.marxists.org/archive/debs/works/1918/canton.htm)**

*"I am not a capitalist soldier; I am a proletarian revolutionist. I do not belong to the regular army of the plutocracy, but to the irregular army of the people. I refuse to obey any command to fight from the ruling class, but I will not wait to be commanded to fight for the working class. I am opposed to every war but one; I am for that war with heart and soul, and that is the world-wide war of social revolution. In that war I am prepared to fight in any way the ruling class may make necessary, even to the barricades."* ― **Eugene V. Debs, [In What War Shall I Take Up Arms and Fight? (September 11, 1915)](https://www.marxists.org/history/etol/newspape/themilitant/socialist-appeal-1939/v03n21/debs.htm)**

*"I may not be able to say all I think; but I am not going to say anything I do not think."* ― **Eugene V. Debs, [The Canton, Ohio Speech, Anti-War Speech (June 16, 1918)](https://www.marxists.org/archive/debs/works/1918/canton.htm)**

*"If it had not been for the discontent of a few fellows who had not been satisfied with their conditions, you would still be living in caves. Intelligent discontent is the mainspring of civilization. Progress is born of agitation. It is agitation or stagnation."*  ― **Eugene V. Debs**

*"I'd rather vote for something I want and not get it than vote for something I don't want, and get it."* ― **Eugene V. Debs**

*"The class which has the power to rob upon a large scale has also the power to control the government and legalize their robbery."* ― **Eugene V. Debs, [The Canton, Ohio Speech, Anti-War Speech (June 16, 1918)](https://www.marxists.org/archive/debs/works/1918/canton.htm)**

*"The Republican and Democratic parties, or, to be more exact, the Republican-Democratic party, represent the capitalist class in the class struggle. They are the political wings of the capitalist system and such differences as arise between them relate to spoils and not to principles."* ― **Eugene V. Debs, [The Socialist Party and the Working Class (September 1, 1904)](https://www.marxists.org/archive/debs/works/1904/sp_wkingclss.htm)**

*"I am opposing a social order in which it is possible for one man who does absolutely nothing that is useful to amass a fortune of hundreds of millions of dollars, while millions of men and women who work all the days of their lives secure barely enough for a wretched existence."* ― **Eugene V. Debs**

*"They tell us that we live in a great free republic; that our institutions are democratic; that we are a free and self-governing people. That is too much, even for a joke. ... Wars throughout history have been waged for conquest and plunder... And that is war in a nutshell. The master class has always declared the wars; the subject class has always fought the battles."* ― **Eugene V. Debs**

*"When we are in partnership and have stopped clutching each other's throats, when we have stopped enslaving each other, we will stand together, hands clasped, and be friends. We will be comrades, we will be brothers, and we will begin the march to the grandest civilization the human race has ever known."* ― **Eugene V. Debs**

*"I am guilty of believing that the human race can be humanized and enriched in every spiritual inference through the saner and more beneficent processes of peaceful persuasion applied to material problems rather than through wars, riots and bloodshed."* ― **Eugene V. Debs**

*"The truth has always been dangerous to the rule of the rogue, the exploiter, the robber. So the truth must be supressed."* ― **Eugene V. Debs**

*"I am not a Labor Leader; I do not want you to follow me or anyone else; if you are looking for a Moses to lead you out of this capitalist wilderness, you will stay right where you are. I would not lead you into the promised land if I could, because if I led you in, some one else would lead you out. You must use your heads as well as your hands, and get yourself out of your present condition."* ― **Eugene V. Debs**

*"Capitalism needs and must have the prison to protect itself from the criminals it has created."* ― **Eugene V. Debs**

*"The issue is Socialism versus Capitalism. I am for Socialism because I am for humanity. We have been cursed with the reign of gold long enough. Money constitutes no proper basis of civilization. The time has come to regenerate society—we are on the eve of universal change."* ― **Eugene V. Debs**

*"As a rule, large capitalists are Republicans and small capitalists are Democrats, but workingmen must remember that they are all capitalists, and that the many small ones, like the fewer large ones, are all politically supporting their class interests, and this is always and everywhere the capitalist class."* ― **Eugene V. Debs, [Outlook for Socialism in the United States (September, 1900)](https://www.marxists.org/archive/debs/works/1900/outlook.htm)**

*"Every one of the aristocratic conspirators and would-be murderers claims to be an arch-patriot; every one of them insists that the war is being waged to make the world safe for democracy. What humbug! What rot! What false pretense! These... tyrants, these red-handed robbers and murderers, the 'patriots,' while the men who have the courage to stand face to face with them, speak the truth, and fight for their exploited victims—they are [called] the disloyalists and traitors. If this be true, I want to take my place side by side with the traitors in this fight."* ― **Eugene V. Debs**

*"Some go to prison for stealing, and others for believing that a better system can be provided and maintained than one that makes it necessary for a man to steal in order to live."* ― **Eugene V. Debs**

---

*"The beliefs I have to defend are so soft and complicated, actually, and, when vivisected, turn into bowls of undifferentiated mush. I am a pacifist, I am an anarchist, I am a planetary citizen, and so on."* ― **Kurt Vonnegut**

*"The two real political parties in America are the Winners and the Losers. The people don't acknowledge this. They claim membership in two imaginary parties, the Republicans and the Democrats, instead."* ― **Kurt Vonnegut**

*"I have been called a Luddite. I welcome it. Do you know what a Luddite is? A person who hates newfangled contraptions."* ― **Kurt Vonnegut, A Man Without a Country**

---

*"I am nothing, but I must be everything."* ― **Carl/Karl Marx**

*"The oppressed are allowed once every few years to decide which particular representatives of the oppressing class are to represent and repress them."* ― **Carl/Karl Marx**

*"The less you eat, drink and read books; the less you go to the theatre, the dance hall, the public house; the less you think, love, theorize, sing, paint, fence, etc., the more you save—the greater becomes your treasure which neither moths nor dust will devour—your capital. The less you are, the more you have; the less you express your own life, the greater is your alienated life—the greater is the store of your estranged being."* ― **Carl/Karl Marx**

---

*"Don't walk behind me; I may not lead. Don't walk in front of me; I may not follow. Just walk beside me and be my friend."* ― **Albert Camus**

*"Poor and free rather than rich and enslaved. Of course, men want to be both rich and free, and this is what leads them at times to be poor and enslaved."* ― **Albert Camus**

*"Every time I hear a political speech or I read those of our leaders, I am horrified at having, for years, heard nothing which sounded human. It is always the same words telling the same lies. And the fact that men accept this, that the people's anger has not destroyed these hollow clowns, strikes me as proof that men attribute no importance to the way they are governed; that they gamble—yes, gamble—with a whole part of their life and their so-called 'vital interests.'"* ― **Albert Camus**

*"Do not be afraid of spending quality time by yourself. Find meaning or don't find meaning but 'steal' some time and give it freely and exclusively to your own self. Opt for privacy and solitude. That doesn't make you antisocial or cause you to reject the rest of the world. But you need to breathe. And you need to be."* ― **Albert Camus**

*"We must admit that today conformity is on the left. To be sure, the right is not brilliant. But the left is in complete decadence, a prisoner of words, caught in its own vocabulary, capable merely of stereotyped replies, constantly at a loss when faced with truth, from which it nevertheless claimed to derive its laws. The left is schizophrenic and needs doctoring through pitiless self-criticism, exercise of the heart, close reasoning, and a little modesty."* ― **Albert Camus**

---

*"Intolerance of ambiguity is the mark of an authoritarian personality."* ― **Theodor W. Adorno**

*"Talent is perhaps nothing other than successfully sublimated rage."* ― **Theodor W. Adorno**

*"Of the world as it exists, it is not possible to be enough afraid."* ― **Theodor W. Adorno**

*"No history leads from savagery to humanitarianism, but there is one leads from the slingshot to the megaton bomb."* ― **Theodor W. Adorno**

*"Happiness without power, wages without work, a home without frontiers, religion without myth. These characteristics are hated by the rulers because the ruled secretly long to possess them. The rulers are only safe as long as the people they rule turn their longed-for goals into hated forms of evil."* ― **Theodor W. Adorno**

*"I have no hobby. As far as my activities beyond the bounds of my recognized profession are concerned, I take them all, without exception, very seriously. So much so, that I should be horrified by the idea that they had anything to do with hobbies—preoccupations in which I had become mindlessly infatuated in order to kill the time—had I not become hardened by experience to such examples of this now widespread, barbarous mentality."* ― **Theodor W. Adorno**

*"Among today's adept practitioners, the lie has long since lost its honest function of misrepresenting reality. Nobody believes anybody, everyone is in the know. Lies are told only to convey to someone that one has no need either of him or his good opinion. The lie, once a liberal means of communication, has today become one of the techniques of insolence enabling each individual to spread around him the glacial atmosphere in whose shelter he can thrive."* ― **Theodor W. Adorno**

*"As naturally as the ruled always took the morality imposed upon them more seriously than did the rulers themselves, the deceived masses are today captivated by the myth of success even more than the successful are. Immovably, they insist on the very ideology which enslaves them. The misplaced love of the common people for the wrong which is done to them is a greater force than the cunning of the authorities."* ― **Theodor W. Adorno**

*"Pleasure always means not to think about anything, to forget suffering even where it is shown. Basically it is helplessness. It is flight; not, as is asserted, flight from a wretched reality, but from the last remaining thought of resistance."* ― **Theodor W. Adorno**

*"The culture industry perpetually cheats its consumers of what it perpetually promises. The promissory note which, with its plots and staging, it draws on pleasure is endlessly prolonged; the promise, which is actually all the spectacle consists of, is illusory: all it actually confirms is that the real point will never be reached, that the diner must be satisfied with the menu."* ― **Theodor W. Adorno**

*"Thought as such… is an act of negation, of resistance to that which is forced upon it; this is what thought has inherited from its archetype, the relation between labor and material. Today, when ideologues tend more than ever to encourage thought to be positive, they cleverly note that positivity runs precisely counter to thought, and that it takes friendly persuasion by social authority to accustom thought to positivity."* ― **Theodor W. Adorno**

*"The only philosophy that can be practiced responsibly in the face of despair is the attempt to contemplate all things as they would present themselves from the standpoint of redemption. Knowledge has no light but that shed on the world by redemption: all else is reconstruction, mere technique. Perspectives must be fashioned that displace and estrange the world, that reveal its fissures and crevices, as indigent and distorted as it will one day appear in the Messianic light."* ― **Theodor W. Adorno**

*"The triumph of advertising in the culture industry is that consumers feel compelled to buy and use its products even though they see through them."* ― **Theodor W. Adorno**

*"In contrast to the Kantian, the categorical imperative of the culture industry no longer has anything in common with freedom. It proclaims: you shall conform, without instruction as to what; conform to that which exists anyway as a reflex of its power and omnipresence. The power of the culture industry's ideology is such that conformity has replaced consciousness."* ― **Theodor W. Adorno**

*"Thus is order ensured: some have to play the game because they cannot otherwise live, and those who could live otherwise are kept out because they do not want to play the game. It is as if the class from which independent intellectuals have defected takes its revenge, by pressing its demands home in the very domain where the deserter seeks refuge."* ― **Theodor W. Adorno**

*"Indeed, happiness is nothing other than being encompassed, an after-image of the original shelter within the mother. But for this reason no one who is happy can know that he is so. To see happiness, he would have to pass out of it: to be as if already born. He who says he is happy lies, and in invoking happiness, sins against it. He alone keeps faith who says: I was happy."* ― **Theodor W. Adorno**

*"If fear and destructiveness are the major emotional sources of fascism, Eros belongs mainly to democracy."* ― **Theodor W. Adorno**

*"Philosophy, which once seemed outmoded, remains alive because the moment of its realization was missed. The summary judgement that it had merely interpreted the world is itself crippled by resignation before reality, and becomes a defeatism of reason after the transformation of the world failed. It guarantees no place from which theory as such could be concretely convicted of the anachronism, which then as now it is suspected of. Perhaps the interpretation which promised the transition did not suffice. The moment on which the critique of theory depended is not to be prolonged theoretically. Praxis, delayed for the foreseeable future, is no longer the court of appeals against self-satisfied speculation, but for the most part the pretext under which executives strangulate that critical thought as idle which a transforming praxis most needs. After philosophy broke with the promise that it would be one with reality or at least struck just before the hour of its production, it has been compelled to ruthlessly criticize itself."* ― **Theodor W. Adorno**

*"Today the order of life allows no room for the ego to draw spiritual or intellectual conclusions. The thought which leads to knowledge is neutralized and used as a mere qualification on specific labor markets and to heighten to commodity value of the personality."* ― **Theodor W. Adorno**

*"Even the aesthetic activities of political opposites are one in their enthusiastic obedience to the rhythm of the iron system."* ― **Theodor W. Adorno**

*"The glorification of splendid underdogs is nothing other than the glorification of the splendid system that makes them so."* ― **Theodor W. Adorno**

*"The subjectivist approach to art simply fails to understand that the subjective experience of art in itself is meaningless, and that in order to grasp the importance of art one has to zero in on the artistic object rather than on the fun of the art lover."* ― **Theodor W. Adorno**

*"The late Franz Borkenau once said, after he had broken with the Communist Party, that he could no longer put up with the practice of discussing municipal regulations in the categories of Hegelian logic, and Hegelian logic in the spirit of meetings of the town council."* ― **Theodor W. Adorno**

*"Enlightenment, understood in the widest sense as the advance of thought, has always aimed at liberating human beings from fear and installing them as masters. Yet the wholly enlightened earth is radiant with triumphant calamity."* ― **Theodor W. Adorno**

*"Amusement under late capitalism is the prolongation of work. It is sought after as an escape from the mechanised work process, and to recruit strength in order to be able to cope with it again. But at the same time mechanisation has such power over a man’s leisure and happiness, and so profoundly determines the manufacture of amusement goods, that his experiences are inevitably after-images of the work process itself. The ostensible content is merely a faded foreground; what sinks in is the automatic succession of standardised operations. What happens at work, in the factory, or in the office can only be escaped from by approximation to it in one’s leisure time."* ― **Theodor W. Adorno**

*"Ever since Plato, bourgeois consciousness has deceived itself that objective antinomies could be mastered by steering a middle course between them, whereas the sought-out mean always conceals the antinomy and is torn apart by it."* ― **Theodor W. Adorno**

*"Everywhere bourgeois society insists on the exertion of will; only love is supposed to be involuntary, pure immediacy of feeling. In its longing for this, which means a dispensation from work, the bourgeois idea of love transcends bourgeois society."* ― **Theodor W. Adorno**

*"Whereas the unconscious colossus of real existence, subjectless capitalism, inflicts its destruction blindly, the deludedly rebellious subject is willing to see that destruction as its fulfillment, and, together with the biting cold it emits toward human beings misused as things, it also radiates the perverted love which, in the world of things, takes the place of love in its immediacy."* ― **Theodor W. Adorno**

---

*"It is the responsibility of intellectuals to speak the truth and expose lies."* ― **Noam Chomsky, The Chomsky Reader**

*"Students who acquire large debts putting themselves through school are unlikely to think about changing society. When you trap people in a system of debt, they can't afford the time to think."* ― **Noam Chomsky**

*"The whole educational and professional training system is a very elaborate filter, which just weeds out people who are too independent, and who think for themselves, and who don't know how to be submissive, and so on—because they're dysfunctional to the institutions."* ― **Noam Chomsky, Understanding Power: The Indispensible Chomsky**

*"Far from creating independent thinkers, schools have always, throughout history, played an institutional role in a system of control and coercion. And once you are well educated you have already been socialized in ways that support the power structure, which, in turn, rewards you immensely."* ― **Noam Chomsky, Chomsky on Mis-Education**

*"The indoctrination is so deep that educated people think they’re being objective."* ― **Noam Chomsky, [On Fake News and Other Societal Woes](https://chomsky.info/20051207/)**

*"Education is a system of imposed ignorance."* ― **Noam Chomsky, [Manufacturing Consent: Noam Chomsky and the Media](https://www.youtube.com/watch?v=BQXsPU25B60)**

*"It doesn't matter how much you learn in school; it's whether you learn how to go on and do things by yourself. And that can be done at any level."* ― **Noam Chomsky**

*"There's a good reason why nobody studies history, it just teaches you too much."* ― **Noam Chomsky**

*"The general population doesn't know what's happening, and it doesn't even know that it doesn't know."* ― **Noam Chomsky, How the World Works**

*"The smart way to keep people passive and obedient is to strictly limit the spectrum of acceptable opinion, but allow very lively debate within that spectrum—even encourage the more critical and dissident views. That gives people the sense that there's free thinking going on, while all the time the presuppositions of the system are being reinforced by the limits put on the range of the debate."* ― **Noam Chomsky, How the World Works**

*"The goal is to keep the bewildered herd bewildered. It's unnecessary for them to trouble themselves with what's happening in the world. In fact, it's undesirable—if they see too much of reality they may set themselves to change it."* ― **Noam Chomsky, How the World Works**

*"It’s ridiculous to talk about freedom in a society dominated by huge corporations. What kind of freedom is there inside a corporation? They’re totalitarian institutions—you take orders from above and maybe give them to people below you. There’s about as much freedom as under Stalinism."* ― **Noam Chomsky, How the World Works**

*"The best defense against democracy is to distract people."* ― **Noam Chomsky, How the World Works**

*"Nobody is going to pour truth into your brain. It's something you have to find out for yourself."* ― **David Barsamian, Noam Chomsky, Propaganda and the Public Mind: Conversations with Noam Chomsky**

*"As long as the general population is passive, apathetic, diverted to consumerism or hatred of the vulnerable, then the powerful can do as they please, and those who survive will be left to contemplate the outcome."* ― **Noam Chomsky, Who Rules the World?**

*"The key element of social control is the strategy of distraction that is to divert public attention from important issues and changes decided by political and economic elites, through the technique of flood or flooding continuous distractions and insignificant information."* ― **Noam Chomsky**

*"Optimism is a strategy for making a better future. Because unless you believe that the future can be better, it’s unlikely you will step up and take responsibility for making it so. If you assume that there’s no hope, you guarantee that there will be no hope. If you assume that there is an instinct for freedom, there are opportunities to change things, there’s a chance you may contribute to making a better world. The choice is yours."* ― **Noam Chomsky**

*"The effect [of the media] on the public isn’t very much studied but to the extent that it has been it seems as though among the more educated sectors the indoctrination works more effectively. Among the less educated sectors people are just more skeptical and cynical."* ― **Noam Chomsky, [On Fake News and Other Societal Woes](https://chomsky.info/20051207/)**

*"Democratic societies can't force people. Therefore they have to control what they think."* ― **Noam Chomsky**

*"It takes one minute to tell a lie, and an hour to refute it."* ― **Noam Chomsky**

*"The public is not to see where power lies, how it shapes policy, and for what ends. Rather, people are to hate and fear one another."* ― **Noam Chomsky, [Let's stay chummy, Chomsky](https://www.independent.co.uk/news/media/let-s-stay-chummy-chomsky-1257575.html)**

*"Governments will use whatever technology is available to combat their primary enemy—their own population."* ― **Noam Chomsky, [NSA surveillance is an attack on American citizens, says Noam Chomsky](https://www.theguardian.com/world/2013/jun/19/nsa-surveillance-attack-american-citizens-noam-chomsky)**

*"How it is we have so much information, but know so little?"* ― **Noam Chomsky**

*"If you are working 50 hours a week in a factory, you don't have time to read 10 newspapers a day and go back to declassified government archives. But such people may have far-reaching insights into the way the world works."* ― **Noam Chomsky**

*"If something is repeated over and over as obvious, the chances are that it is obviously false."* ― **Noam Chomsky**

*"If you are not offending people who ought to be offended, you're doing something wrong."* ― **Noam Chomsky**

*"That's the standard technique of privatization: defund, make sure things don't work, people get angry, you hand it over to private capital."* ― **Noam Chomsky**

*"All public resources go to the rich. The poor, if they can survive in the labor market, fine. Otherwise, they die. That's economics in a nutshell."* ― **Noam Chomsky**

*"If voting could actually change anything, it would be illegal."* ― **Noam Chomsky**

*"The point of public relations slogans like 'Support Our Troops' is that they don't mean anything ... that's the whole point of good propaganda. You want to create a slogan that nobody is going to be against and I suppose everybody will be for, because nobody knows what it means, because it doesn't mean anything. But its crucial value is that it diverts your attention from a question that does mean something, do you support our policy? And that's the one you're not allowed to talk about."* ― **Noam Chomsky**

*"When I was in high school I asked myself at one point: 'Why do I care if my high school's team wins the football game? I don't know anybody on the team, they have nothing to do with me... why am I here and applaud? It does not make any sense.' But the point is, it does make sense: It's a way of building up irrational attitudes of submission to authority and group cohesion behind leadership elements. In fact it's training in irrational jingoism. That's also a feature of competitive sports."* ― **Noam Chomsky, [Manufacturing Consent: Noam Chomsky and the Media](https://www.youtube.com/watch?v=BQXsPU25B60)**

*"Jingoism, racism, fear, religious fundamentalism: these are the ways of appealing to people if you're trying to organize a mass base of support for policies that are really intended to crush them."* ― **Noam Chomsky, Understanding Power**

*"Concentration of wealth yields concentration of political power. And concentration of political power gives rise to legislation that increases and accelerates the cycle."* ― **Noam Chomsky, Occupy**

*"Neoliberal democracy. Instead of citizens, it produces consumers. Instead of communities, it produces shopping malls. The net result is an atomized society of disengaged individuals who feel demoralized and socially powerless. In sum, neoliberalism is the immediate and foremost enemy of genuine participatory democracy, not just in the United States but across the planet, and will be for the foreseeable future."* ― **Noam Chomsky, Profit Over People: Neoliberalism and Global Order**

*"If you want to control a population... give them a God to worship."* ― **Noam Chomsky**

*"You cannot control your own population by force, but it can be distracted by consumption."* ― **Noam Chomsky**

*"Nobody should have any illusions. The United States has essentially a one-party system and the ruling party is the business party."* ― **Noam Chomsky, [The United States Has Essentially a One-Party System](https://www.spiegel.de/international/world/interview-with-noam-chomsky-the-united-states-has-essentially-a-one-party-system-a-583454.html)**

*"Anyone who studies declassified documents soon becomes aware that government secrecy is largely an effort to protect policy makers from scrutiny by citizens, not to protect the country from enemies."* ― **Noam Chomsky**

*"If anybody thinks they should listen to me because I'm a professor at MIT, that's nonsense. You should decide whether something makes sense by its content, not by the letters after the name of the person who says it."* ― **Noam Chomsky, Understanding Power: The Indispensible Chomsky**

---

*"If you don't read the newspaper, you're uninformed. If you read the newspaper, you're misinformed."* ― **Mark Twain**

---

*"In the sciences, the authority of thousands of opinions is not worth as much as one tiny spark of reason in an individual man."* ― **Galileo Galilei**

*"You can't teach anybody anything, only make them realize the answers are already inside them."* ― **Galileo Galilei**

*"I have never met a man so ignorant that I couldn't learn something from him."* ― **Galileo Galilei**

---

*"Be a free thinker and don't accept everything you hear as truth. Be critical and evaluate what you believe in."* ― **Aristotle**

*"The most important relationship we can all have is the one you have with yourself, the most important journey you can take is one of self-discovery. To know yourself, you must spend time with yourself, you must not be afraid to be alone. Knowing yourself is the beginning of all wisdom."* ― **Aristotle**

---

*"If you do not take an interest in the affairs of your government, then you are doomed to live under the rule of fools."* ― **Plato**

*"I know not how I may seem to others, but to myself I am but a small child wandering upon the vast shores of knowledge, every now and then finding a small bright pebble to content myself with."* ― **Plato**

*"Someday, in the distant future, our grandchildren's grandchildren will develop a new equivalent of our classrooms. They will spend many hours in front of boxes with fires glowing within. May they have the wisdom to know the difference between light and knowledge."* ― **Plato**

---

*"The truth is rarely pure and never simple."* ― **Oscar Wilde, The Importance of Being Earnest: A Trivial Comedy for Serious People**

*"Imitation is the sincerest form of flattery that mediocrity can pay to greatness."* ― **Oscar Wilde**

---

*"In a time of universal deceit, telling the truth is a revolutionary act."* ― **George Orwell**

*"The further a society drifts from truth, the more it will hate those who speak it."* ― **George Orwell**

*"The most effective way to destroy people is to deny and obliterate their own understanding of their history."* ― **George Orwell**

*"Who controls the past controls the future. Who controls the present controls the past."* ― **George Orwell, 1984**

*"Every record has been destroyed or falsified, every book has been rewritten, every picture has been repainted, every statue and street and building has been renamed, every date has been altered. And that process is continuing day by day and minute by minute. History has stopped. Nothing exists except an endless present in which the Party is always right."* ― **George Orwell, 1984**

---

*"If a state is governed by the principles of reason, poverty and misery are subjects of shame. If a state is not governed by the principles of reason, riches and honors are subjects of shame."* ― **Confucius**

---

*"Most of the luxuries and many of the so-called comforts of life are not only not indispensable, but positive hindrances to the elevation of mankind."* ― **Henry David Thoreau, Walden**

*"Simplify your life. Don't waste the years struggling for things that are unimportant. Don't burden yourself with possessions. Keep your needs and wants simple and enjoy what you have. Don't destroy your peace of mind by looking back, worrying about the past. Live in the present. Simplify!"* ― **Henry David Thoreau**

*"Think for yourself, or others will think for you without thinking of you."* ― **Henry David Thoreau**

*"The price of anything is the amount of life you exchange for it."* ― **Henry David Thoreau**

*"I went to the woods because I wished to live deliberately, to front only the essential facts of life, and see if I could not learn what it had to teach, and not, when I came to die, discover that I had not lived. I did not wish to live what was not life, living is so dear; nor did I wish to practise resignation, unless it was quite necessary. I wanted to live deep and suck out all the marrow of life, to live so sturdily and Spartan-like as to put to rout all that was not life, to cut a broad swath and shave close, to drive life into a corner, and reduce it to its lowest terms, and, if it proved to be mean, why then to get the whole and genuine meanness of it, and publish its meanness to the world; or if it were sublime, to know it by experience, and be able to give a true account of it in my next excursion."* ― **Henry David Thoreau, Walden**

*"I heartily accept the motto,—'That government is best which governs least;' and I should like to see it acted up to more rapidly and systematically. Carried out, it finally amounts to this, which also I believe—'That government is best which governs not at all;' and when men are prepared for it, that will be the kind of government which they will have. Government is at best but an expedient; but most governments are usually, and all governments are sometimes, inexpedient."* ― **Henry David Thoreau, [On the Duty of Civil Disobedience](https://www.gutenberg.org/files/71/71-h/71-h.htm)**

*"Under a government which imprisons any unjustly, the true place for a just man is also a prison. ... where the State places those who are not with her, but against her,—the only house in a slave State in which a free man can abide with honor. If any think that their influence would be lost there, and their voices no longer afflict the ear of the State, that they would not be as an enemy within its walls, they do not know by how much truth is stronger than error, nor how much more eloquently and effectively he can combat injustice who has experienced a little in his own person. Cast your whole vote, not a strip of paper merely, but your whole influence. A minority is powerless while it conforms to the majority; it is not even a minority then; but it is irresistible when it clogs by its whole weight. If the alternative is to keep all just men in prison, or give up war and slavery, the State will not hesitate which to choose. If a thousand men were not to pay their tax bills this year, that would not be a violent and bloody measure, as it would be to pay them, and enable the State to commit violence and shed innocent blood. This is, in fact, the definition of a peaceable revolution, if any such is possible. If the tax-gatherer, or any other public officer, asks me, as one has done, 'But what shall I do?' my answer is, 'If you really wish to do any thing, resign your office.' When the subject has refused allegiance, and the officer has resigned his office, then the revolution is accomplished. But even suppose blood should flow. Is there not a sort of blood shed when the conscience is wounded? Through this wound a man's real manhood and immortality flow out, and he bleeds to an everlasting death. I see this blood flowing now."* ― **Henry David Thoreau, [On the Duty of Civil Disobedience](https://www.gutenberg.org/files/71/71-h/71-h.htm)**

---

*"Our strategy should be not only to confront empire, but to lay siege to it. To deprive it of oxygen. To shame it. To mock it. With our art, our music, our literature, our stubbornness, our joy, our brilliance, our sheer relentlessness—and our ability to tell our own stories. Stories that are different from the ones we're being brainwashed to believe. The corporate revolution will collapse if we refuse to buy what they are selling—their ideas, their version of history, their wars, their weapons, their notion of inevitability. Remember this: We be many and they be few. They need us more than we need them. Another world is not only possible, she is on her way. On a quiet day, I can hear her breathing."* ― **Arundhati Roy, War Talk**

---

*"Do the right thing because it is right."* ― **Immanuel Kant**

*"Most men use their knowledge only under guidance from others because they lack the courage to think independently using their own reasoning abilities. It takes intellectual daring to discover the truth."* ― **Immanuel Kant**

*"Enlightenment is man's leaving his self-caused immaturity. Immaturity is the incapacity to use one's intelligence without the guidance of another. Such immaturity is self-caused if it is not caused by lack of intelligence, but by lack of determination and courage to use one's intelligence without being guided by another. Sapere Aude! Have the courage to use your own intelligence is therefore the motto of the enlightenment."* ― **Immanuel Kant**

*"If you punish a child for being naughty, and reward him for being good, he will do right merely for the sake of the reward; and when he goes out into the world and finds that goodness is not always rewarded, nor wickedness always punished, he will grow into a man who only thinks about how he may get on in the world, and does right or wrong according as he finds advantage to himself."* ― **Immanuel Kant**

---

*"The best life is the one in which the creative impulses play the largest part and the possessive impulses the smallest."* ― **Bertrand Russell, Pacifism and Revolution**

---

*"I should like to say two things, one intellectual and one moral:*

*The intellectual thing I should want to say to them is this: When you are studying any matter or considering any philosophy, ask yourself only what are the facts and what is the truth that the facts bear out. Never let yourself be diverted either by what you wish to believe or by what you think would have beneficent social effects if it were believed, but look only and solely at what are the facts. That is the intellectual thing that I should wish to say.*

*The moral thing I should wish to say to them is very simple. I should say: **Love is wise, hatred is foolish. In this world, which is getting more and more closely interconnected, we have to learn to tolerate each other. We have to learn to put up with the fact that some people say things that we don't like. We can only live together in that way, and if we are to live together and not die together we must learn a kind of charity and a kind of tolerance which is absolutely vital to the continuation of human life on this planet.**"* ― **Bertrand Russell, [BBC Face to Face (1959)](https://www.youtube.com/watch?v=a10A5PneXlo&t=1646s)**

---

*"Education can help us only if it produces 'whole men.' The truly educated man is not a man who knows a bit of everything, not even the man who knows all the details of all subjects (if such a thing were possible): the 'whole man' in fact, may have little detailed knowledge of facts and theories, he may treasure the Encyclopædia Britannica because 'she knows and he needn't', but he will be truly in touch with the centre. He will not be in doubt about his basic convictions, about his view on the meaning and purpose of his life. He may not be able to explain these matters in words, but the conduct of his life will show a certain sureness of touch which stems from this inner clarity."* ― **E. F. Schumacher, Small is Beautiful: A Study of Economics as if People Mattered**

---

*"People who talk well but do nothing are like musical instruments; the sound is all they have to offer."* ― **Diogenes, Herakleitos and Diogenes**

---

*"Missing from such histories are the countless small actions of unknown people that led up to those great moments. When we understand this, we can see that the tiniest acts of protest in which we engage may become the invisible roots of social change."* ― **Howard Zinn, You Can't Be Neutral on a Moving Train: A Personal History of Our Times**

---

*"The best people possess a feeling for beauty, the courage to take risks, the discipline to tell the truth, the capacity for sacrifice. Ironically, their virtues make them vulnerable; they are often wounded, sometimes destroyed."* ― **Ernest Hemingway, The Letters of Ernest Hemingway**

---

*"Keep away from people who try to belittle your ambitions. Small people always do that, but the really great make you feel that you, too, can become great."* ― **Mark Twain**

---

*"Without music, life would be a mistake."* ― **Friedrich Nietzsche, Twilight of the Idols**

---

[**Music Playlist**](https://www.youtube.com/watch_videos?video_ids=NdSLsRKnafI,0ivQwwgW4OY,YonS9_QJbp8,nyWbun_PbTc,lt-udg9zQSE,MbXWrmQW-OE,l47At7wrhyI,20ZDch2_6YI,kln_bIndDJg,aOt0bUAzqD4,Z0GFRcFm-aY,8OyBtMPqpNY,eFTLKWw542g,-ucDiz3GYrg,bc5Nk1DXyEY,T8wiV29Znxg,o5FPPoLqkCk,SBjQ9tuuTJQ,XRXyCcyM7k4,vfpgpf6QVnI,WdoXZf-FZyA,7oGHqMijc3M,PnngnWjwtdM,12kcpP-8jfM,djrUf9tz--o,YlUKcNNmywk,iywaBOMvYLI,bT7Hj-ea0VE,yjOeWiPZeZw,HrxX9TBj2zY,7xxgRUyzgs0,3L4YrGaR8E4,LUwviJMVV5g,SLi7Ljcy6n8,OshNahVo9-c,sqSA-SY5Hro,bZAMiK6ROZA,1lyu1KKwC74,LUkqBRC1zUA,keRMhpjjn_U,4VxdufqB9zg,3qzBGYG786Y,fMWIC7LMsKU,8eNoms9wsGc,2X_2IdybTV0,B7nKzCRL_oo,8DyziWtkfBw,ncfTCyVSdWM,g-UOTOqtU0M,GySIToHCPac,vwSRqaZGsPw,CNdOsL4Xe7Q,aq9gVp-Wk7I,NSTct2FFamw,WuXwSyahgW4,Yxep-9BQ6Uo,w8KQmps-Sog,G_sBOsh-vyI,_NzLs-xSss0,tAGnKpE4NCI,KDMvN45sjo4,aqW-VF49Sok,VjATZYHs4Tc,WC5FdFlUcl0,L_jWHffIx5E,YFDg-pgE0Hk,MzGnX-MbYE4,nQusU0g9H-E,HuS5NuXRb5Y,F3DUxYUR5IA,BHOevX4DlGk,CxMXDwP7lqM,UkHSmDxX1t4,Hy8kmNEo1i8,02vDkMEdIkY,edwk-8KJ1Js,AdUianylWuo,DZyYapMZSec)

<details><summary><b>Music Lyrics</b> <i>(click to expand/collapse)</i></summary><p>

🎵 [***Welcome my son, welcome to the machine. Where have you been? It's alright we know where you've been.***](https://www.youtube.com/watch?v=lt-udg9zQSE) 🎵

🎵 [***You've been in the pipeline, filling in time. Provided with toys and scouting for boys. You bought a guitar to punish your ma. And you didn't like school, you know you're nobody's fool. So welcome to the machine.***](https://www.youtube.com/watch?v=lt-udg9zQSE) 🎵

🎵 [***Welcome my son, welcome to the machine. What did you dream? It's alright we told you what to dream.***](https://www.youtube.com/watch?v=lt-udg9zQSE) 🎵

🎵 [***You dreamed of a big star. He played a mean guitar. He always ate at the steak bar. He loved to drive in his Jaguar. So welcome to the machine.***](https://www.youtube.com/watch?v=lt-udg9zQSE) 🎵

---

🎵 *Just a castaway, an island lost at sea, oh. Another lonely day, with no one here but me, oh. More loneliness than any man could bear. Rescue me before I fall into despair, oh.* 🎵

🎵 [***I'll send an S.O.S. to the world, I'll send an S.O.S. to the world. I hope that someone gets my, I hope that someone gets my, I hope that someone gets my message in a bottle, yeah. Message in a bottle, yeah.***](https://www.youtube.com/watch?v=MbXWrmQW-OE) 🎵

🎵 *A year has passed since I wrote my note. I should have know this right from the start. Only hope can keep me together. Love can mend your life, but love can break your heart.* 🎵

🎵 [***I'll send an S.O.S. to the world, I'll send an S.O.S. to the world. I hope that someone gets my, I hope that someone gets my, I hope that someone gets my message in a bottle, yeah. Message in a bottle, yeah. Oh, message in a bottle, yeah. Message in a botle, yeah.***](https://www.youtube.com/watch?v=MbXWrmQW-OE) 🎵

🎵 *Walked out this morning, I don't believe what I saw. Hundred billion bottles washed up on the shore. Seems I'm not alone in being alone. Hundred billion castaways, looking for a home.* 🎵

🎵 [***I'll send an S.O.S. to the world, I'll send an S.O.S. to the world. I hope that someone gets my, I hope that someone gets my, I hope that someone gets my message in a bottle, yeah. Message in a bottle, whoah. Message in a bottle, whoah. Message in a bottle, whoah. Message in a bottle, yeah.***](https://www.youtube.com/watch?v=MbXWrmQW-OE) 🎵

🎵 [***Sending out an S.O.S. Sending out an S.O.S. I'm sending out an S.O.S. I'm sending out an S.O.S. I'm sending out an S.O.S. I'm sending out an S.O.S. Sending out an S.O.S. Sending out an S.O.S. Sending out an S.O.S. Sending out an S.O.S. Sending out an S.O.S. Sending out an S.O.S. Sending out an S.O.S. Sending out an S.O.S. Sending out an S.O.S. Sending out an S.O.S.***](https://www.youtube.com/watch?v=MbXWrmQW-OE) 🎵

---

🎵 *This passage of time, for I have always been. Through shadows of doubt, again and again.* 🎵

🎵 [***All that you wished, all that you'd need. Abandon your regrets, and then you'll see. Take my hand, walk with me. A world without end, I'll set you free.***](https://www.youtube.com/watch?v=l47At7wrhyI) 🎵

🎵 *Promises made, the oppressor of lies. The falsehood of truth, that can never die.* 🎵

🎵 [***All that you wished, all that you'd need. Abandon your regrets, and then you'll see. Take my hand, walk with me. A world without end, I'll set you free.***](https://www.youtube.com/watch?v=l47At7wrhyI) 🎵

🎵 [***Take my hand, walk with me. A world without end, I'll set you free.***](https://www.youtube.com/watch?v=l47At7wrhyI) 🎵

---

🎵 *Standing by my window, breathing summer breeze. Saw a figure floating, 'neath the willow tree. Asked us if we were happy, we said we didn't know. Took us by the hands and up we go.* 🎵

🎵 [***We followed the dreamer through the purple hazy clouds, he could control our sense of time. We thought we were lost but no matter how we tried, everyone was in peace of mind.***](https://www.youtube.com/watch?v=20ZDch2_6YI) 🎵

🎵 *We felt the sensations drift inside our frames, finding complete contentment there. And all the tensions that hurt us in the past, just seemed to vanish in thin air.* 🎵

🎵 *He said in the cosmos is a single sonic sound that is vibrating constantly. And if we could grip and hold on to the note, we would see our minds were free, oh they're free.* 🎵

🎵 *Ah, we are lost above, floating way up high. If you think you can find a way, you can surely try.* 🎵

---

🎵 *When I was young, it seemed that life was so wonderful, a miracle, oh, it was beautiful, magical. And all the birds in the trees, well they'd be singing so happily, oh, joyfully, oh, playfully watching me.* 🎵

🎵 *But then they send me away to teach me how to be sensible, logical, oh, responsible, practical. And then they showed me a world where I could be so dependable, oh, clinical, oh, intellectual, cynical.* 🎵

🎵 [***There are times when all the world's asleep, the questions run too deep for such a simple man. Won't you please, please tell me what we've learned. I know it sounds absurd, please tell me who I am.***](https://www.youtube.com/watch?v=kln_bIndDJg) 🎵

🎵 *I said, watch what you say or they'll be calling you a radical, a liberal, oh, fanatical, criminal. Won't you sign up your name, we'd like to feel you're acceptable, respectable, oh, presentable, a vegetable. Oh, take it, take it, take it, yeah.* 🎵

🎵 [***But at night, when all the world's asleep, the questions run so deep for such a simple man. Won't you please (Oh won't you tell me). Please tell me what we've learned (Can you hear me). I know it sounds absurd (Oh won't you help me), please tell me who I am, who I am, who I am, who I am.***](https://www.youtube.com/watch?v=kln_bIndDJg) 🎵

🎵 *'Cause I was feeling so logical, d-d-d-d-d-d-d-digital. One, two, three, five. Oh, oh, oh, oh yeah. Ooh it's getting unbelievable, yeah.* 🎵

---

🎵 *I'm taking a ride with my best friend. I hope he never lets me down again. He knows where he's taking me, taking me where I want to be. I'm taking a ride with my best friend.* 🎵

🎵 [***We're flying high, we're watching the world pass us by. Never want to come down, never want to put my feet back down on the ground.***](https://www.youtube.com/watch?v=aOt0bUAzqD4) 🎵

🎵 *I'm taking a ride with my best friend. I hope he never lets me down again. Promises me I'm as safe as houses, as long as I remember who's wearing the trousers. I hope he never lets me down again.* 🎵

🎵 [***We're flying high, we're watching the world pass us by. Never want to come down, never want to put my feet back down on the ground.***](https://www.youtube.com/watch?v=aOt0bUAzqD4) 🎵

🎵 [***We're flying high, we're watching the world pass us by. Never want to come down, never want to put my feet back down on the ground.***](https://www.youtube.com/watch?v=aOt0bUAzqD4) 🎵

🎵 *Never let me down, never let me down, never let me down, never let me down.* 🎵

🎵 *See the stars, they're shining bright. Everything's alright tonight. See the stars, they're shining bright. Everything's alright tonight.* 🎵

🎵 *See the stars, they're shining bright. Everything's alright tonight. See the stars, they're shining bright. Everything's alright tonight.* 🎵

---

🎵 *That's great, it starts with an earthquake, birds and snakes, an airplane, and Lenny Bruce is not afraid. Eye of a hurricane, listen to yourself churn. World serves its own needs, don't misserve your own needs. Feed it off an aux, speak, grunt, no strength. The ladder starts to clatter with fear fight, down, height. Wire in a fire, representing seven games and a government for hire and a combat site. Left of west and coming in a hurry with the furies breathing down your neck. Team by team, reporters baffled, trumped, tethered cropped. Look at that low plane, fine, then. Uh oh, overflow, population, common food, but it'll do. Save yourself, serve yourself. World serves its own needs, listen to your heart bleed. Tell me with the rapture and the reverent in the right, right. You vitriolic, patriotic, slam fight, bright light. Feeling pretty psyched.* 🎵

🎵 [***It's the end of the world as we know it, it's the end of the world as we know it, it's the end of the world as we know it, and I feel fine.***](https://www.youtube.com/watch?v=Z0GFRcFm-aY) 🎵

🎵 *Six o'clock, TV hour, don't get caught in foreign tower. Slash and burn, return, listen to yourself churn. Lock him in uniform and book burning, bloodletting. Every motive escalate, automotive incinerate. Light a candle, light a votive, step down, step down. Watch your heel crush, crush. Uh oh, this means no fear, cavalier, renegade, steer clear. A tournament, a tournament, a tournament of lies. Offer me solutions, offer me alternatives, and I decline.* 🎵

🎵 [***It's the end of the world as we know it, it's the end of the world as we know it (it's time I had some time alone), it's the end of the world as we know it (it's time I had some time alone), and I feel fine (I feel fine).***](https://www.youtube.com/watch?v=Z0GFRcFm-aY) 🎵

🎵 [***It's the end of the world as we know it (it's time I had some time alone), it's the end of the world as we know it (it's time I had some time alone), it's the end of the world as we know it (it's time I had some time alone), and I feel fine.***](https://www.youtube.com/watch?v=8OyBtMPqpNY) 🎵

🎵 *The other night I dreamt a nice continental drift divide. Mountains sit in a line, Leonard Bernstein, Leonid Brezhnev, Lenny Bruce, and Lester Bangs. Birthday party, cheesecake, jelly bean, boom! You symbiotic, patriotic, slam, but neck. Right? (Right!)* 🎵

🎵 [***It's the end of the world as we know it (it's time I had some time alone), it's the end of the world as we know it (it's time I had some time alone), it's the end of the world as we know it (it's time I had some time alone), and I feel fine.***](https://www.youtube.com/watch?v=8OyBtMPqpNY) 🎵

🎵 [***It's the end of the world as we know it, it's the end of the world as we know it, it's the end of the world as we know it (it's time I had some time alone), and I feel fine.***](https://www.youtube.com/watch?v=Z0GFRcFm-aY) 🎵

🎵 [***It's the end of the world as we know it (it's time I had some time alone), it's the end of the world as we know it (it's time I had some time alone), it's the end of the world as we know it (it's time I had some time alone), and I feel fine.***](https://www.youtube.com/watch?v=8OyBtMPqpNY) 🎵

🎵 [***It's the end of the world as we know it (it's time I had some time alone), it's the end of the world as we know it (it's time I had some time alone), it's the end of the world as we know it (it's time I had some time alone), and I feel fine.***](https://www.youtube.com/watch?v=8OyBtMPqpNY) 🎵

---

🎵 *Harry Truman, Doris Day, Red China, Johnnie Ray, South Pacific, Walter Winchell, Joe DiMaggio, Joe McCarthy, Richard Nixon, Studebaker, television, North Korea, South Korea, Marilyn Monroe.* 🎵

🎵 *Rosenbergs, H-bomb, Sugar Ray, Panmunjom, Brando, "The King and I", and "The Catcher in the Rye", Eisenhower, vaccine, England's got a new queen, Marciano, Liberace, Santayana, goodbye.* 🎵

🎵 [***We didn't start the fire, it was always burning, since the world's been turning. We didn't start the fire, no, we didn't light it, but we tried to fight it.***](https://www.youtube.com/watch?v=eFTLKWw542g) 🎵

🎵 *Joseph Stalin, Malenkov, Nasser and Prokofiev, Rockefeller, Campanella, Communist Bloc, Roy Cohn, Juan Peron, Toscanini, Dacron, Dien Bien Phu falls, "Rock Around the Clock", Einstein, James Dean, Brooklyn's got a winning team, Davy Crockett, Peter Pan, Elvis Presley, Disneyland, Bardot, Budapest, Alabama, Krushchev, Princess Grace, "Peyton Place", Trouble in the Suez.* 🎵

🎵 [***We didn't start the fire, it was always burning, since the world's been turning. We didn't start the fire, no, we didn't light it, but we tried to fight it.***](https://www.youtube.com/watch?v=eFTLKWw542g) 🎵

🎵 *Little Rock, Pasternak, Mickey Mantle, Kerouac, Sputnik, Chou En-Lai, "Bridge on the River Kwai", Lebanon, Charles de Gaulle, California baseball, Starkweather homicide, children of thalidomide, Buddy Holly, "Ben-Hur", space monkey, mafia, Hula hoops, Castro, Edsel is a no-go, U2, Syngman Rhee, Payola and Kennedy, Chubby Checker, "Psycho", Belgians in the Congo.* 🎵

🎵 [***We didn't start the fire, it was always burning, since the world's been turning. We didn't start the fire, no, we didn't light it, but we tried to fight it.***](https://www.youtube.com/watch?v=eFTLKWw542g) 🎵

🎵 *Hemingway, Eichmann, "Stranger in a Strange Land", Dylan, Berlin, Bay of Pigs invasion, "Lawrence of Arabia", British Beatlemania, Ole Miss, John Glenn, Liston beats Patterson, Pope Paul, Malcolm X, British politician sex, JFK blown away, what else do I have to say?!* 🎵

🎵 [***We didn't start the fire, it was always burning, since the world's been turning. We didn't start the fire, no, we didn't light it, but we tried to fight it.***](https://www.youtube.com/watch?v=eFTLKWw542g) 🎵

🎵 *Birth control, Ho Chi Minh, Richard Nixon back again, Moonshot, Woodstock, Watergate, punk rock, Begin, Reagan, Palestine, terror on the airline, Ayatollah's in Iran, Russians in Afghanistan, "Wheel of Fortune", Sally Ride, heavy metal suicide, foreign debts, homeless vets, AIDS, crack, Bernie Goetz, hypodermics on the shore, China's under martial law, rock and roller, cola wars, I can't take it anymore!* 🎵

🎵 [***We didn't start the fire, it was always burning, since the world's been turning. We didn't start the fire, but when we are gone it will still burn on, and on, and on, and on, and on, and on, and on, and on.***](https://www.youtube.com/watch?v=eFTLKWw542g) 🎵

🎵 [***We didn't start the fire, it was always burning, since the world's been turning. We didn't start the fire, no, we didn't light it, but we tried to fight it. We didn't start the fire, it was always burning, since the world's been turning. We didn't start the fire, no, we didn't light it, but we tried to fight it. We didn't start the fire, it was always burning, since the world's been turning. We didn't start the fire, no, we didn't light it, but we tried to fight it.***](https://www.youtube.com/watch?v=eFTLKWw542g) 🎵

---

🎵 *What we've got here is failure to communicate. Some men you just can't reach, so you get what we had here last week. Which is the way he wants it, well, he gets it! I don't like this any more than you men.* 🎵

🎵 *Look at your young men fighting, look at your women crying, look at your young men dying, the way they've always done before. Look at the hate we're breeding, look at the fear we're feeding, look at the lives we're leading, the way we've always done before.* 🎵

🎵 *My hands are tied! The billions shift from side to side, and the wars go on with brainwashed pride for the love of God and our human rights. And all these things are swept aside by bloody hands, time can't deny and are washed away by your genocide. And history hides the lies of our civil wars.* 🎵

🎵 *D'you wear a black armband when they shot the man who said, "Peace could last forever"? And in my first memories, they shot Kennedy. I went numb when I learned to see. So I never fell for Vietnam, we got the wall in D.C. to remind us all that you can't trust freedom when it's not in your hands, when everybody's fightin' for their promised land and* 🎵

🎵 [***I don't need your civil war. It feeds the rich, while it buries the poor. You're power-hungry, sellin' soldiers in a human grocery store. Ain't that fresh? I don't need your civil war. Ooh, no, no, no, no, no.***](https://www.youtube.com/watch?v=-ucDiz3GYrg) 🎵

🎵 *Look at the shoes you're filling, look at the blood we're spilling, look at the world we're killing, the way we've always done before. Look in the doubt we've wallowed, look at the leaders we've followed, look at the lies we've swallowed, and I don't want to hear no more!* 🎵

🎵 *My hands are tied! For all I've seen has changed my mind, but still, the wars go on, as the years go by, with no love of God or human rights. And all these dreams are swept aside by bloody hands of the hypnotized who carry the cross of homicide. And history bears the scars of our civil wars.* 🎵

🎵 *We practice selective annihilation of mayors and government officials. For example, we create a vacuum, then we fill that vacuum. As popular war advances, peace is closer.* 🎵

🎵 [***I don't need your civil war. It feeds the rich, while it buries the poor. You're power-hungry, sellin' soldiers in a human grocery store. Ain't that fresh? I don't need your civil war. No no no no no no no no no no no no. I don't need your civil war, I don't need your civil war. You're power-hungry, sellin' soldiers in a human grocery store. Ain't that fresh? I don't need your civil war. No no no no no no no no no no no no. I don't need one more war. Ooh, I don't need one more war. No no no, no whoa, no whoa. What's so civil 'bout war anyway?***](https://www.youtube.com/watch?v=-ucDiz3GYrg) 🎵

---

🎵 [***Generals gathered in their masses, just like witches at black masses. Evil minds that plot destruction, sorcerer of death's construction. In the fields, the bodies burning, as the war machine keeps turning. Death and hatred to mankind, poisoning their brainwashed minds. Oh lord, yeah!***](https://www.youtube.com/watch?v=bc5Nk1DXyEY) 🎵

🎵 [***Politicians hide themselves away, they only started the war. Why should they go out to fight? They leave that role to the poor, yeah! Time will tell on their power minds, making war just for fun. Treating people just like pawns in chess, wait till their judgement day comes, yeah!***](https://www.youtube.com/watch?v=bc5Nk1DXyEY) 🎵

🎵 [***Now in darkness, world stops turning, ashes where their bodies burning. No more war pigs have the power, hand of God has struck the hour. Day of judgement, God is calling, on their knees, the war pigs crawling. Begging mercy for their sins, Satan laughing, spreads his wings. Oh lord, yeah!***](https://www.youtube.com/watch?v=bc5Nk1DXyEY) 🎵

---

🎵 *Please allow me to introduce myself, I'm a man of wealth and taste. I've been around for a long, long year. Stole many a man's soul and faith. I was 'round when Jesus Christ had his moment of doubt and pain. Made damn sure that Pilate washed his hands and sealed his fate.* 🎵

🎵 [***Pleased to meet you, hope you guess my name. But what's puzzlin' you is the nature of my game.***](https://www.youtube.com/watch?v=T8wiV29Znxg) 🎵

🎵 *I stuck around St. Petersburg, when I saw it was a time for a change. Killed Tsar and his ministers, Anastasia screamed in vain. I rode a tank, held a general's rank when the Blitzkrieg raged, and the bodies stank.* 🎵

🎵 [***Pleased to meet you, hope you guess my name, oh yeah. Ah what's puzzlin' you is the nature of my game, oh yeah.***](https://www.youtube.com/watch?v=T8wiV29Znxg) 🎵

🎵 *I watched with glee while your kings and queens fought for ten decades for the gods they made. I shouted out, "Who killed the Kennedys?" When after all, it was you and me. Let me please introduce myself, I'm a man of wealth and taste. And I laid traps for troubadours who get killed before they reach Bombay.* 🎵

🎵 [***Pleased to meet you, hope you guessed my name, oh yeah. But what's puzzlin' you is the nature of my game, oh yeah, get down heavy. Pleased to meet you, hope you guessed my name, oh yeah. But what's confusing you is just the nature of my game, um yeah.***](https://www.youtube.com/watch?v=T8wiV29Znxg) 🎵

🎵 *Just as every cop is a criminal, and all the sinners saints. As heads is tails, just call me Lucifer, cause I'm in need of some restraint. So if you meet me, have some courtesy, have some sympathy, and some taste. Use all your well-learned politesse or I'll lay your soul to waste, um yeah.* 🎵

🎵 [***Pleased to meet you, hope you guessed my name, um yeah. But what's puzzlin' you is the nature of my game, um mean it, get down.***](https://www.youtube.com/watch?v=T8wiV29Znxg) 🎵

🎵 *Woo, who, ah yeah, get on down, oh yeah ... ah yeah! Tell me baby, what's my name? Tell me honey, can ya guess my name? Tell me baby, what's my name? I tell you one time, you're to blame. Ooo, who, ooo, who, ooo, alright.* 🎵

🎵 *Ooo, who, who. Ooo, who, who. Ooo, who, who, ah, yeah. Ooo, who, who. Ooo, who, who, ah, yeah. What's my name? Tell me, baby, what's my name? Tell me, sweetie, what's my name? Ooo, who, who. Ooo, who, who. Ooo, who, who. Ooo, who, who. Ooo, who, who. Ooo, who, who. Ooo, who, who. Oh, yeah!* 🎵

---

🎵 *Another suburban family morning, grandmother screaming at the wall. We have to shout above the din of our Rice Krispies, we can't hear anything at all. Mother chants her litany of boredom and frustration, but we know all her suicides are fake. Daddy only stares into the distance, there's only so much more that he can take.* 🎵

🎵 [***Many miles away something crawls from the slime at the bottom of a dark Scottish lake.***](https://www.youtube.com/watch?v=o5FPPoLqkCk) 🎵

🎵 *Another industrial ugly morning, the factory belches filth into the sky. He walks unhindered through the picket lines today, he doesn't think to wonder why. The secretaries pout and preen like cheap tarts in a red light street, but all he ever thinks to do is watch. And every single meeting with his so called superior is a humiliating kick in the crotch.* 🎵

🎵 [***Many miles away something crawls to the surface of a dark Scottish loch.***](https://www.youtube.com/watch?v=o5FPPoLqkCk) 🎵

🎵 *Another working day has ended, only the rush hour hell to face. Packed like lemmings into shiny metal boxes, contestants in a suicidal race. Daddy grips the wheel and stares alone into the distance, he knows that something somewhere has to break. He sees the family home now looming in his headlights. The pain upstairs that makes his eyeballs ache.* 🎵

🎵 [***Many miles away there's a shadow on the door of a cottage on the shore of a dark Scottish lake. Many miles away, many miles away, many miles away, many miles away, many miles away.***](https://www.youtube.com/watch?v=o5FPPoLqkCk) 🎵

---

🎵 *Send in your skeletons, sing as their bones go marching in, again. The need you buried deep, the secrets that you keep are at the ready. Are you ready? I'm finished making sense. Done pleading ignorance, that whole defense. Spinning infinity, boy, the wheel is spinning me. It's never ending, never ending, same old story.* 🎵

🎵 [***What if I say I'm not like the others? What if I say I'm not just another one of your plays? You're the pretender. What if I say I will never surrender? What if I say I'm not like the others? What if I say I'm not just another one of your plays? You're the pretender. What if I say that I'll never surrender?***](https://www.youtube.com/watch?v=SBjQ9tuuTJQ) 🎵

🎵 *In time, or so I'm told. I'm just another soul for sale, oh, well. The page is out of print, we are not permanent, we're temporary, temporary. Same old story.* 🎵

🎵 [***What if I say I'm not like the others? What if I say I'm not just another one of your plays? You're the pretender. What if I say that I'll never surrender? What if I say I'm not like the others? What if I say I'm not just another one of your plays? You're the pretender. What if I say I will never surrender?***](https://www.youtube.com/watch?v=SBjQ9tuuTJQ) 🎵

🎵 *I'm the voice inside your head, you refuse to hear. I'm the face that you have to face, mirroring your stare. I'm what's left, I'm what's right, I'm the enemy. I'm the hand that'll take you down, bring you to your knees. So, who are you? Yeah, who are you? Yeah, who are you? Yeah, who are you?* 🎵

🎵 [***Keep you in the dark, you know they all pretend.***](https://www.youtube.com/watch?v=SBjQ9tuuTJQ) 🎵

🎵 [***What if I say I'm not like the others? What if I say I'm not just another one of your plays? You're the pretender. What if I say I will never surrender? What if I say I'm not like the others? What if I say I'm not just another one of your plays? You're the pretender. What if I say that I'll never surrender?***](https://www.youtube.com/watch?v=SBjQ9tuuTJQ) 🎵

🎵 [***(Keep you in the dark, you know they all pretend.) What if I say I'm not like the others? What if I say I'm not just another one of your plays? You're the pretender. What if I say I will never surrender? (Keep you in the dark, you know they all pretend.) What if I say I'm not like the others? What if I say I'm not just another one of your plays? You're the pretender. What if I say I will never surrender?***](https://www.youtube.com/watch?v=SBjQ9tuuTJQ) 🎵

🎵 *So, who are you? Yeah, who are you? Yeah, who are you?* 🎵

---

🎵 *Behold 'tis I the commander, whose grip controls you all. Resist me not, surrender, I won't compassion call.* 🎵

🎵 [***(Tyrant) Capture of humanity. (Tyrant) Conquerer of all. (Tyrant) Hideous destructor. (Tyrant) Every man shall fall.***](https://www.youtube.com/watch?v=XRXyCcyM7k4) 🎵

🎵 *Your very lives are held within my fingers, I snap them and you cower down in fear. You spineless things who belly down to slither, to the end of the world you follow to be near.* 🎵

🎵 [***(Tyrant) Capture of humanity. (Tyrant) Conquerer of all. (Tyrant) Hideous destructor. (Tyrant) Every man shall fall.***](https://www.youtube.com/watch?v=XRXyCcyM7k4) 🎵

🎵 [***Mourn for us oppressed in fear. Chained and shackled, we are bound. Freedom choked in dread we live, since tyrant was enthroned.***](https://www.youtube.com/watch?v=XRXyCcyM7k4) 🎵

🎵 *I listen not to sympathy, whilst ruler of this land. Withdraw your feeble aches and moans or suffer smite from this my hand.* 🎵

🎵 [***(Tyrant) Capture of humanity. (Tyrant) Conquerer of all. (Tyrant) Hideous destructor. (Tyrant) Every man shall fall.***](https://www.youtube.com/watch?v=XRXyCcyM7k4) 🎵

🎵 [***Mourn for us oppressed in fear. Chained and shackled, we are bound. Freedom choked in dread we live, since tyrant was enthroned.***](https://www.youtube.com/watch?v=XRXyCcyM7k4) 🎵

🎵 *My legions faithful unto death, I'll summon to my court. And as you perish each of you shall scream as you are sought.* 🎵

🎵 [***(Tyrant) Capture of humanity. (Tyrant) Conquerer of all. (Tyrant) Hideous destructor. (Tyrant) Every man shall fall.***](https://www.youtube.com/watch?v=XRXyCcyM7k4) 🎵

---

🎵 *You take a mortal man and put him in control. Watch him become a God. Watch people's heads a-roll, a-roll, a-roll.* 🎵

🎵 [***Just like the pied piper led rats through the streets. We dance like marionettes swaying to the symphony of destruction.***](https://www.youtube.com/watch?v=vfpgpf6QVnI) 🎵

🎵 *Acting like a robot. Its metal brain corrodes. You try to take its pulse before the head explodes, explodes, explodes.* 🎵

🎵 [***Just like the pied piper led rats through the streets. We dance like marionettes swaying to the symphony.***](https://www.youtube.com/watch?v=vfpgpf6QVnI) 🎵

🎵 [***Just like the pied piper led rats through the streets. We dance like marionettes swaying to the symphony, swaying to the symphony of destruction.***](https://www.youtube.com/watch?v=WdoXZf-FZyA) 🎵

🎵 *The Earth starts to rumble. World powers fall. A warring for the heavens. A peaceful man stands tall, a-tall, a-tall.* 🎵

🎵 [***Just like the pied piper led rats through the streets. We dance like marionettes swaying to the symphony.***](https://www.youtube.com/watch?v=vfpgpf6QVnI) 🎵

🎵 [***Just like the pied piper led rats through the streets. We dance like marionettes swaying to the symphony, swaying to the symphony of destruction.***](https://www.youtube.com/watch?v=WdoXZf-FZyA) 🎵

---

🎵 [***I am the one, Orgasmatron, the outstretched grasping hand. My image is of agony, my servants rape the land. Obsequious and arrogant, clandestine and vain. Two thousand years of misery, of torture in my name. Hypocrisy made paramount, paranoia the law. My name is called religion; sadistic, sacred whore.***](https://www.youtube.com/watch?v=7oGHqMijc3M) 🎵

🎵 [***I twist the truth, I rule the world, my crown is called deceit. I am the emperor of lies, you grovel at my feet. I rob you and I slaughter you, your downfall is my gain. And still you play the sycophant, and revel in your pain. And all my promises are lies, all my love is hate. I am the politician, and I decide your fate.***](https://www.youtube.com/watch?v=PnngnWjwtdM) 🎵

🎵 [***I march before a martyred world, an army for the fight. I speak of great heroic days, of victory and might. I hold a banner drenched in blood, I urge you to be brave. I lead you to your destiny, I lead you to your grave. Your bones will build my palaces, your eyes will stud my crown. For I am Mars, the God of War, and I will cut you down.***](https://www.youtube.com/watch?v=PnngnWjwtdM) 🎵

---

🎵 *I don't need to be a global citizen, 'cause I'm blessed by nationality. I'm a member of a growing populace, we enforce our popularity. There are things that seem to pull us under, and there are things that drag us down. But there's a power and a vital presence that's lurking all around.* 🎵

🎵 [***We've got the American Jesus, see him on the interstate. We've got the American Jesus, he helped build the president's estate.***](https://www.youtube.com/watch?v=12kcpP-8jfM) 🎵

🎵 *I feel sorry for the Earth's population, 'cause so few live in the USA. At least the foreigners can copy our morality. They can visit, but they cannot stay. Only precious few can garner the prosperity, it makes us walk with renewed confidence. We've got a place to go when we die, and the architect resides right here.* 🎵

🎵 [***We've got the American Jesus, bolstering national faith. We've got the American Jesus, overwhelming millions every day.***](https://www.youtube.com/watch?v=12kcpP-8jfM) 🎵

🎵 *He's the farmers' barren fields (in God), the force the army wields (we trust), the expression on the faces of the starving millions (because he's one of us), the power of the man (break down), the fuel that drives the Klan (cave in). He's the motive and the conscience of the murderer (we can redeem your sin). He's the preacher on TV (strong heart), the false sincerity (clear mind), the form letter that's written by the big computers (and infinitely kind), the nuclear bombs (you lose), the kids with no moms (we win), and I'm fearful that he's inside me (he is our champion), yeah!* 🎵

🎵 [***We've got the American Jesus, see him on the interstate. We've got the American Jesus, exercising his authority. We've got the American Jesus, bolstering national faith. We've got the American Jesus, overwhelming millions every day, yeah!***](https://www.youtube.com/watch?v=12kcpP-8jfM) 🎵

🎵 *One nation under God. One nation under God. One nation under God. One nation under God. One nation under God. One nation under God. One nation under God. One nation under God. One nation under God. One nation under God.* 🎵

---

🎵 *White man came across the sea. He brought us pain and misery. He killed our tribes, he killed our creed. He took our game for his own need. We fought him hard, we fought him well. Out on the plains, we gave him hell. But many came, too much for Cree. Oh, will we ever be set free?* 🎵

🎵 *Riding through dust clouds and barren wastes. Galloping hard on the plains. Chasing the redskins back to their holes. Fighting them at their own game. Murder for freedom, a stab in the back. Women and children and cowards attack.* 🎵

🎵 [***Run to the hills, run for your lives. Run to the hills, run for your lives.***](https://www.youtube.com/watch?v=djrUf9tz--o) 🎵

🎵 *Soldier blue in the barren wastes. Hunting and killing's a game. Raping the women and wasting the men. The only good Indians are tame. Selling them whiskey and taking their gold. Enslaving the young and destroying the old.* 🎵

🎵 [***Run to the hills, run for your lives. Run to the hills, run for your lives. Yeah. Run to the hills, run for your lives. Run to the hills, run for your lives. Run to the hills, run for your lives. Run to the hills, run for your lives.***](https://www.youtube.com/watch?v=djrUf9tz--o) 🎵

---

🎵 *Psychic spies from China try to steal your mind's elation. And little girls from Sweden dream of silver screen quotation. And if you want these kind of dreams, it's Californication.* 🎵

🎵 *It's the edge of the world, end all of western civilization. The sun may rise in the East, at least it's settled in the final location. It's understood that Hollywood sells Californication.* 🎵

🎵 *Pay your surgeon very well to break the spell of aging. Celebrity skin, is this your chin, or is that war you're waging? Firstborn unicorn, hardcore soft porn.* 🎵

🎵 [***Dream of Californication. Dream of Californication. Dream of Californication. Dream of Californication.***](https://www.youtube.com/watch?v=YlUKcNNmywk) 🎵

🎵 *Marry me, girl, be my fairy to the world, be my very own constellation. A teenage bride with a baby inside, gettin' high on information. And buy me a star on the boulevard, it's Californication.* 🎵

🎵 *Space may be the final frontier, but it's made in a Hollywood basement. And Cobain, can you hear the spheres, singing songs off station to station? And Alderaan's not far away, it's Californication.* 🎵

🎵 *Born and raised by those who praise control of population. Well, everybody's been there, and I don't mean on vacation. Firstborn unicorn, hardcore soft porn.* 🎵

🎵 [***Dream of Californication. Dream of Californication. Dream of Californication. Dream of Californication.***](https://www.youtube.com/watch?v=YlUKcNNmywk) 🎵

🎵 *Destruction leads to a very rough road, but it also breeds creation. And earthquakes are to a girl's guitar, they're just another good vibration. And tidal waves couldn't save the world from Californication.* 🎵

🎵 *Pay your surgeon very well to break the spell of aging. Sicker than the rest, there is no test, but this is what you're craving. Firstborn unicorn, hardcore soft porn.* 🎵

🎵 [***Dream of Californication. Dream of Californication. Dream of Californication. Dream of Californication.***](https://www.youtube.com/watch?v=YlUKcNNmywk) 🎵

---

🎵 *Conversion software version 7.0. Looking at life through the eyes of a tire hub. Eating seeds as a pastime activity. The toxicity of our city, of our city.* 🎵

🎵 [***You, what do you own, the world? How do you own disorder, disorder? Now, somewhere between the sacred silence, sacred silence and sleep. Somewhere between the sacred silence and sleep. Disorder, disorder, disorder.***](https://www.youtube.com/watch?v=iywaBOMvYLI) 🎵

🎵 *More wood for their fires, loud neighbors. Flashlight reveries caught in the headlights of a truck. Eating seeds as a pastime activity. The toxicity of our city, of our city.* 🎵

🎵 [***You, what do you own, the world? How do you own disorder, disorder? Now, somewhere between the sacred silence, sacred silence and sleep. Somewhere between the sacred silence and sleep. Disorder, disorder, disorder.***](https://www.youtube.com/watch?v=iywaBOMvYLI) 🎵

🎵 [***You, what do you own, the world? How do you own disorder, disorder? Now, somewhere between the sacred silence, sacred silence and sleep. Somewhere between the sacred silence and sleep. Disorder, disorder, disorder.***](https://www.youtube.com/watch?v=iywaBOMvYLI) 🎵

🎵 *When I became the sun, I shone life into the man's heart. When I became the sun, I shone life into the man's heart.* 🎵

---

🎵 *"There must be some way out of here", said the joker to the thief. "There's too much confusion, I can't get no relief. Businessmen, they drink my wine, plowmen dig my earth. None of them along the line, know what any of it is worth".* 🎵

🎵 *"No reason to get excited", the thief, he kindly spoke. "There are many here among us, who feel that life is but a joke. But you and I, we've been through that, and this is not our fate. So, let us not talk falsely now, the hour is getting late".* 🎵

🎵 [***All along the watchtower, princes kept the view. While all the women came and went, barefoot servants, too. Outside, in the distance, a wildcat did growl. Two riders were approaching, the wind began to howl.***](https://www.youtube.com/watch?v=bT7Hj-ea0VE) 🎵

---

🎵 *All the stories have been told of kings and days of old, but there's no England now (there's no England now). All the wars that were won and lost somehow don't seem to matter very much anymore. All the lies we were told (all the lies that we were told), all the lives of the people running round, their castles have burned. Oh, I see change, but inside we're the same as we ever were.* 🎵

🎵 [***Living on a thin line, ooh. Tell me now, what are we supposed to do? Living on a thin line (living on a thin line), ooh. Tell me now, what are we supposed to do? Living on a thin line (living on a thin line). Living this way, each day is a dream. What am I, what are we supposed to do? Living on a thin line (living on a thin line), ooh. Tell me now, what are we supposed to do?***](https://www.youtube.com/watch?v=yjOeWiPZeZw) 🎵

🎵 *Now another century nearly gone. What are we gonna leave for the young? What we couldn't do, what we wouldn't do, it's a crime, but does it matter? Does it matter much, does it matter much to you? Does it ever really matter? Yes, it really, really matters!* 🎵

🎵 [***Living on a thin line (living on a thin line), ooh. Tell me now, what are we supposed to do? Living on a thin line (living on a thin line), ooh. Tell me now, what are we supposed to do?***](https://www.youtube.com/watch?v=yjOeWiPZeZw) 🎵

🎵 *Then another leader says, "Break their hearts and break some heads." Is there nothing we can say or do? Blame the future on the past, always lost in bloody guts. And when they're gone, it's me and you.* 🎵

🎵 [***Living on a thin line, ooh. Tell me now, what are we supposed to do? Living on a thin line (living on a thin line), ooh. Tell me now, what are we supposed to do? Living on a thin line, ooh.***](https://www.youtube.com/watch?v=yjOeWiPZeZw) 🎵

---

🎵 *We don't need no education, we don't need no thought control. No dark sarcasm in the classroom. Teacher, leave them kids alone. Hey, teacher, leave them kids alone.* 🎵

🎵 [***All in all, it's just another brick in the wall. All in all, you're just another brick in the wall.***](https://www.youtube.com/watch?v=HrxX9TBj2zY) 🎵

🎵 *We don't need no education, we don't need no thought control. No dark sarcasm in the classroom. Teachers, leave them kids alone. Hey, teacher, leave us kids alone.* 🎵

🎵 [***All in all, you're just another brick in the wall. All in all, you're just another brick in the wall.***](https://www.youtube.com/watch?v=HrxX9TBj2zY) 🎵

🎵 *Wrong, do it again. Wrong, do it again. If you don't eat yer meat, you can't have any pudding. How can you have any pudding if you don't eat yer meat? You! (If you don't eat yer meat) Yes, you behind the bike stands. (you can't have any pudding.) How can you have any pudding if you don't eat yer meat? Stand still, laddy! You! Yes, you behind the bike stands...* 🎵

---

🎵 *(And during the few moments that we have left, we wanna talk right down to earth in a language that everybody here can easily understand.)* ― **Malcolm X, [Message to the Grass Roots](https://en.wikipedia.org/wiki/Message_to_the_Grass_Roots)** 🎵

🎵 [***Look in my eyes, what do you see? The cult of personality. I know your anger, I know your dreams. I've been everything you wanna be. Oh, I'm the cult of personality.***](https://www.youtube.com/watch?v=7xxgRUyzgs0) 🎵

🎵 [***Like Mussolini and Kennedy. I'm the cult of personality. The cult of personality. The cult of personality.***](https://www.youtube.com/watch?v=7xxgRUyzgs0) 🎵

🎵 *Neon lights, Nobel Prize. When a mirror speaks, the reflection lies. You won't have to follow me. Only you can set me free.* 🎵

🎵 [***I sell the things you need to be. I'm the smiling face on your TV. Oh, I'm the cult of personality. I exploit you, still you love me. I tell you one and one makes three. Oh, I'm the cult of personality.***](https://www.youtube.com/watch?v=7xxgRUyzgs0) 🎵

🎵 [***Like Joseph Stalin and Gandhi. Oh, I'm the cult of personality. The cult of personality. The cult of personality.***](https://www.youtube.com/watch?v=7xxgRUyzgs0) 🎵

🎵 *Neon lights, Nobel Prize. When a leader speaks, that leader dies. You won't have to follow me. Only you can set you free.* 🎵

🎵 [***You gave me fortune, you gave me fame. You gave me power in your God's name. I'm every person you need to be. Oh, I'm the cult of personality.***](https://www.youtube.com/watch?v=7xxgRUyzgs0) 🎵

🎵 [***I'm the cult of, I'm the cult of, I'm the cult of, I'm the cult of, I'm the cult of, I'm the cult of, I'm the cult of, I'm the cult of personality.***](https://www.youtube.com/watch?v=7xxgRUyzgs0) 🎵

🎵 *(Ask not what your country can do for you.)* ― **John F. Kennedy, [Inauguration](https://en.wikipedia.org/wiki/Inauguration_of_John_F._Kennedy)** 🎵

🎵 *(The only thing we have to fear is fear itself.)* ― **Franklin D. Roosevelt, [First inauguration](https://en.wikipedia.org/wiki/First_inauguration_of_Franklin_D._Roosevelt)** 🎵

---

🎵 *Come with it now, come with it now!* 🎵

🎵 *The microphone explodes, shattering the molds. Either drop the hits like [de la O](https://en.wikipedia.org/wiki/Genovevo_de_la_O), or get the fuck off the commode. With the sure shot, sure to make the bodies drop. Drop and don't copy yo, don't call this a co-op. Terror rains drenchin', quenchin' the thirst of the power dons, that five-sided Fist-a-gon. The rotten sore on the face of Mother Earth gets bigger. The trigger's cold, empty your purse.* 🎵

🎵 [***Rally 'round the family with a pocket full of shells. They rally 'round the family with a pocket full of shells. They rally 'round the family with a pocket full of shells. They rally 'round the family with a pocket full of shells.***](https://www.youtube.com/watch?v=3L4YrGaR8E4) 🎵

🎵 *Weapons, not food, not homes, not shoes. Not need, just feed the war cannibal animal. I walk the corner to the rubble that used to be a library. Line up to the mind cemetery now.* 🎵

🎵 *What we don't know keeps the contracts alive and movin'. They don't gotta burn the books, they just remove 'em. While arms warehouses fill as quick as the cells. Rally 'round the family, pocket full of shells.* 🎵

🎵 [***Rally 'round the family with a pocket full of shells. They rally 'round the family with a pocket full of shells. They rally 'round the family with a pocket full of shells. They rally 'round the family with a pocket full of shells.***](https://www.youtube.com/watch?v=3L4YrGaR8E4) 🎵

🎵 *Bulls on parade! Come with it now, come with it now! Bulls on parade, bulls on parade, bulls on parade, bulls on parade, bulls on parade!* 🎵

---

🎵 *Lightning smokes on the hill rise, brought the man with the warning light, shoutin' out, "You had better fly, while the darkness can help you hide." Trouble's comin' without control, no one's stayin' that's got a hope. Hurricane at the very least, in the words of the gypsy queen.* 🎵

🎵 [***Sign of the gypsy queen, pack your things and leave. Word of a woman who knows, take all your gold and you go.***](https://www.youtube.com/watch?v=LUwviJMVV5g) 🎵

🎵 *Get my saddle and tie it on, western wind moves fast and strong. Jump on back, he is good and long, we'll resist 'til we reach the dawn. Runnin' seems like the best defense, stayin' just don't make any sense. No one could ever stop it now, show the cards of the gypsy town.* 🎵

🎵 [***Sign of the gypsy queen, pack your things and leave. Word of a woman who knows, take all your gold and you go.***](https://www.youtube.com/watch?v=LUwviJMVV5g) 🎵

🎵 *Shadows moving without a sound, from the hold of the sleepless town. Evil seems to be everywhere, heed the spirit that brought despair. Trouble's comin' without control, no one's stayin' that's got a hope. Hurricane at the very least, in the words of the gypsy queen.* 🎵

🎵 [***Sign of the gypsy queen, pack your things and leave. Word of a woman who knows, take all your gold and you go. Sign of the gypsy queen, pack your things and leave. Word of a woman who knows, take all your gold and you go. Sign of the gypsy queen, pack your things and leave. Word of a woman who knows, take all your gold and you go.***](https://www.youtube.com/watch?v=LUwviJMVV5g) 🎵

---

🎵 *Where do we go from here, now that all other children are growin' up? And how do we spend our lives, if there's no one to lend us a hand?* 🎵

🎵 *I don't wanna live here no more, I don't wanna stay. Ain't gonna spend the rest of my life quietly fading away.* 🎵

🎵 [***Games people play, you take it or you leave it. Things that they say are not right. If I promise you the moon and the stars, would you believe me? Games people play in the middle of the night.***](https://www.youtube.com/watch?v=SLi7Ljcy6n8) 🎵

🎵 *Where do we go from here, now that all of the children have grown up? And how do we spend our time knowing nobody gives us a damn?* 🎵

🎵 *I don't wanna live here no more, I don't wanna stay. Ain't gonna spend the rest of my life quietly fading away.* 🎵

🎵 [***Games people play, you take it or you leave it. Things that they say, just don't make it right. If I'm telling you the truth right now, do you believe me? Games people play in the middle of the night.***](https://www.youtube.com/watch?v=SLi7Ljcy6n8) 🎵

🎵 [***Games people play, you take it or you leave it. Things that they say are not right. If I promise you the moon and the stars, would you believe me? Games people play in the middle of the night.***](https://www.youtube.com/watch?v=SLi7Ljcy6n8) 🎵

🎵 [***Games people play, you take it or you leave it. Things that they say, just don't make it right. If I'm telling you the truth right now, do you believe me? Games people play in the middle of the night.***](https://www.youtube.com/watch?v=SLi7Ljcy6n8) 🎵

---

🎵 *Welcome to the world, baby boy, I'll paint you red and white and blue. The indoctrination starts as soon as you come out the womb. Pretty quick we'll make you stupid with curriculums at school. And if the classroom doesn't do the trick, we'll make you watch the news.* 🎵

🎵 *Pick your team, right or left, pick the red pill or the blue. You can vote, but even if you win, still everyone will lose. Don't forget to buy designer, because Gucci makes you cool. We prioritize material belongings over truth.* 🎵

🎵 *Get a job that you can't stand so you can buy some cans of food. Go overseas and die for freedom, there's some oil we could use. Our democracy exists so that you think that you could choose. But our algorithms make you do what we want you to do.* 🎵

🎵 *What's the problem, you're depressed? Society has you confused? We got medication for you that you'll probably abuse. Go get married to a lady who also don't have a clue. And pump out a few babies that are just the same as you.* 🎵

🎵 [***Welcome to the system, everyone's a victim. Doesn't matter if you're black or white, it hates you all. Here inside the system, violence is a symptom. Fighting for what's right, but somehow everyone is wrong.***](https://www.youtube.com/watch?v=OshNahVo9-c) 🎵

🎵 *Welcome to the world, baby girl, I'll paint you pink if that's okay. We'll encourage self-destruction through the music that you play. We divided all the men by tryin' politics and race. And honestly, it's workin' awesome, so for you, we'll do the same.* 🎵

🎵 *Never teachin' you to love yourself, inject you full of hate. Objectify your sexuality then blame you for the rape. And weaponize the differences that make our men and women great. And just to screw with you, erase the genders, everyone's the same.* 🎵

🎵 *We'll empower you with rights to vote and fight for equal pay. Then have the men turn into women and you'll fight for them again. But you thought you had it figured out, but everything has changed. Welcome to the system, bitch, please enjoy your stay.* 🎵

🎵 *Here's a Bible and a bottle of the cheapest booze we make. Find a man who can take care of you to fill the holes we made. Buy a house and settle down, fulfill your duty, procreate. And make a couple babies who will also do the same.* 🎵

🎵 [***Welcome to the system, everyone's a victim. Doesn't matter if you're black or white, it hates you all. Here inside the system, violence is a symptom. Fighting for what's right, but somehow everyone is wrong.***](https://www.youtube.com/watch?v=OshNahVo9-c) 🎵

🎵 *Welcome to the world, everybody, I'mma paint you black and white. I'mma make you hate each other so that everyone will fight. I'mma give you all religion, let the righteous find the light. But I will also give you science to oppose the word of Christ.* 🎵

🎵 *And I'mma give you borders, they're imaginary lines. If you cross them, go to war, and win when everybody dies. And I'mma give you money that you'll value more than life. And let the one percent have everything while you fight to survive.* 🎵

🎵 *And then I'll give you politics, I'll call it left and right. And while you divide yourselves, I will conquer both the sides. Can't you see? I'm the system, my whole purpose is divide. What you choose will never matter because everything is mine.* 🎵

🎵 [***Welcome to the system, everyone's a victim. Doesn't matter if you're black or white, it hates you all. Here inside the system, violence is a symptom. Fighting for what's right, but somehow everyone is wrong.***](https://www.youtube.com/watch?v=OshNahVo9-c) 🎵

---

🎵 *I've been selling my soul, working all day. Overtime hours for bullshit pay. So I can sit out here and waste my life away. Drag back home and drown my troubles away.* 🎵

🎵 *It's a damn shame what the world's gotten to, for people like me, people like you. Wish I could just wake up and it not be true, but it is, oh, it is.* 🎵

🎵 [***Livin' in the new world with an old soul. These rich men, north of Richmond, Lord knows, they all just wanna have total control. Wanna know what you think, wanna know what you do. And they don't think you know, but I know that you do. 'Cause your dollar ain't shit, and it's taxed to no end, 'cause of rich men, north of Richmond.***](https://www.youtube.com/watch?v=sqSA-SY5Hro) 🎵

🎵 *I wish politicians would look out for miners, and not just minors on an island somewhere. Lord, we got folks in the street ain't got nothin' to [h]eat, and the obese milkin' welfare. But God, if you're five foot three and you're three hundred pounds, taxes ought not to pay for your bags of fudge rounds. Young men are putting themselves six feet in the ground, 'cause all this damn country does is keep on kicking them down.* 🎵

🎵 *Lord, it's a damn shame what the world's gotten to, for people like me, people like you. Wish I could just wake up and it not be true, but it is, oh, it is.* 🎵

🎵 [***Livin' in the new world with an old soul. These rich men, north of Richmond, Lord knows, they all just wanna have total control. Wanna know what you think, wanna know what you do. And they don't think you know, but I know that you do. 'Cause your dollar ain't shit, and it's taxed to no end, 'cause of rich men, north of Richmond.***](https://www.youtube.com/watch?v=sqSA-SY5Hro) 🎵

🎵 *I've been selling my soul, working all day. Overtime hours for bullshit pay.* 🎵

---

🎵 *Do I have to change my name? Will it get me far? Should I lose some weight? Am I gonna be a star?* 🎵

🎵 *I tried to be a boy, I tried to be a girl. I tried to be a mess, I tried to be the best. I guess I did it wrong, that's why I wrote this song. This type of modern life, is it for me? This type of modern life, is it for free?* 🎵

🎵 *So, I went into a bar looking for sympathy, a little company. I tried to find a friend, it's more easily said, it's always been the same. This type of modern life, is not for me. This type of modern life, is not for free.* 🎵

🎵 [***American life (American life). I live the American dream (American dream). You are the best thing I've seen. You are not just a dream (American life).***](https://www.youtube.com/watch?v=bZAMiK6ROZA) 🎵

🎵 *I tried to stay ahead, I tried to stay on top. I tried to play the part, but somehow I forgot, just what I did it for and why I wanted more. This type of modern life, is it for me? This type of modern life, is it for free?* 🎵

🎵 *Do I have to change my name? Will it get me far? Should I lose some weight? Am I gonna be a star?* 🎵

🎵 [***American life (American life). I live the American dream (American dream). You are the best thing I've seen. You are not just a dream (American life).***](https://www.youtube.com/watch?v=bZAMiK6ROZA) 🎵

🎵 *(American life) I tried to be a boy, tried to be a girl, tried to be a mess, tried to be the best, tried to find a friend, tried to stay ahead. I tried to stay on top, fuck it!* 🎵

🎵 *Do I have to change my name? Will it get me far? Should I lose some weight? Am I gonna be a star?* 🎵

🎵 *Oh, fuck it! Oh, fuck it! Oh, fuck it! Oh, fuck it!* 🎵

🎵 *I'm drinking a soy latte, I get a double shot, it goes right through my body and you know I'm satisfied.* 🎵

🎵 *I drive my Mini Cooper and I'm feeling super-duper. Yo, they tell I'm a trooper and you know I'm satisfied.* 🎵

🎵 *I do yoga and Pilates and the room is full of hotties, so I'm checking out the bodies and you know I'm satisfied.* 🎵

🎵 *I'm digging on the isotopes, this metaphysics shit is dope. And if all this can give me hope, you know I'm satisfied.* 🎵

🎵 *I got a lawyer and a manager, an agent and a chef. Three nannies, an assistant, and a driver and a jet. A trainer and a butler, and a bodyguard or five. A gardener and a stylist, do you think I'm satisfied?* 🎵

🎵 *I'd like to express my extreme point of view. I'm not a Christian and I'm not a Jew, I'm just living out the American dream, and I just realized that nothing is what it seems.* 🎵

🎵 *Do I have to change my name? Am I gonna be a star? Do I have to change my name? Am I gonna be a star? Do I have to change my name?* 🎵

---

🎵 [***'Cause it's a bitter sweet symphony, that's life. Tryin' to make ends meet, you're a slave to money, then you die. I'll take you down the only road I've ever been down. You know the one that takes you to the places where all the veins meet, yeah.***](https://www.youtube.com/watch?v=1lyu1KKwC74) 🎵

🎵 *No change, I can change, I can change, I can change. But I'm here in my mold, I am here in my mold. But I'm a million different people from one day to the next. I can't change my mould, no, no, no, no, no. (Have you ever been down?)* 🎵

🎵 *Well, I've never prayed, but tonight I'm on my knees, yeah. I need to hear some sounds that recognize the pain in me, yeah. I let the melody shine, let it cleanse my mind, I feel free now. But the airwaves are clean and there's nobody singing to me now.* 🎵

🎵 *No change, I can change, I can change, I can change. But I'm here in my mold, I am here in my mold. And I'm a million different people from one day to the next. I can't change my mould, no, no, no, no, no. (Have you ever been down?) I can't change it you know, I can't change it.* 🎵

🎵 [***'Cause it's a bitter sweet symphony, that's life. Tryin' to make ends meet, tryin' to find some money, then you die. I'll take you down the only road I've ever been down. You know the one that takes you to the places where all the veins meet, yeah.***](https://www.youtube.com/watch?v=1lyu1KKwC74) 🎵

🎵 *You know, I can change, I can change, I can change, I can change. But I'm here in my mold, I am here in my mold. And I'm a million different people from one day to the next. I can't change my mold, no, no, no, no, no. I can't change my mold, no, no, no, no, no. I can't change my mold, no, no, no, no, no.* 🎵

🎵 *It's just sex and violence, melody and silence. It's just I can't change my violence, melody and violence. I'll take you down the only road I've ever been down (sex and violence, melody and silence). It's just I can't change my violence, melody and silence. I'll take you down the only road I've ever been down (sex and violence, melody and silence). Been down, ever been down, ever been down, ever been down. Have you ever been down? Have you ever been down? Have you ever been down?* 🎵

---

🎵 *When I was a young boy, they said, "You're only gettin' older." But how was I to know then, that they'd be cryin' on my shoulder. Put your money in a big house, get yourself a pretty wife. She'll collect your life insurance, when she connects you with a knife.* 🎵

🎵 [***Somebody get the doctor, I'm feelin' pretty poor. Somebody get the stretcher, before I hit the floor. Somebody save me, I lost my job, they kicked me out of my tree. Somebody save me, save me.***](https://www.youtube.com/watch?v=LUkqBRC1zUA) 🎵

🎵 *And everybody's got opinions, but nobody's got the answers. And that shit you ate for breakfast, well, it'll only give you cancer. We're runnin' in a circle, runnin' to the morning light. And if ya ain't quite workable, it's been one hell of a night.* 🎵

🎵 [***Somebody get the doctor, I think I'm gonna crash. Never paid the bill, because I ain't got the cash. Somebody save me, I lost my job, they kicked me out of my tree. Somebody save me, save me, look out!***](https://www.youtube.com/watch?v=LUkqBRC1zUA) 🎵

🎵 [***Save me, I lost my job, they kicked me out of my tree. Somebody save me, oh, save me. Somebody save me, I'm goin' down for the last time, look out! Save me, (save), save me!***](https://www.youtube.com/watch?v=LUkqBRC1zUA) 🎵

---

🎵 *Lost in a dream, nothing is what it seems. Searching my head for the words that you said. Tears filled my eyes as we said our last goodbyes. This sad scene replays of you walking away.* 🎵

🎵 [***My body aches from mistakes, betrayed by lust. We lied to each other so much that in nothing we trust.***](https://www.youtube.com/watch?v=keRMhpjjn_U) 🎵

🎵 *Time and again, she repeats, "Let's be friends." I smile and say, "Yes", another truth bends, I must confess. I try to let go, but I know we'll never end 'til we're dust. We lied to each other again, (I wish I could trust) but I wish I could trust.* 🎵

🎵 [***My body aches from mistakes, betrayed by lust. We lied to each other so much that in nothing we trust (in nothing we trust). God help me please, on my knees (on my knees), betrayed by lust (by lust). We lied to each other so much (so much) now there's nothing we trust.***](https://www.youtube.com/watch?v=3qzBGYG786Y) 🎵

🎵 *How could this be happening to me? I'm lying when I say, "Trust me." I can't believe this is true. Trust hurts, why does trust equal suffering? Suffering.* 🎵

🎵 [***My body aches from mistakes, betrayed by lust. We lied to each other so much that in nothing we trust (in nothing we trust). God help me please, on my knees, betrayed by lust. We lied to each other so much (so much) now there's nothing we trust (nothing we trust). My body aches from mistakes, betrayed by lust. We lied to each other so much that there's nothing we trust. God help me please (help me please), on my knees, betrayed by lust. (Lust!) We lied to each other so much.***](https://www.youtube.com/watch?v=3qzBGYG786Y) 🎵

🎵 [***Absolutely in nothing we trust!***](https://www.youtube.com/watch?v=keRMhpjjn_U) 🎵

---

🎵 *Underneath the bridge, tarp has sprung a leak. And the animals I've trapped have all become my pets. And I'm living off of grass, and the drippings from my ceiling. It's okay to eat fish, 'cause they don't have any feelings.* 🎵

🎵 [***Something in the way, hmm-mmm. Something in the way, yeah, hmm-mmm. Something in the way, hmm-mmm. Something in the way, yeah, hmm-mmm. Something in the way, hmm-mmm. Something in the way, yeah, hmm-mmm.***](https://www.youtube.com/watch?v=4VxdufqB9zg) 🎵

🎵 *Underneath the bridge, tarp has sprung a leak. And the animals I've trapped have all become my pets. And I'm living off of grass, and the drippings from my ceiling. It's okay to eat fish, 'cause they don't have any feelings.* 🎵

🎵 [***Something in the way, hmm-mmm. Something in the way, yeah, hmm-mmm. Something in the way, hmm-mmm. Something in the way, yeah, hmm-mmm. Something in the way, hmm-mmm. Something in the way, yeah, hmm-mmm. Something in the way, hmm-mmm. Something in the way, yeah, hmm-mmm.***](https://www.youtube.com/watch?v=4VxdufqB9zg) 🎵

---

🎵 *In the misty morning, on the edge of time. We've lost the rising sun, a final sign. As the misty morning rolls away to die. Reaching for the stars, we blind the sky.* 🎵

🎵 *We sailed across the air before we learned to fly. We thought that it could never end. We'd glide above the ground before we learned to run, run. Now it seems our world has come undone.* 🎵

🎵 [***Oh, they say that it's over. That it just had to be. Ooh, they say that it's over. We're lost children of the sea, ooh, oh.***](https://www.youtube.com/watch?v=fMWIC7LMsKU) 🎵

🎵 *We made the mountains shake with laughter as we played. Hiding in our corner of the world. Then we did the demon dance and rushed to nevermore. Threw away the key and locked the door.* 🎵

🎵 [***Oh, they say that it's over, yeah. And it just had to be. Yes, they say that it's over. We're lost children of the sea, oh.***](https://www.youtube.com/watch?v=fMWIC7LMsKU) 🎵

🎵 *In the misty morning, on the edge of time. We've lost the rising sun, the final sign. As the misty morning rolls away to die. Reaching for the stars, we blind the sky.* 🎵

🎵 [***Oh, they say that it's over, yeah. And it just had to be. Oh, they say that it's over. Poor lost children of the sea, yeah.***](https://www.youtube.com/watch?v=fMWIC7LMsKU) 🎵

🎵 *Look out, the sky is falling down. Look out, the world is spinning 'round and 'round and 'round. Look out, the sun is going black, black. Look out, it's never, never, never coming back. Look out.* 🎵

---

🎵 *Mama told me when I was young, "Come sit beside me, my only son, and listen closely to what I say, and if you do this it'll help you some sunny day." Oh, yeah.* 🎵

🎵 *Oh, take your time, don't live too fast. Troubles will come, and they will pass. You'll find a woman, yeah, and you'll find love. And don't forget son there is someone up above.* 🎵

🎵 [***And be a simple kind of man. Oh, be something you love and understand. Baby, be a simple kind of man. Oh, won't you do this for me son, if you can?***](https://www.youtube.com/watch?v=8eNoms9wsGc) 🎵

🎵 *Forget your lust for the rich man's gold. All that you need is in your soul. And you can do this, oh, baby, if you try. All that I want for you, my son, is to be satisfied.* 🎵

🎵 [***And be a simple kind of man. Oh, be something you love and understand. Baby, be a simple kind of man. Oh, won't you do this for me son, if you can? Oh, yes, I will.***](https://www.youtube.com/watch?v=8eNoms9wsGc) 🎵

🎵 *Boy, don't you worry, you'll find yourself. Follow your heart, and nothing else. And you can do this, oh, baby, if you try. All that I want for you, my son, is to be satisfied.* 🎵

🎵 [***And be a simple kind of man. Oh, be something you love and understand. Baby, be a simple kind of man. Oh, won't you do this for me son, if you can? Baby, be a simple, be a simple man. Oh, be something you love and understand. Baby, be a simple kind of man.***](https://www.youtube.com/watch?v=8eNoms9wsGc) 🎵

---

🎵 [***Carry on, my wayward son, there'll be peace when you are done. Lay your weary head to rest, don't you cry no more.***](https://www.youtube.com/watch?v=2X_2IdybTV0) 🎵

🎵 *Once I rose above the noise and confusion, just to get a glimpse beyond this illusion. I was soaring ever higher, but I flew too high. Though my eyes could see, I still was a blind man. Though my mind could think, I still was a madman. I hear the voices when I'm dreaming, I can hear them say.* 🎵

🎵 [***Carry on, my wayward son, there'll be peace when you are done. Lay your weary head to rest, don't you cry no more.***](https://www.youtube.com/watch?v=2X_2IdybTV0) 🎵

🎵 *Masquerading as a man with a reason, my charade is the event of the season. And if I claim to be a wise man, well, it surely means that I don't know. On a stormy sea of moving emotion. Tossed about, I'm like a ship on the ocean. I set a course for winds of fortune, but I hear the voices say.* 🎵

🎵 [***Carry on, my wayward son, there'll be peace when you are done. Lay your weary head to rest, don't you cry no more.***](https://www.youtube.com/watch?v=2X_2IdybTV0) 🎵

---

🎵 *One day in the year of the fox, came a time remembered well, when the strong young man of the rising sun heard the tolling of the great black bell. One day in the year of the fox, when the bell began to ring, meant the time had come for one to go to the temple of the king.* 🎵

🎵 [***There in the middle of the circle he stands, searching, seeking. With just one touch of his trembling hand, the answer will be found. Daylight waits while the old man sings, "Heaven, help me." And then like the rush of a thousand wings, it shines upon the one. And the day had just begun.***](https://www.youtube.com/watch?v=B7nKzCRL_oo) 🎵

🎵 *One day in the year of the fox, came a time remembered well, when the strong young man of the rising sun heard the tolling of the great black bell. One day in the year of the fox, when the bell began to sing, it meant the time had come for the one to go to the temple of the king.* 🎵

🎵 [***There in the middle of the people he stands, seeing, feeling. With just a wave of a strong right hand, he's gone to the temple of the king. Far from the circle at the edge of the world, he's hoping, wondering. Thinking back from the stories he's heard of what he's gonna see.***](https://www.youtube.com/watch?v=B7nKzCRL_oo) 🎵

🎵 *And there in middle of the circle it lies, Heaven, help me. Then all could see by the shine in his eyes, the answer had been found. Back with the people in the circle he stands, giving, feeling. With just one touch of a strong right hand, they know of the temple and the king.* 🎵

---

🎵 [***Can't stop, addicted to the shindig. Chop Top, he says I'm gonna win big. Choose not a life of imitation. Distant cousin to the reservation. Defunct the pistol that you pay for. This punk, the feelin' that you stay for. In time, I want to be your best friend. East side love is living on the west end.***](https://www.youtube.com/watch?v=8DyziWtkfBw) 🎵

🎵 [***Knocked out, but boy, you better come to. Don't die, you know, the truth is some do. Go write your message on the pavement. Burn so bright, I wonder what the wave meant. White heat is screamin' in the jungle. Complete the motion if you stumble. Go ask the dust for any answers. Come back strong with fifty belly dancers.***](https://www.youtube.com/watch?v=8DyziWtkfBw) 🎵

🎵 [***The world I love, the tears I drop to be part of the wave, can't stop. Ever wonder if it's all for you? The world I love, the trains I hop to be part of the wave, can't stop. Come and tell me when it's time to.***](https://www.youtube.com/watch?v=8DyziWtkfBw) 🎵

🎵 *Sweetheart is bleeding in the snow cone. So smart, she's leading me to ozone. Music, the great communicator. Use two sticks to make it in the nature. I'll get you into penetration. The gender of a generation. The birth of every other nation. Worth your weight, the gold of meditation.* 🎵

🎵 *This chapter's gonna be a close one. Smoke rings, I know you're gonna blow one. All on a spaceship, persevering. Use my hands for everything but steering. Can't stop the spirits when they need you. Mop tops are happy when they feed you. J. Butterfly is in the treetop. Birds that blow the meaning into bebop.* 🎵

🎵 [***The world I love, the tears I drop to be part of the wave, can't stop. Ever wonder if it's all for you? The world I love, the trains I hop to be part of the wave, can't stop. Come and tell me when it's time to.***](https://www.youtube.com/watch?v=8DyziWtkfBw) 🎵

🎵 *Wait a minute, I'm passin' out, win or lose, just like you. Far more shocking than anything I ever knew. How about you? Ten more reasons why I need somebody new, just like you. Far more shocking than anything I ever knew. Right on cue.* 🎵

🎵 [***Can't stop, addicted to the shindig. Chop Top, he says I'm gonna win big. Choose not a life of imitation. Distant cousin to the reservation. Defunct the pistol that you pay for. This punk, the feelin' that you stay for. In time, I want to be your best friend. East side love is living on the west end.***](https://www.youtube.com/watch?v=8DyziWtkfBw) 🎵

🎵 [***Knocked out, but boy, you better come to. Don't die, you know, the truth is some do. Go write your message on the pavement. Burn so bright, I wonder what the wave meant.***](https://www.youtube.com/watch?v=8DyziWtkfBw) 🎵

🎵 *Kick-start the golden generator. Sweet talk but don't intimidate her. Can't stop the gods from engineering. Feel no need for any interfering.* 🎵

🎵 [***Your image in the dictionary. This life is more than ordinary. Can I get two, maybe even three of these? Comin' from space to teach you of the Pleiades. Can't stop the spirits when they need you. This life is more than just a read-through.***](https://www.youtube.com/watch?v=8DyziWtkfBw) 🎵

---

🎵 *Never straight and narrow, I won't keep in time. Tend to burn the arrow out of line. Been inclined to wander off the beaten track. That's where there's thunder and the wind shouts back.* 🎵

🎵 [***Grinder, looking for meat. Grinder, wants you to eat.***](https://www.youtube.com/watch?v=ncfTCyVSdWM) 🎵

🎵 *Got no use for routine, I shiver at the thought. Open skies are my scene, that's why I won't get caught. Refuse to bait the mantrap, be led to set the snare. I love to have my sight capped everywhere.* 🎵

🎵 [***Grinder, looking for meat. Grinder, wants you to eat.***](https://www.youtube.com/watch?v=ncfTCyVSdWM) 🎵

🎵 *I have my licence, it came with birth, for self reliance on this earth. You take the bullet on which my name was etched upon in your game.* 🎵

🎵 *Day of independence stamped us like a brand, round the necks of millions to the land. As the mighty eagle, I need room to breathe. Witness from the treadmill, I take my leave.* 🎵

🎵 [***Grinder, looking for meat. Grinder, wants you to eat. Grinder, looking for meat. Grinder, wants you to eat. Grinder.***](https://www.youtube.com/watch?v=ncfTCyVSdWM) 🎵

---

🎵 *What's up with what's going down in every city, in every town. Cramping styles is the plan. They've got us in the palm of every hand.* 🎵

🎵 [***When we pretend that we're dead, when we pretend that we're dead. They can't hear a word we've said, when we pretend that we're dead.***](https://www.youtube.com/watch?v=g-UOTOqtU0M) 🎵

🎵 *Come on, come on, come on, come on. Come on, come on, come on, come on. Come on, come on, come on, come on. Come on, come on, come on, come on.* 🎵

🎵 *Turn the tables with our unity. They're neither moral nor majority. Wake up and smell the coffee or just say no to individuality.* 🎵

🎵 [***When we pretend that we're dead (Pretend we're dead), when we pretend that we're dead (Pretend we're dead). They can't hear a word we've said (Pretend we're dead), when we pretend that we're dead.***](https://www.youtube.com/watch?v=g-UOTOqtU0M) 🎵

🎵 *Come on, come on, come on, come on. Come on, come on, come on, come on. Come on, come on, come on, come on. Come on, come on, come on, come on.* 🎵

🎵 [***When we pretend that we're dead (Pretend we're dead), when we pretend that we're dead (Pretend we're dead). They can't hear a word we've said (Pretend we're dead), when we pretend that we're dead.***](https://www.youtube.com/watch?v=g-UOTOqtU0M) 🎵

🎵 *Come on, come on, come on, come on. Dead... (Pretend we're dead) Dead... Come on, come on, come on, come on. Dead... (Pretend we're dead) Dead... Come on, come on, come on, come on. Dead... (Pretend we're dead) Dead... Come on, come on, come on, come on. Dead... (Pretend we're dead) Dead... Come on, come on, come on, come on. Dead... (Pretend we're dead) Dead... Come on, come on, come on, come on. Dead... (Pretend we're dead) Dead... Come on, come on, come on, come on. Dead... (Pretend we're dead) Dead... Come on, come on, come on, come on. Dead... (Pretend we're dead)* 🎵

---

🎵 [***I am flesh and I am bone. Rise up, ting ting, like glitter and gold. I've got fire in my soul. Rise up, ting ting, like glitter. Yeah, like glitter and gold. Yeah, like glitter.***](https://www.youtube.com/watch?v=GySIToHCPac) 🎵

🎵 *Do you walk in the valley of kings? Do you walk in the shadow of men who sold their lives to a dream? Do you ponder the manner of things in the dark? The dark, the dark, the dark.* 🎵

🎵 [***I am flesh and I am bone. Arise, ting ting, like glitter and gold. I've got fire in my soul. Rise up, ting ting, like glitter. Yeah, like glitter and gold. Yeah, like glitter.***](https://www.youtube.com/watch?v=GySIToHCPac) 🎵

🎵 *Do you walk in the meadow of spring? Do you talk to the animals? Do you hold their lives from a string? Do you ponder the manner of things in the dark? The dark, the dark, the dark.* 🎵

🎵 [***I am flesh and I am bone. Arise, ting ting, like glitter and gold. I've got fire in my soul. Rise up, ting ting, like glitter. Yeah, like glitter and gold. Yeah, like glitter.***](https://www.youtube.com/watch?v=GySIToHCPac) 🎵

🎵 *'Cause everybody in the back room's spinning up. Don't remember what you're asking for. And everybody in the front room's tripping out. You left your bottle at the door.* 🎵

🎵 [***I am flesh and I am bone. Arise, ting ting, like glitter and gold. I've got fire in my soul. Rise up, ting ting, like glitter. Yeah, like glitter and gold. Yeah, like glitter. Yeah, like glitter and gold. Yeah, like glitter.***](https://www.youtube.com/watch?v=GySIToHCPac) 🎵

---

🎵 [***You will not be able to stay home, brother. You will not be able to plug in, turn on, and cop out. You will not be able to lose yourself on skag and skip out for beer during commercials, because the revolution will not be televised.***](https://www.youtube.com/watch?v=vwSRqaZGsPw) 🎵

🎵 [***The revolution will not be televised. The revolution will not be brought to you by Xerox, in four parts without commercial interruptions. The revolution will not show you pictures of Nixon blowing a bugle and leading a charge by John Mitchell, General Abrams, and Spiro Agnew to eat hog maws confiscated from a Harlem sanctuary. The revolution will not be televised.***](https://www.youtube.com/watch?v=vwSRqaZGsPw) 🎵

🎵 [***The revolution will not be brought to you by the Schaefer Award Theatre and will not star Natalie Woods and Steve McQueen or Bullwinkle and Julia. The revolution will not give your mouth sex appeal. The revolution will not get rid of the nubs. The revolution will not make you look five pounds thinner, because the revolution will not be televised, brother.***](https://www.youtube.com/watch?v=vwSRqaZGsPw) 🎵

🎵 [***There will be no pictures of you and Willie May pushing that shopping cart down the block on the dead run or trying to slide that color TV into a stolen ambulance. NBC will not be able predict the winner at 8:32 or report from 29 districts. The revolution will not be televised.***](https://www.youtube.com/watch?v=vwSRqaZGsPw) 🎵

🎵 *There will be no pictures of pigs shooting down brothers on the instant replay. There will be no pictures of pigs shooting down brothers on the instant replay. There will be no pictures of Whitney Young being run out of Harlem on a rail with a brand new process. There will be no slow motion or still lifes of Roy Wilkens strolling through Watts in a red, black and green liberation jumpsuit, that he had been saving for just the proper occasion.* 🎵

🎵 [***Green Acres, Beverly Hillbillies, and Hooterville Junction will no longer be so damned relevant, and women will not care if Dick finally got down with Jane on "Search for Tomorrow", because black people will be in the street looking for a brighter day. The revolution will not be televised.***](https://www.youtube.com/watch?v=vwSRqaZGsPw) 🎵

🎵 [***There will be no highlights on the eleven o'clock news and no pictures of hairy armed women liberationists and Jackie Onassis blowing her nose. The theme song will not be written by Jim Webb or Francis Scott Key, nor sung by Glen Campbell, Tom Jones, Johnny Cash, Englebert Humperdink or the Rare Earth. The revolution will not be televised.***](https://www.youtube.com/watch?v=vwSRqaZGsPw) 🎵

🎵 [***The revolution will not be right back after a message about a white tornado, white lightning or white people. You will not have to worry about a dove in your bedroom, the tiger in your tank or the giant in your toilet bowl. The revolution will not go better with Coke. The revolution will not fight germs that may cause bad breath. The revolution will put you in the driver's seat.***](https://www.youtube.com/watch?v=vwSRqaZGsPw) 🎵

🎵 [***The revolution will not be televised, will not be televised, will not be televised, will not be televised. The revolution will be no re-run, brothers. The revolution will be live.***](https://www.youtube.com/watch?v=vwSRqaZGsPw) 🎵

---

🎵 *For a price I'd do about anything, except pull the trigger, for that I'd need a pretty good cause. Then I heard of Dr. X, the man with the cure. Just watch the television, yeah, you'll see there's something going on.* 🎵

🎵 *Got no love for politicians or that crazy scene in D.C., it's just a power mad town. But the time is ripe for changes. There's a growing feeling that taking a chance on a new kind of vision is due.* 🎵

🎵 *I used to trust the media to tell me the truth, tell us the truth. But now I've seen the payoffs everywhere I look. Who do you trust when everyone's a crook?* 🎵

🎵 [***Revolution calling, revolution calling, revolution calling you. There's a revolution calling, revolution calling. Gotta make a change, gotta push, gotta push it on through.***](https://www.youtube.com/watch?v=CNdOsL4Xe7Q) 🎵

🎵 *Well I'm tired of all this bullshit they keep selling me on T.V. about the communist plan. And all the shady preachers, begging for my cash. Swiss bank accounts while giving their secretaries the slam.* 🎵

🎵 *They're all in Penthouse now or Playboy magazine, million dollar stories to tell. I guess Warhol wasn't wrong. Fame fifteen minutes long. Everyone's using everybody, making the sale.* 🎵

🎵 *I used to think that only America's way, way was right. But now the holy dollar rules everybody's lives. Gotta make a million doesn't matter who dies.* 🎵

🎵 [***Revolution calling, revolution calling, revolution calling you. There's a revolution calling, revolution calling. Gotta make a change, gotta push, gotta push it on through.***](https://www.youtube.com/watch?v=aq9gVp-Wk7I) 🎵

🎵 *I used to trust the media to tell me the truth, tell us the truth. But now I've seen the payoffs everywhere I look. Who do you trust when everyone's a crook?* 🎵

🎵 [***Revolution calling, revolution calling, revolution calling you. There's a revolution calling, revolution calling. Gotta make a change, gotta push, gotta push it on through.***](https://www.youtube.com/watch?v=CNdOsL4Xe7Q) 🎵

---

🎵 *(Next message, saved Saturday at 9:24 PM "Sorry, I'm just... it's starting to hit me like a um... um... two ton heavy thing")* 🎵

🎵 *Last night the word came down, ten dead in Chinatown. Innocent, their only crime was being in the wrong place at the wrong time. "Too bad", people say, "What's wrong with the kids today?" I tell you right now, they've got nothing to lose. They're building empire.* 🎵

🎵 *Johnny used to work after school at the cinema show. Gotta hustle if he wants an education. Yeah, he's got a long way to go. Now he's out on the streets all day, selling crack to the people who pay. Got an AK-47 for his best friend, business the American way.* 🎵

🎵 *East side meets West side downtown. No time, the walls fall down.* 🎵

🎵 [***Can't you feel it coming? (Empire) Can't you hear it calling?***](https://www.youtube.com/watch?v=NSTct2FFamw) 🎵

🎵 *Black man, trapped again. Holds his chains in his hand. Brother killing brother for the profit of another. Game point, nobody wins.* 🎵

🎵 *Declines, right on time. What happened to the dream sublime? Tear it all down, we'll put it up again. Another empire.* 🎵

🎵 *East side meets West side downtown. No time, no line, the walls fall down.* 🎵

🎵 [***Can't you feel it coming? (Empire) Can't you hear it coming? (Empire) Can't someone here stop it?***](https://www.youtube.com/watch?v=NSTct2FFamw) 🎵

🎵 *"In fiscal year 1986-87 the local, state, and federal governments spent a combined total of 16.6 billion dollars on law enforcement. Federal law enforcement expenditures ranked last in absolute dollars and accounted for only 6 percent of all federal spending. By way of comparison, the federal government spent 24 million more on space exploration, and 43 times more on national defense and international relations, than on law enforcement."* 🎵

🎵 [***Can't you feel it coming? (Empire) Can't you hear it calling? (Empire) Can't you feel it coming? (Empire) Can't you hear it calling? (Empire)***](https://www.youtube.com/watch?v=NSTct2FFamw) 🎵

🎵 [***Can't you feel it coming? (Empire) Can't you hear it calling? (Empire) Can't you feel it coming? (Empire) Can't someone here stop it? (Empire)***](https://www.youtube.com/watch?v=NSTct2FFamw) 🎵

---

🎵 *Time keeps on slippin', slippin', slippin' into the future. Time keeps on slippin', slippin', slippin' into the future.* 🎵

🎵 [***I want to fly like an eagle to the sea. Fly like an eagle, let my spirit carry me. I want to fly like an eagle 'til I'm free. Oh, Lord, through the revolution.***](https://www.youtube.com/watch?v=WuXwSyahgW4) 🎵

🎵 *Feed the babies who don't have enough to eat. Shoe the children with no shoes on their feet. House the people livin' in the street. Oh, oh, there's a solution.* 🎵

🎵 [***I want to fly like an eagle to the sea. Fly like an eagle, let my spirit carry me. I want to fly like an eagle 'til I'm free. Right through the revolution.***](https://www.youtube.com/watch?v=WuXwSyahgW4) 🎵

🎵 *Time keeps on slippin', slippin', slippin' into the future. Time keeps on slippin', slippin', slippin' into the future. Time keeps on slippin', slippin', slippin' into the future. Time keeps on slippin', slippin', slippin' into the future.* 🎵

🎵 [***I want to fly like an eagle to the sea. Fly like an eagle, let my spirit carry me. I want to fly like an eagle 'til I'm free. Right through the revolution.***](https://www.youtube.com/watch?v=WuXwSyahgW4) 🎵

🎵 *Time keeps on slippin', slippin', slippin' into the future. Time keeps on slippin', slippin', slippin' into the future.* 🎵

---

🎵 *Now in the street there is violence and, and a lots of work to be done. No place to hang out our washing and, and I can't blame all on the sun.* 🎵

🎵 [***(Oh, no) We gonna rock down to Electric Avenue, and then we'll take it higher. (Oh) We gonna rock down to Electric Avenue, and then we'll take it higher.***](https://www.youtube.com/watch?v=Yxep-9BQ6Uo) 🎵

🎵 *Workin' so hard like a soldier, can't afford a thing on TV. Deep in my heart, I abhor ya'. Can't get food for them kid.* 🎵

🎵 [***(Good God) We gonna rock down to Electric Avenue, and then we'll take it higher. (Oh) We gonna rock down to Electric Avenue, and then we'll take it higher. Oh, no. Oh, no. Oh, no. Oh, no. (Oh God) We gonna rock down to Electric Avenue, and then we'll take it higher. (Oh) We gonna rock down to Electric Avenue, and then we'll take it higher.***](https://www.youtube.com/watch?v=Yxep-9BQ6Uo) 🎵

🎵 *Who is to blame in one country? Never can get to the one. Dealin' in multiplication, and they still can't feed everyone.* 🎵

🎵 [***(Oh, no) We gonna rock down to Electric Avenue, and then we'll take it higher. (Oh, no) We gonna rock down to Electric Avenue, and then we'll take it higher. Oh, out in the street, out in the street, out in the daytime, out in the night.***](https://www.youtube.com/watch?v=Yxep-9BQ6Uo) 🎵

🎵 [***(Oh) We gonna rock down to Electric Avenue, and then we'll take it higher. (Oh) We gonna rock down to Electric Avenue, and then we'll take it higher. Out in the street, out in the street, out in the playground, in the dark side of town.***](https://www.youtube.com/watch?v=Yxep-9BQ6Uo) 🎵

🎵 [***(Oh) We gonna rock down to Electric Avenue, and then we'll take it higher. (Hey) We gonna rock down to Electric Avenue, (oh, yeah) and then we'll take it higher. (Rock it in the daytime) We gonna rock down to Electric Avenue, (rock it in the night) and then we'll take it higher (Electric Avenue). (Rock it in Miami, mama, meh) We gonna rock down to (Electric Avenue) (whoa in a Brixton) and then we'll take it higher (Electric Avenue).***](https://www.youtube.com/watch?v=Yxep-9BQ6Uo) 🎵

---

🎵 *Paranoia is in bloom. The PR transmissions will resume. They'll try to push drugs that keep us all dumbed down, and hope that we will never see the truth around. So come on.* 🎵

🎵 *Another promise, another scene, another packaged lie to keep us trapped in greed. And all the green belts wrapped around our minds, and endless red tape to keep the truth confined. So come on.* 🎵

🎵 [***They will not force us, and they will stop degrading us, and they will not control us, and we will be victorious. So come on.***](https://www.youtube.com/watch?v=w8KQmps-Sog) 🎵

🎵 *Interchanging mind control. Come let the revolution take its toll. If you could flick a switch and open your third eye. You'd see that we should never be afraid to die. So come on.* 🎵

🎵 *Rise up and take the power back. It's time the fat cats had a heart attack. You know that their time's coming to an end. We have to unify and watch our flag ascend. So come on.* 🎵

🎵 [***They will not force us, they will stop degrading us, they will not control us, and we will be victorious. So come on.***](https://www.youtube.com/watch?v=w8KQmps-Sog) 🎵

🎵 [***They will not force us, they will stop degrading us, they will not control us, and we will be victorious. So come on.***](https://www.youtube.com/watch?v=w8KQmps-Sog) 🎵

---

🎵 *Come ride with me through the veins of history. I'll show you how God falls asleep on the job.* 🎵

🎵 *And how can we win when fools can be kings? Don't waste your time or time will waste you.* 🎵

🎵 [***No one's gonna take me alive. The time has come to make things right. You and I must fight for our rights. You and I must fight to survive.***](https://www.youtube.com/watch?v=G_sBOsh-vyI) 🎵

---

🎵 *We'll be fighting in the streets with our children at our feet. And the morals that they worship will be gone. And the men who spurred us on sit in judgement of all wrong. They decide and the shotgun sings the song.* 🎵

🎵 [***I'll tip my hat to the new Constitution. Take a bow for the new revolution. Smile and grin at the change all around. Pick up my guitar and play, just like yesterday. Then I'll get on my knees and pray we don't get fooled again.***](https://www.youtube.com/watch?v=_NzLs-xSss0) 🎵

🎵 *A change, it had to come. We knew it all along. We were liberated from the fold, that's all. And the world looks just the same, and history ain't changed. 'Cause the banners, they all flown in the last war.* 🎵

🎵 [***I'll tip my hat to the new Constitution. Take a bow for the new revolution. Smile and grin at the change all around. Pick up my guitar and play, just like yesterday. Then I'll get on my knees and pray we don't get fooled again, no, no.***](https://www.youtube.com/watch?v=_NzLs-xSss0) 🎵

🎵 *I'll move myself and my family aside, if we happen to be left half-alive. I'll get all my papers and smile at the sky. For I know that the hypnotized never lie. Do ya'? Yeah!* 🎵

🎵 *There's nothing in the street, looks any different to me. And the slogans are effaced, by-the-bye. And the parting on the left is now parting on the right. And the beards have all grown longer overnight.* 🎵

🎵 [***I'll tip my hat to the new Constitution. Take a bow for the new revolution. Smile and grin at the change all around. Pick up my guitar and play, just like yesterday. Then I'll get on my knees and pray we don't get fooled again, don't get fooled again, no, no.***](https://www.youtube.com/watch?v=_NzLs-xSss0) 🎵

🎵 *Yeah!!! Meet the new boss, same as the old boss.* 🎵

---

🎵 [***So close, no matter how far. Couldn't be much more from the heart. Forever trusting who we are, and nothing else matters.***](https://www.youtube.com/watch?v=tAGnKpE4NCI) 🎵

🎵 [***Never opened myself this way. Life is ours, we live it our way. All these words, I don't just say, and nothing else matters.***](https://www.youtube.com/watch?v=tAGnKpE4NCI) 🎵

🎵 [***Trust I seek and I find in you. Every day for us something new. Open mind for a different view, and nothing else matters.***](https://www.youtube.com/watch?v=tAGnKpE4NCI) 🎵

🎵 *Never cared for what they do. Never cared for what they know. But I know.* 🎵

🎵 [***So close, no matter how far. Couldn't be much more from the heart. Forever trusting who we are, and nothing else matters.***](https://www.youtube.com/watch?v=tAGnKpE4NCI) 🎵

🎵 *Never cared for what they do. Never cared for what they know. But I know.* 🎵

🎵 [***Never opened myself this way. Life is ours, we live it our way. All these words, I don't just say, and nothing else matters.***](https://www.youtube.com/watch?v=tAGnKpE4NCI) 🎵

🎵 [***Trust I seek and I find in you. Every day for us something new. Open mind for a different view, and nothing else matters.***](https://www.youtube.com/watch?v=tAGnKpE4NCI) 🎵

🎵 *Never cared for what they say. Never cared for games they play. Never cared for what they do. Never cared for what they know. And I know.* 🎵

🎵 [***So close, no matter how far. It couldn't be much more from the heart. Forever trusting who we are, and nothing else matters.***](https://www.youtube.com/watch?v=tAGnKpE4NCI) 🎵

---

🎵 *Well, I've been watching, while you've been coughing. I've been drinking life, while you've been nauseous. And so I drink to health, while you kill yourself. And I've got just one thing that I can offer.* 🎵

🎵 [***Go on and save yourself, and take it out on me. Go on and save yourself, and take it out on me, yeah.***](https://www.youtube.com/watch?v=KDMvN45sjo4) 🎵

🎵 *I'm not a martyr, I'm not a prophet. And I won't preach to you, but here's a caution. You better understand, that I won't hold your hand. But if it helps you mend, then I won't stop it.* 🎵

🎵 [***Go on and save yourself, and take it out on me. Go on and save yourself, and take it out on me.***](https://www.youtube.com/watch?v=KDMvN45sjo4) 🎵

🎵 [***Go on and save yourself, and take it out on me. Go on and save yourself, and take it out on me, yeah.***](https://www.youtube.com/watch?v=KDMvN45sjo4) 🎵

🎵 *Drown if you want, and I'll see you at the bottom. Where you'll crawl on my skin, and put the blame on me, so you don't feel a thing!* 🎵

🎵 [***Go on and save yourself, and take it out on me. Go on and save yourself, and take it out on me.***](https://www.youtube.com/watch?v=KDMvN45sjo4) 🎵

🎵 [***Go on and save yourself, and take it out on me. Go on and save yourself, and take it out on me, yeah.***](https://www.youtube.com/watch?v=KDMvN45sjo4) 🎵

---

🎵 *Whatsoever I've feared has come to life. And whatsoever I've fought off, became my life. Just when every day seemed to greet me with a smile. Sunspots have faded, now I'm doing time, now I'm doing time.* 🎵

🎵 [***'Cause I fell on black days, I fell on black days.***](https://www.youtube.com/watch?v=aqW-VF49Sok) 🎵

🎵 *Whomsoever I've cured, I've sickened now. And whomsoever I've cradled, I've put you down. Searchlight so they say, but I can't see it in the night. I'm only faking when I get it right, when I get it right.* 🎵

🎵 [***'Cause I fell on black days, I fell on black days.***](https://www.youtube.com/watch?v=aqW-VF49Sok) 🎵

🎵 *How would I know that this could be my fate? How would I know that this could be my fate?* 🎵

🎵 *Do what you wanted to see good, has made you blind. And what you wanted to be yours, has made it mine. So don't you lock up something that you wanted to see fly. Hands are for shaking, no, not tying. No, not tying.* 🎵

🎵 *I sure don't mind a change. I sure don't mind a change. Yeah, I sure don't mind, I sure don't mind a change. I sure don't mind a change.* 🎵

🎵 [***'Cause I fell on black days, I fell on black days.***](https://www.youtube.com/watch?v=aqW-VF49Sok) 🎵

🎵 *How would I know that this could be my fate? How would I know that this could be my fate? How would I know that this could be my fate? How would I know that this could be my fate?* 🎵

🎵 *I sure don't mind a change.* 🎵

---

🎵 *In your life, you seem to have it all, you seem to have control, but deep within your soul you're losing it. You never took the time, assume that you're to blame. You think that you're insane? Won't you spare me?* 🎵

🎵 [***I know the breakdown. Everything is gonna shake now someday. I know the breakdown. Tell me again, am I awake now, maybe? You can find the reason that no one else is living this way, living this way.***](https://www.youtube.com/watch?v=VjATZYHs4Tc) 🎵

🎵 *Yeah, your lies. Your world is built around two faces of a clown. Voices in your head think there's four pawns down. But in this unity, faith has found the need. So you better check yourself before you check out.* 🎵

🎵 [***I know the breakdown. Everything is gonna shake now someday. I know the breakdown. Tell me again, am I awake now, maybe? You can find the reason that no one else is living this way, living this way.***](https://www.youtube.com/watch?v=VjATZYHs4Tc) 🎵

🎵 *If you find yourself, then you might believe. Then within yourself, you just might conceive.* 🎵

🎵 [***I know the breakdown. Everything is gonna shake now someday. I know the breakdown. Tell me again, am I awake now, maybe? You can find the reason that no one else is living this way, living this way. You can find the reason that no one else is living this way. You can find the reason that no one else is living this way.***](https://www.youtube.com/watch?v=VjATZYHs4Tc) 🎵

---

🎵 *Someone falls to pieces sleeping all alone. Someone kills the pain. Spinning in the silence, she finally drifts away.* 🎵

🎵 *Someone gets excited in a chapel yard, and catches a bouquet. Another lays a dozen white roses on a grave, yeah.* 🎵

🎵 [***And to be yourself is all that you can do, hey. To be yourself is all that you can do.***](https://www.youtube.com/watch?v=WC5FdFlUcl0) 🎵

🎵 *Someone finds salvation in everyone. Another only pain. Someone tries to hide himself, down inside himself, he prays.* 🎵

🎵 *Someone swears his true love until the end of time. Another runs away. Separate or united, healthy or insane.* 🎵

🎵 [***And to be yourself is all that you can do, hey. To be yourself is all that you can do. To be yourself is all that you can do, hey. Be yourself is all that you can do.***](https://www.youtube.com/watch?v=WC5FdFlUcl0) 🎵

🎵 *And even when you've paid enough, been pulled apart or been held up. With every single memory of the good or bad faces of luck. Don't lose any sleep tonight, I'm sure everything will end up alright. You may win or lose.* 🎵

🎵 [***But to be yourself is all that you can do, yeah. To be yourself is all that you can do, oh. To be yourself is all that you can do, hey. To be yourself is all that you can do.***](https://www.youtube.com/watch?v=WC5FdFlUcl0) 🎵

🎵 [***To be yourself is all that you can. Be yourself is all that you can. Be yourself is all that you can do.***](https://www.youtube.com/watch?v=WC5FdFlUcl0) 🎵

---

🎵 *Somebody once told me the world is gonna roll me. I ain't the sharpest tool in the shed. She was looking kind of dumb with her finger and her thumb in the shape of an "L" on her forehead.* 🎵

🎵 *Well, the years start coming and they don't stop coming. Fed to the rules and I hit the ground running. Didn't make sense not to live for fun. Your brain gets smart, but your head gets dumb.* 🎵

🎵 *So much to do, so much to see, so what's wrong with taking the back streets? You'll never know if you don't go. You'll never shine if you don't glow.* 🎵

🎵 [***Hey now, you're an all star, get your game on, go play. Hey now, you're a rock star, get the show on, get paid. And all that glitters is gold. Only shooting stars break the mold.***](https://www.youtube.com/watch?v=L_jWHffIx5E) 🎵

🎵 *It's a cool place and they say it gets colder. You're bundled up now, wait 'til you get older. But the meteor men beg to differ. Judging by the hole in the satellite picture.* 🎵

🎵 *The ice we skate is getting pretty thin. The water's getting warm so you might as well swim. My world's on fire, how about yours? That's the way I like it and I never get bored.* 🎵

🎵 [***Hey now, you're an all star, get your game on, go play. Hey now, you're a rock star, get the show on, get paid. And all that glitters is gold. Only shooting stars break the mold.***](https://www.youtube.com/watch?v=L_jWHffIx5E) 🎵

🎵 [***Hey now, you're an all star, get your game on, go play. Hey now, you're a rock star, get the show on, get paid. And all that glitters is gold. Only shooting stars.***](https://www.youtube.com/watch?v=L_jWHffIx5E) 🎵

🎵 *Somebody once asked, "Could I spare some change for gas? I need to get myself away from this place." I said, "Yep, what a concept. I could use a little fuel myself, and we could all use a little change."* 🎵

🎵 *Well, the years start coming and they don't stop coming. Fed to the rules and I hit the ground running. Didn't make sense not to live for fun. Your brain gets smart, but your head gets dumb.* 🎵

🎵 *So much to do, so much to see, so what's wrong with taking the back streets? You'll never know if you don't go. You'll never shine if you don't glow.* 🎵

🎵 [***Hey now, you're an all star, get your game on, go play. Hey now, you're a rock star, get the show on, get paid. And all that glitters is gold. Only shooting stars break the mold. And all that glitters is gold. Only shooting stars break the mold.***](https://www.youtube.com/watch?v=L_jWHffIx5E) 🎵

---

🎵 [***I look at you all, see the love there that's sleeping. While my guitar gently weeps.***](https://www.youtube.com/watch?v=YFDg-pgE0Hk) 🎵

🎵 [***I look at the floor and I see it needs sweeping. Still my guitar gently weeps.***](https://www.youtube.com/watch?v=YFDg-pgE0Hk) 🎵

🎵 *I don't know why nobody told you how to unfold your love. I don't know how someone controlled you. They bought and sold you.* 🎵

🎵 [***I look at the world and I notice it's turning. While my guitar gently weeps.***](https://www.youtube.com/watch?v=YFDg-pgE0Hk) 🎵

🎵 [***With every mistake we must surely be learning. Still my guitar gently weeps.***](https://www.youtube.com/watch?v=YFDg-pgE0Hk) 🎵

🎵 *I don't know how you were diverted. You were perverted too. I don't know how you were inverted. No one alerted you.* 🎵

🎵 [***I look at you all, see the love there that's sleeping. While my guitar gently weeps.***](https://www.youtube.com/watch?v=YFDg-pgE0Hk) 🎵

🎵 [***Look at you all. Still my guitar gently weeps.***](https://www.youtube.com/watch?v=YFDg-pgE0Hk) 🎵

---

🎵 [***People are people, so why should it be? You and I should get along so awfully. People are people, so why should it be? You and I should get along so awfully.***](https://www.youtube.com/watch?v=MzGnX-MbYE4) 🎵

🎵 *So, we're different colors and we're different creeds, and different people have different needs. It's obvious you hate me, though, I've done nothing wrong. I've never even met you, so, what could I have done?* 🎵

🎵 *I can't understand what makes a man hate another man, help me understand.* 🎵

🎵 [***People are people, so why should it be? You and I should get along so awfully. People are people, so why should it be? You and I should get along so awfully.***](https://www.youtube.com/watch?v=MzGnX-MbYE4) 🎵

🎵 *(Help me understand, help me understand, help me understand)* 🎵

🎵 *Now you're punching, and you're kicking, and you're shouting at me. I'm relying on your common decency. So far, it hasn't surfaced, but I'm sure it exists. It just takes a while to travel from your head to your fist (head to your fist).* 🎵

🎵 *I can't understand what makes a man hate another man, help me understand.* 🎵

🎵 [***People are people, so why should it be? You and I should get along so awfully. People are people, so why should it be? You and I should get along so awfully.***](https://www.youtube.com/watch?v=MzGnX-MbYE4) 🎵

🎵 *I can't understand what makes a man hate another man, help me understand. I can't understand what makes a man hate another man, help me understand. I can't understand what makes a man hate another man, help me understand. I can't understand what makes a man hate another man, help me understand. I can't understand what makes a man hate another man, help me understand. I can't understand what makes a man hate another man, help me understand. I can't understand what makes a man hate another man ...* 🎵

---

🎵 *There's life underground.* 🎵

🎵 *I feel it all around, I feel it in my bones. My life is on the line when I'm away from home. When I step out the door, the jungle is alive. I do not trust my ears, I don't believe my eyes. I will not fall in love, I cannot risk the bet. 'Cause hearts are fragile toys, so easy to forget.* 🎵

🎵 *It's just another day, there's murder in the air. It drags me when I walk, I smell it everywhere. It's just another day where people cling to light, to drive away the fear that comes with every night.* 🎵

🎵 [***It's just another, it's just another day. It's just another day, it's just another day. It's just another, it's just another day. It's just another day, it's just another day.***](https://www.youtube.com/watch?v=nQusU0g9H-E) 🎵

🎵 *It's just another day when people wake from dreams, with voices in their ears that will not go away.* 🎵

🎵 *I had a dream last night, the world was set on fire. Anywhere I ran, there wasn't any water. The temperature increased, the sky was crimson red, the clouds turned into smoke, and everyone was dead.* 🎵

🎵 *But there's a smile on my face, for everyone. There's a golden coin that reflects the sun. There's a lonely place that's always cold. There's a place in the stars for when you get old.* 🎵

🎵 [***It's just another, it's just another day. It's just another, it's just another day.***](https://www.youtube.com/watch?v=nQusU0g9H-E) 🎵

🎵 *There's razors in my bed that come out late at night. They always disappear before the morning light. I'm dreaming again of life underground. It doesn't ever move, it doesn't make a sound.* 🎵

🎵 *And just when I think that things are in their place. The heavens are secure, the whole thing explodes in my face.* 🎵

🎵 [***It's just another, it's just another day. It's just another day, it's just another day. It's just another, it's just another day. It's just another day, it's just another day.***](https://www.youtube.com/watch?v=nQusU0g9H-E) 🎵

🎵 *There's a smile on my face, for everyone. There's a golden coin that reflects the sun. There's a lonely place that's always cold. There's a place in the stars for when you get old.* 🎵

🎵 [***It's just another, it's just another day. It's just another day, it's just another day. It's just another, it's just another day. It's just another day, it's just another day. ...***](https://www.youtube.com/watch?v=nQusU0g9H-E) 🎵

---

🎵 [***Ah, look at all the lonely people. Ah, look at all the lonely people.***](https://www.youtube.com/watch?v=HuS5NuXRb5Y) 🎵

🎵 *Eleanor Rigby, picks up the rice in the church where a wedding has been, lives in a dream. Waits at the window, wearing the face that she keeps in a jar by the door, who is it for?* 🎵

🎵 [***All the lonely people, where do they all come from? All the lonely people, where do they all belong?***](https://www.youtube.com/watch?v=HuS5NuXRb5Y) 🎵

🎵 *Father McKenzie, writing the words of a sermon that no one will hear, no one comes near. Look at him working, darning his socks in the night when there's nobody there, what does he care?* 🎵

🎵 [***All the lonely people, where do they all come from? All the lonely people, where do they all belong?***](https://www.youtube.com/watch?v=HuS5NuXRb5Y) 🎵

🎵 [***Ah, look at all the lonely people. Ah, look at all the lonely people.***](https://www.youtube.com/watch?v=HuS5NuXRb5Y) 🎵

🎵 *Eleanor Rigby, died in the church and was buried along with her name, nobody came. Father McKenzie, wiping the dirt from his hands as he walks from the grave, no one was saved.* 🎵

🎵 [***All the lonely people (Ah, look at all the lonely people). Where do they all come from? All the lonely people (Ah, look at all the lonely people). Where do they all belong?***](https://www.youtube.com/watch?v=HuS5NuXRb5Y) 🎵

---

🎵 *I'm gonna take you to a place far from here. No one will see us, watch the pain as it disappears. No time for anger, no time for despair. Won't you come with me? There's room for us there.* 🎵

🎵 *This innocent beauty, my own words can't describe. This rebirth to purity, brings a sullen tear right to your eyes. No time for anger, no time for despair. Please, let me take you, 'cause I'm already there.* 🎵

🎵 [***I'm so alone, my head's my home, I return to serenity.***](https://www.youtube.com/watch?v=F3DUxYUR5IA) 🎵

🎵 *A rhyme without reason is why children cry/die. They see through the system that's breeding them just so they die. So please let me take you, and I'll show you the truth. Inside my reality, we shared in our youth.* 🎵

🎵 [***I'm so alone, my head's my home, and I feel so alone (So alone). At last (At last) I return to serenity.***](https://www.youtube.com/watch?v=F3DUxYUR5IA) 🎵

🎵 *Now that I've taken you to a place far from here. I really must go back, close your eyes, and we'll disappear. Won't you come with me, salvation we share. Inside of my head now, there's room for us there.* 🎵

🎵 [***I'm so alone, my head's my home, and I feel so alone (So alone). At last (At last) I return to serenity.***](https://www.youtube.com/watch?v=F3DUxYUR5IA) 🎵

---

🎵 *There is no political solution to our troubled evolution. Have no faith in constitution. There is no bloody revolution.* 🎵

🎵 [***We are spirits in the material world. We are spirits in the material world. We are spirits in the material world. We are spirits in the material world.***](https://www.youtube.com/watch?v=BHOevX4DlGk) 🎵

🎵 *Our so-called leaders speak. With words, they try to jail ya'. They subjugate the meek. But it's the rhetoric of failure.* 🎵

🎵 [***We are spirits in the material world. We are spirits in the material world. We are spirits in the material world. We are spirits in the material world.***](https://www.youtube.com/watch?v=BHOevX4DlGk) 🎵

🎵 *Where does the answer lie? Living from day to day. If it's something we can't buy. There must be another way.* 🎵

🎵 [***We are spirits in the material world. We are spirits in the material world. We are spirits in the material world. We are spirits in the material world. We are spirits in the material world. We are spirits in the material world. We are spirits in the material world. We are spirits in the material world.***](https://www.youtube.com/watch?v=BHOevX4DlGk) 🎵

---

🎵 [***I'm a melancholy man, that's what I am. All the world surrounds me and my feet are on the ground. I'm a very lonely man, doing what I can. All the world astounds me and I think I understand. That we're going to keep growing, wait and see.***](https://www.youtube.com/watch?v=CxMXDwP7lqM) 🎵

🎵 *When all the stars are falling down into the sea and on the ground, and angry voices carry on the wind. A beam of light will fill your head and you'll remember what's been said by all the good men this world's ever known. Another man is what you'll see, who looks like you and looks like me. And yet, somehow he will not feel the same. His life caught up in misery, he doesn't think like you and me. 'Cause he can't see what you and I can see.* 🎵

🎵 *When all the stars are falling down into the sea and on the ground, and angry voices carry on the wind. A beam of light will fill your head and you'll remember what's been said by all the good men this world's ever known. Another man is what you'll see, who looks like you and looks like me. And yet, somehow he will not feel the same. His life caught up in misery (Meloncholy man), he doesn't think like you and me (Very lonely man). 'Cause he can't see what you and I can see.* 🎵

🎵 [***I'm a melancholy man, that's what I am. All the world surrounds me and my feet are on the ground. I'm a very lonely man, doing what I can. All the world astounds me and I think I understand. That we're going to keep growing, wait and see.***](https://www.youtube.com/watch?v=CxMXDwP7lqM) 🎵

🎵 [***I'm a melancholy man, that's what I am. All the world surrounds me. I'm a very lonely man, doing what I can. All the world astounds me and I think I understand.***](https://www.youtube.com/watch?v=CxMXDwP7lqM) 🎵

---

🎵 [***Time is money and money's time. We wasted every second dime on diets, lawyers, shrinks, and apps, and flags, and plastic surgery. Now Willy Wonka, Major Tom, Ali, and Leia have moved on. Signal the final curtain call in all its atomic pageantry.***](https://www.youtube.com/watch?v=UkHSmDxX1t4) 🎵

🎵 *Bravissimo, hip, hip, hooray for this fireworks display. Mind and body blown away. What a radiant crescendo. Ticker-tape parade. Our hair and skin like Marilyn Monroe in an afterwind.* 🎵

🎵 [***Time is money and money's time. We wasted every second dime on politicians, fancy water, and guns, and plastic surgery. Like our Prince and Reenie's mom. All the dolphins have moved on, signaling the final curtain call in all its atomic pageantry.***](https://www.youtube.com/watch?v=UkHSmDxX1t4) 🎵

🎵 *Bravissimo, hip, hip, hooray. What a glorious display. Melt our joyous hearts away under the mushroom cloud confetti. Hip, hip, hooray for this fireworks display. Mind and body blown away. What a radiant crescendo. Hip, hip, hooray. Hip, hip, hooray. Ticker-tape parade. Our hair and skin like Marilyn Monroe in an afterwind.* 🎵

🎵 [***Time is money and money's time. We wasted every second dime on diets, lawyers, shrinks, and apps, and flags, and plastic surgery. Now Willy Wonka, Major Tom, Ali, and Leia have moved on. Signal the final curtain call in all its atomic pageantry.***](https://www.youtube.com/watch?v=UkHSmDxX1t4) 🎵

[**Download (webm)**](https://www.mediafire.com/file/evgx0vuog0lycvd/A_Perfect_Circle_-_So_Long%252C_And_Thanks_For_All_The_Fish_%2528Official_Video%2529.webm)

---

🎵 *Ska-badabadabadoo-belidabbelydabbladabbladabblabab-belibabbelibabbelibabbelabbelo-doobelidoo. I'm the Scatman. Ski-bi-dibby-dib yo-da-dub-dub, yo-da-dub-dub. Ski-bi-dibby-dib yo-da-dub-dub, yo-da-dub-dub. I'm the Scatman. Ski-bi-dibby-dib yo-da-dub-dub, yo-da-dub-dub. Ski-bi-dibby-dib yo-da-dub-dub, yo-da-dub-dub. Ba-da-ba-da-ba-be bop-bop-bodda-bope, bop-ba-bodda-bope. Be-bop-ba-bodda-bope, bop-ba-bodda. Ba-da-ba-da-ba-be bop-ba-bodda-bope, bop-ba-bodda-bope. Be-bop-ba-bodda-bope, bop-ba-bodda-bope. Ska-badabadabadoo-belidabbelydabbladabbladabblabab-belibabbelibabbelibabbelabbelo-doobelidoo.* 🎵

🎵 [***Everybody stutters one way or the other, so check out my message to you. As a matter of fact, I don't let nothing hold you back. If the Scatman can do it, so can you. Everybody's saying that the Scatman stutters, but doesn't ever stutter when he sings. But what you don't know, I'm gonna tell you right now, that the stutter and the scat is the same thing, yo. I'm the Scatman. Where's the Scatman? I'm the Scatman.***](https://www.youtube.com/watch?v=Hy8kmNEo1i8) 🎵

🎵 [***Why should we be pleasing any politician heathens, who would try to change the seasons if they could? The state of the condition insults my intuition, and it only makes me crazy, and a heart like wood. Everybody stutters one way or the other, so check out my message to you. As a matter of fact, I'm letting nothing hold you back. If the Scatman can do it, brother, so can you. I'm the Scatman.***](https://www.youtube.com/watch?v=Hy8kmNEo1i8) 🎵

🎵 *Ba-da-ba-da-ba-be bop-bop-bodda-bope, bop-ba-bodda-bope. Be-bop-ba-bodda-bope, bop-ba-bodda. Ba-da-ba-da-ba-be bop-ba-bodda-bope, bop-ba-bodda-bope. Be-bop-ba-bodda-bope, bop-ba-bodda-bope. Ski-bi-dibby-dib yo-da-dub-dub, yo-da-dub-dub. Ski-bi-dibby-dib yo-da-dub-dub, yo-da-dub-dub. Ski-bi-dibby-dib yo-da-dub-dub, yo-da-dub-dub. Ski-bi-dibby-dib yo-da-dub-dub, yo-da-dub-dub.* 🎵

🎵 [***Everybody stutters one way or the other, so check out my message to you. As a matter of fact, I don't let nothing hold you back. If the Scatman can do it, so can you. I hear y'all ask 'bout the meaning of scat. Well, I'm the professor and all I can tell you is while you're still sleeping, the saints are still weeping 'cause things you called dead haven't yet had the chance to be born. I'm the Scatman.***](https://www.youtube.com/watch?v=Hy8kmNEo1i8) 🎵

🎵 *Ba-da-ba-da-ba-be bop-bop-bodda-bope, bop-ba-bodda-bope. Be-bop-ba-bodda-bope, bop-ba-bodda. Ba-da-ba-da-ba-be bop-ba-bodda-bope, bop-ba-bodda-bope. Be-bop-ba-bodda-bope, bop-ba-bodda-bope. Ski-bi-dibby-dib yo-da-dub-dub, yo-da-dub-dub. Ski-bi-dibby-dib yo-da-dub-dub, yo-da-dub-dub. Ski-bi-dibby-dib yo-da-dub-dub, yo-da-dub-dub. Ski-bi-dibby-dib yo-da-dub-dub, yo-da-dub-dub. Yeah, I'm the Scatman. Dong dong dong, ding ding dong dong. Wo-go-ez-ze-ze-za-de-ya-de-za-de-ya-ze-ze-zee. Dong-ding-dong-dong-dong, do-dong-ding. Where's the Scatman? Ska-badabadabadoo-belidabbelydabbladabbladabblabab-belibabbelibabbelibabbelabbelo-doobelidoo. I'm the Scatman, repeat after me. It's a scoobie-oobie-doobie, scoobie-doobie melody. I'm the Scatman, sing along with me. It's a scoobie-oobie-doobie, scoobie-doobie melody. Yeah, I'm the Scatman (I'm the Scatman). Bop-ba-bodda-bope, be-bop-ba-bodda-bope, bop-ba-bodda. I'm the Scatman (I'm the Scatman). Bop-ba-bodda-bope, be-bop-ba-bodda-bope. Ska-badabadabadoo-belidabbelydabbladabbladabblabab-belibabbelibabbelibabbelabbelo-doobelidoo. Be-bop-ba-bodda-bope, bop-ba-bodda bope. Be-bop-ba-bodda-bope, bop-ba-bodda. Ba-da-ba-da-ba-be bop-ba-bodda-bope. Yeah, I'm the Scatman, sing along with me. Bop-ba-bodda. It's a scoobie-oobie-doobie, scoobie-doobie melody.* 🎵

---

🎵 *Sting-doogadungdadingdoodung doodungdadingdadingduhduhdungdughdugh Scatman's World. Babobeh-bopbopahdop-bababa-ba-babedi babobeh-bopbopahdop-bababa-bee-babedi babobeh-bopbopahdop-babobeh-bopbopahdop babobeh-bopbopahdop-bababa-ba-babedi.* 🎵

🎵 [***I'm calling out from Scatland. I'm calling out from Scatman's World. If you wanna break free, you better listen to me. You've got to learn how to see in your fantasy. I'm calling out from Scatland. I'm calling out from Scatman's World. If you wanna break free, you better listen to me. You've got to learn how to see in your fantasy.***](https://www.youtube.com/watch?v=02vDkMEdIkY) 🎵

🎵 [***Everybody's talkin' something very shockin' just to keep on blockin' what they're feeling inside. But listen to me, brother, you just keep on walkin', 'cause you and me and sister ain't got nothing to hide. Scatman, fat man, black and white and brown man. Tell me 'bout the color of your soul. If part of your solution isn't ending the pollution, then I don't want to hear your stories told. I want to welcome you to Scatman's World.***](https://www.youtube.com/watch?v=02vDkMEdIkY) 🎵

🎵 *Babobeh-bopbopahdop-bababa-ba-babedi babobeh-bopbopahdop-bababa-bee-babedi babobeh-bopbopahdop-babobeh-bopbopahdop babobeh-bopbopahdop-bababa-ba-babedi.* 🎵

🎵 [***I'm calling out from Scatland. I'm calling out from Scatman's World. If you wanna break free, you better listen to me. You've got to learn how to see in your fantasy.***](https://www.youtube.com/watch?v=02vDkMEdIkY) 🎵

🎵 [***Everybody's born to compete as he chooses, but how can someone win if winning means that someone loses? I sit and see and wonder what it's like to be in touch. No wonder all my brothers and my sisters need a crutch. I want to be a human being, not a human doing. I couldn't keep that pace up if I tried. The source of my intention really isn't crime prevention. My intention is prevention of the lie, yeah, welcome to the Scatman's World.***](https://www.youtube.com/watch?v=02vDkMEdIkY) 🎵

🎵 *Babobeh-bopbopahdop-bababa-ba-babedi babobeh-bopbopahdop-bababa-bee-babedi babobeh-bopbopahdop-babobeh-bopbopahdop babobeh-bopbopahdop-bababa-ba-babedi.* 🎵

🎵 [***I'm calling out from Scatland. I'm calling out from Scatman's World. If you wanna break free, you better listen to me. You've got to learn how to see in your fantasy.***](https://www.youtube.com/watch?v=02vDkMEdIkY) 🎵

🎵 *Ski-bi-debeh-dopabodidingdickidi dingdingdingdi-ding-bopdopidi bi-di-di-di-ding-bopdopidi dalalalalalalalalala-bippidibi bi-twee-dada-twee-dada-deedle-dada-dadadada dadadadadadadadadadada-de bopadopa-dobadopa-dobadopa-da. Hey-yeah! Babobeh-bopbopahdop-bababa-ba-babedi babobeh-bopbopahdop-bababa-bee-babedi babobeh-bopbopahdop-babobeh-bopbopahdop babobeh-bopbopahdop-bababa-ba-babedi.* 🎵

🎵 [***I'm calling out from Scatland. I'm calling out from Scatman's World. If you wanna break free, you better listen to me. You've got to learn how to see in your fantasy. Skibi-debida-bide-bidep. Listen to me! I'm calling out from Scatland (bopbopahdop-bababa-ba-babedi). I'm calling out from Scatman's World (bopbopahdop-bababa-bee-babedi). If you wanna break free, you better listen to me. You've got to learn how to see in your fantasy.***](https://www.youtube.com/watch?v=02vDkMEdIkY) 🎵

🎵 *Di-ding-doogadungdadingdoodung doodungdadingdadingduhduhdungdughdugh (Bopbopahdop-bababa-ba-babedi). Di-ding-doogadungdadingdoodung doodungdadingdadingduhduhdungdughdugh (bopbopahdop-bababa-bee-babedi). Di-ding-doogadungdadingdoodung doodungdadingdadingduhduhdungdughdugh (Bopbopahdop-bababa-ba-babedi). Di-ding-doogadungdadingdoodung doodungdadingdadingduhduhdungdughdugh (Bopbopahdop-bababa-bee-babedi).* 🎵

[**Download (webm)**](https://www.mediafire.com/file/ey4eteyr7olreq1/Scatmans+World+(Official+Video).webm)

---

🎵 *Now if you're feeling kinda low about the dues you've been paying, future's coming much too slow. And you wanna run, but somehow you just keep on stayin'. Can't decide on which way to go. Yeah, yeah, yeah.* 🎵

🎵 [***I understand about indecision, but I don't care if I get behind. People livin' in competition, all I want is to have my peace of mind.***](https://www.youtube.com/watch?v=edwk-8KJ1Js) 🎵

🎵 *Now you're climbing to the top of the company ladder, hope it doesn't take too long. Can't you see there'll come a day when it won't matter, come a day when you'll be gone. Ohh, ohh.* 🎵

🎵 [***I understand about indecision, but I don't care if I get behind. People livin' in competition, all I want is to have my peace of mind.***](https://www.youtube.com/watch?v=edwk-8KJ1Js) 🎵

🎵 *Take a look ahead, take a look ahead, yeah, yeah, yeah, yeah.* 🎵

🎵 *Now everybody's got advice they just keep on givin', doesn't mean too much to me. Lots of people out to make believe they're livin', can't decide who they should be. Oh, oh.* 🎵

🎵 [***I understand about indecision, but I don't care if I get behind. People livin' in competition, all I want is to have my peace of mind.***](https://www.youtube.com/watch?v=edwk-8KJ1Js) 🎵

🎵 *Take a look ahead, take a look ahead, look ahead!* 🎵

---

🎵 *Now we've come to the age where the splendor fades, and we look behind drooping facades. The glossy front's just fake, the firm base breaks, as this doomed world slowly decays.* 🎵

🎵 *Illusions fly high, nothing we don't try to build up fantasies we can believe. The dance on dragon's jaws, in reach of its claws, destroys the little we could retrieve.* 🎵

🎵 [***We have resigned to our fate, afraid that our time is up now. Though it's not quite too late, if we take to action now.***](https://www.youtube.com/watch?v=AdUianylWuo) 🎵

🎵 *We see no future, just today's endured, and tomorrow is smoke in the wind. We dance, sing, and play, 'cause we feel the strain of living at the end of our time.* 🎵

🎵 [***We have resigned to our fate, afraid that our time is up now. Though it's not quite too late, if we take to action now.***](https://www.youtube.com/watch?v=AdUianylWuo) 🎵

🎵 *Our legacy fades and melts away, 'cause tomorrow may not ever be. So we dance and sing, try to bear the thought of approaching the end of our time.* 🎵

🎵 *Our legacy fades and melts away, 'cause tomorrow may not ever be. So we dance and sing, play the game's return, 'cause tomorrow may not ever be.* 🎵

🎵 *Our legacy fades and melts away, 'cause tomorrow may not ever be. So we dance and sing, play the game's return, 'cause tomorrow may not ever be.* 🎵

🎵 *Our legacy fades and melts away, 'cause tomorrow may not ever be. So we dance and sing, play the game's return, 'cause tomorrow may not ever be.* 🎵

---

🎵 *On a cold winter morning, in the time before the light. In flames of death's eternal reign, we ride towards the fight. When the darkness has fallen down, and the times are tough alright. The sound of evil laughter falls around the world tonight. Fighting hard, fighting on for the steel, through the wastelands evermore. The scattered souls will feel the hell, bodies wasted on the shores. On the blackest plains in Hell's domain, we watch them as they go. In fire and pain, now once again, we know!* 🎵

🎵 *So now we fly ever free, we're free before the thunderstorm. On towards the wilderness, our quest carries on. Far beyond the sundown, far beyond the moonlight. Deep inside our hearts and all our souls!* 🎵

🎵 [***So far away, we wait for the day. For the lives all so wasted and gone. We feel the pain of a lifetime lost in a thousand days. Through the fire and the flames, we carry on!***](https://www.youtube.com/watch?v=DZyYapMZSec) 🎵

🎵 *As the red day is dawning, and the lightning cracks the sky. They'll raise their hands to the heavens above with resentment in their eyes. Running back through the mid-morning light, there's a burning in my heart. We're banished from a time in a fallen land to a life beyond the stars. In your darkest dreams, see to believe our destiny is time. And endlessly we'll all be free tonight!* 🎵

🎵 *And on the wings of a dream, so far beyond reality. All alone in desperation, now the time has gone. Lost inside, you'll never find, lost within my own mind. Day after day, this misery must go on!* 🎵

🎵 [***So far away, we wait for the day. For the lives all so wasted and gone. We feel the pain of a lifetime lost in a thousand days. Through the fire and the flames, we carry on!***](https://www.youtube.com/watch?v=DZyYapMZSec) 🎵

🎵 *Now here we stand with their blood on our hands (on our hands). We fought so hard, now can we understand? (We understand). I'll break the seal of this curse if I possibly can, for freedom of every man!* 🎵

🎵 [***So far away, we wait for the day. For the lives all so wasted and gone. We feel the pain of a lifetime lost in a thousand days. Through the fire and the flames, we carry on!***](https://www.youtube.com/watch?v=DZyYapMZSec) 🎵

</details>

---

*"Religion is regarded by the common people as true, by the wise as false, and by rulers as useful."* ― **Seneca**

---

*"A pessimist is a well-informed optimist."* ― **Mark Twain**

---

*"The power of accurate observation is commonly called cynicism by those who don't have it."* ― **~~George~~ Bernard Shaw**

---

*"If you scratch a cynic, you'll find a disappointed idealist."* ― **George Carlin**

*"A person of good intelligence and of sensitivity cannot exist in this society very long without having some anger about the inequality—and it's not just a bleeding-heart, knee-jerk, liberal kind of a thing—it is just a normal human reaction to a nonsensical set of values where we have cinnamon flavored dental floss and there are people sleeping in the street."* ― **George Carlin**

*"Consumption. It's the new national pastime. Fuck baseball, it's consumption. The only true lasting American value that's left; buying things, buying things. People spending money they dont have, on things they dont need. Money they dont have, on things they dont need. So they can max out their credit cards and spend the rest of their lives paying 18 percent interest on something that cost 12.50. And they didn't like it when they got it home anyway. Not too bright, folks. Not too fucking bright. But if you talk to one of them about this, if you isolate one of them, you sit down with them, rationally you talk to them, about the low IQ's and the dumb behavior and the bad decisions. Right away they start talking about education. That's the big answer to everything; education. They say 'We need more money for education. We need more books, more teachers, more classrooms, more schools. We need more testing for the kids'. You say to them, 'Well, you know, we have tried all of that, and the kids still can't pass the tests'. They say, 'ah, don't you worry about that, we're going to lower the passing grades'. And that's what they do in lots of these schools now. They lower the passing grades so more kids can pass. More kids pass, the school looks good, everybody's happy, the IQ of the country slips another two or three points, and pretty soon all you'll need to get into college is a fucking pencil. Got a pencil? Get the fuck in there, it's physics. Then everyone wonders, why 17 other countries graduate more scientists than we do; education. Politicians know that word, they use it on you. Politicians have traditionally hidden behind three things; the flag, the Bible, and children. 'No child left behind, no child left behind'. Oh, really? Well, it wasn't long ago, you were talking about giving kids a head start. Head start, left behind. Someone's losing fucking ground here. But there's a reason for this. There's a reason that education sucks; and it's the same reason that it will never ever, ever be fixed. It's never going to get any better. Don't look for it. Be happy with what you got. Because the owners of this country don't want that. I'm talking about the real owners now. The real owners. The big, wealthy business interests that control things and make all the important decisions. Forget the politicians. The politicians are put there to give you the idea that you have freedom of choice. You don't. You have no choice. You have owners. They own you. They own everything. They own all the important land. They own and control the corporations. They've long since bought and paid for the Senate, the Congress, the state houses, and city halls. They got the judges in their back pocket. And they own all the big media companies; so they control just about all of the news and information you get to hear. They got you by the balls. They spend billions of dollars every year lobbying, lobbying to get what they want. Well, we know what they want. They want more for themselves, and less for everybody else. But I'll tell you what they don't want. They don't want a population of citizens capable of critical thinking. They don't want well-informed, well-educated people, capable of critical thinking. They're not interested in that. That doesn't help them. That's against their interest. That's right. They don't want people who are smart enough to sit around the kitchet table to figure out how badly they're getting fucked by a system that threw them overboard 30 fucking years ago. They don't want that. You know what they want? They want obedient workers. Obedient workers. People who are just smart enough to run the machines and do the paperwork, and just dumb enough to passively accept all these increasingly shittier jobs, with the lower pay, the longer hours, the reduced benefits. The end of overtime and the vanishing pension that disappears the minute you go to collect it. And now, they're coming for your Social Security money. They want your fucking retirement money. They want it back, so they can give it to their criminal friends on Wall Street. And you know something, they'll get it. They'll get it all from you, sooner or later, because they own this fucking place. It's a big club, and you ain't in it. You and I are not in the big club. By the way, it's the same big club they use to beat you over the head with all day long when they tell you what to believe. All day long beating you over the head, and their media telling you what to believe, what to think and what to buy. The table is tilted, folks. The game is rigged. And nobody seems to notice. Nobody seems to care. Good, honest, hard-working people; white collar, blue collar, it doesn't matter what color shirt you have on. Good, honest, hard-working people continue, these are people of modest means, continue to elect these rich cocksuckers who don't give a fuck about them. They don't give a fuck about you. They don't give a fuck about you. They don't care about you. At all. At all. At all. Yeah. You know. And nobody seems to notice. Nobody seems to care. That's what the owners count on. The fact that Americans will probably remain willfully ignorant of the big red, white and blue dick that is being jammed up their assholes every day. Because the owners of this country know the truth; it's called 'the American dream' because you have to be asleep to believe it."* ― **George Carlin, [Life is Worth Losing (2005)](https://www.youtube.com/watch?v=KLODGhEyLvk)**

---

*"The optimist proclaims that we live in the best of all possible worlds; and the pessimist fears this is true."* ― **James Branch Cabell, The Silver Stallion**

*"The wickedness and the foolishness of no man can avail against the foolishness and the fond optimism of man-kind."* ― **James Branch Cabell, The Silver Stallion**

---

*"Life is not an easy matter... You cannot live through it without falling into prostration and cynicism unless you have before you a great idea which raises you above personal misery, above weakness, above all kinds of perfidy and baseness."* ― **Leon Trotsky, Trotsky's Diary in Exile, 1935**

*"As long as I breathe I hope. As long as I breathe I shall fight for the future, that radiant future, in which man, strong and beautiful, will become master of the drifting stream of his history and will direct it towards the boundless horizons of beauty, joy and happiness!"* ― **Leon Trotsky, The Prophet Armed: Trotsky, 1879-1921**

*"The depth and strength of a human character are defined by its moral reserves. People reveal themselves completely only when they are thrown out of the customary conditions of their life, for only then do they have to fall back on their reserves."* ― **Leon Trotsky, Portraits, Political and Personal**

*"As long as human labor power, and, consequently, life itself, remain articles of sale and purchase, of exploitation and robbery, the principle of the "sacredness of human life" remains a shameful lie, uttered with the object of keeping the oppressed slaves in their chains."* ― **Leon Trotsky, [Terrorism and Communism: A Reply to Karl Kautsky](https://www.marxists.org/archive/trotsky/1920/terrcomm/) ([Chapter 4: Terrorism](https://www.marxists.org/archive/trotsky/1920/terrcomm/ch04.htm))**

*"As a general rule, man strives to avoid labor. Love for work is not at all an inborn characteristic: it is created by economic pressure and social education. One may even say that man is a fairly lazy animal. It is on this quality, in reality, that is founded to a considerable extent all human progress; because if man did not strive to expend his energy economically, did not seek to receive the largest possible quantity of products in return for a small quantity of energy, there would have been no technical development or social culture. It would appear, then, from this point of view that human laziness is a progressive force, Old Antonio Labriola, the Italian Marxist, even used to picture the man of the future as a 'happy and lazy genius.'"* ― **Leon Trotsky, [Dictatorship vs. Democracy](https://www.gutenberg.org/files/38982/38982-h/38982-h.htm)**

*"The basis of bureaucratic rule is the poverty of society in objects of consumption, with the resulting struggle of each against all. When there is enough goods in a store, the purchasers can come whenever they want to. When there is little goods, the purchasers are compelled to stand in line. When the lines are very long, it is necessary to appoint a policeman to keep order. Such is the starting point of the power of the Soviet bureaucracy. It "knows" who is to get something and who has to wait."* ― **Leon Trotsky, [The Revolution Betrayed](https://www.marxists.org/archive/trotsky/1936/revbet/) ([Chapter 5: The Soviet Thermidor](https://www.marxists.org/archive/trotsky/1936/revbet/ch05.htm))**

*"Fascism is nothing else but capitalist reaction ..."* ― **Leon Trotsky, [What Next?](https://www.marxists.org/archive/trotsky/germany/1932-ger/index.htm) ([Chapter 5: An Historical Review of the ... United Front](https://www.marxists.org/archive/trotsky/germany/1932-ger/next02.htm#s5))**

*"The bourgeoisie, which far surpasses the proletariat in the completeness and irreconcilibility of its class consciousness, is vitally interested in imposing its moral philosophy upon the exploited masses. It is exactly for this purpose that the concrete norms of the bourgeois catechism are concealed under moral abstractions...The appeal to abstract norms is not a disinterested philosophic mistake but a necessary element in the mechanics of class deception."* ― **Leon Trotsky**

*"For us, the tasks of education in socialism were closely integrated with those of fighting. Ideas that enter the mind under fire remain there securely and forever."* ― **Leon Trotsky, [My Life: An Attempt at an Autobiography](https://www.marxists.org/archive/trotsky/1930/mylife/1930-lif.pdf)**

*"DURING AN EPOCH OF triumphant reaction, Messrs. democrats, social-democrats, anarchists, and other representatives of the 'left' camp begin to exude double their usual amount of moral effluvia, similar to persons who perspire doubly in fear. Paraphrasing the Ten Commandments or the Sermon on the Mount, these moralists address themselves not so much to triumphant reaction as to those revolutionists suffering under its persecution, who with their 'excesses' and 'amoral' principles 'provoke' reaction and give it moral justification. Moreover they prescribe a simple but certain means of avoiding reaction: it is necessary only to strive and morally to regenerate oneself. Free samples of moral perfection for those desirous are furnished by all the interested editorial offices. The class basis of this false and pompous sermon is the intellectual petty bourgeoisie. The political basis – their impotence and confusion in the face of approaching reaction. Psychological basis – their effort at overcoming the feeling of their own inferiority through masquerading in the beard of a prophet. A moralizing Philistine’s favorite method is the lumping of reaction’s conduct with that of revolution. He achieves success in this device through recourse to formal analogies. To him czarism and Bolshevism are twins. Twins are likewise discovered in fascism and communism. An inventory is compiled of the common features in Catholicism – or more specifically, Jesuitism – and Bolshevism. Hitler and Mussolini, utilizing from their side exactly the same method, disclose that liberalism, democracy, and Bolshevism represent merely different manifestations of one and the same evil. The conception that Stalinism and Trotskyism are 'essentially' one and the same now enjoys the joint approval of liberals, democrats, devout Catholics, idealists, pragmatists, and anarchists. If the Stalinists are unable to adhere to this 'People’s Front', then it is only because they are accidentally occupied with the extermination of Trotskyists. The fundamental feature of these approchements and similitudes lies in their completely ignoring the material foundation of the various currents, that is, their class nature and by that token their objective historical role. Instead they evaluate and classify different currents according to some external and secondary manifestation, most often according to their relation to one or another abstract principle which for the given classifier has a special professional value. Thus to the Roman pope Freemasons and Darwinists, Marxists and anarchists are twins because all of them sacrilegiously deny the immaculate conception. To Hitler, liberalism and Marxism are twins because they ignore 'blood and honor'. To a democrat, fascism and Bolshevism are twins because they do not bow before universal suffrage. And so forth. Undoubtedly the currents grouped above have certain common features. But the gist of the matter lies in the fact that the evolution of mankind exhausts itself neither by universal suffrage, not by 'blood and honor,' nor by the dogma of the immaculate conception. The historical process signifies primarily the class struggle; moreover, different classes in the name of different aims may in certain instances utilize similar means. Essentially it cannot be otherwise. Armies in combat are always more or less symmetrical; were there nothing in common in their methods of struggle they could not inflict blows upon each other. If an ignorant peasant or shopkeeper, understanding neither the origin nor the sense of the struggle between the proletariat and the bourgeoisie, discovers himself between the two fires, he will consider both belligerent camps with equal hatred. And who are all these democratic moralists? Ideologists of intermediary layers who have fallen, or are in fear of falling between the two fires. The chief traits of the prophets of this type are alienism to great historical movements, a hardened conservative mentality, smug narrowness, and a most primitive political cowardice. More than anything moralists wish that history should leave them in peace with their petty books, little magazines, subscribers, common sense, and moral copy books. But history does not leave them in peace. It cuffs them now from the left, now from the right. Clearly – revolution and reaction, Czarism and Bolshevism, communism and fascism, Stalinism and Trotskyism – are all twins. Whoever doubts this may feel the symmetrical skull bumps upon both the right and left sides of these very moralists."* ― **Leon Trotsky, [Their Morals and Ours](https://www.marxists.org/archive/trotsky/1938/morals/morals.htm)**

*"The United States is not only the strongest, but also the most terrified country."* ― **Leon Trotsky, [My Life: An Attempt at an Autobiography](https://www.marxists.org/archive/trotsky/1930/mylife/1930-lif.pdf)**

*"Religions are illogical primitive ignorance. There is nothing as ridiculous and tragic as a religious government."* ― **Leon Trotsky**

*"Tell me anyway—Maybe I can find the truth by comparing the lies."* ― **Leon Trotsky**

---

*"As a child I felt myself to be alone, and I am still, because I know things and must hint at things which others apparently know nothing of, and for the most part do not want to know. Loneliness does not come from having no people about one, but from being unable to communicate the things that seem important to oneself, or from holding certain views which others find inadmissible."* ― **Carl Jung, Memories, Dreams, Reflections**

*"Do not compare, do not measure. No other way is like yours. All other ways deceive and tempt you. You must fulfill the way that is in you."* ― **Carl Jung**

*"The deep critical thinker has become the misfit of the world, this is not a coincidence. To maintain order and control you must isolate the intellectual, the sage, the philosopher, the savant before their ideas awaken people."* ― **Carl Jung**

---

*"Pain and suffering are always inevitable for a large intelligence and a deep heart. The really great men must, I think, have great sadness on earth."* ― **Fyodor Dostoevsky**

*"It is better to be unhappy and know the worst, than to be happy in a fool's paradise."* ― **Fyodor Dostoevsky**

*"Humanity can live without science, it can live without bread, but it cannot live without beauty. Without beauty, there would be nothing left to do in this life. Here the secret lies. Here lies the entire story."* ― **Fyodor Dostoevsky**

*"The mystery of human existence lies not in just staying alive, but in finding something to live for."* ― **Fyodor Dostoevsky**

*"The world says: 'You have needs—satisfy them. You have as much right as the rich and the mighty. Don't hesitate to satisfy your needs; indeed, expand your needs and demand more.' This is the worldly doctrine of today. And they believe that this is freedom. The result for the rich is isolation and suicide, for the poor, envy and murder."* ― **Fyodor Dostoevsky**

*"The man who lies to himself can be more easily offended than anyone else. You know it is sometimes very pleasant to take offense, isn't it? A man may know that nobody has insulted him, but that he has invented the insult for himself, has lied and exaggerated to make it picturesque, has caught at a word and made a mountain out of a molehill—he knows that himself, yet he will be the first to take offense, and will revel in his resentment till he feels great pleasure in it."* ― **Fyodor Dostoevsky**

---

*"What is a poet? An unhappy man who hides deep anguish in his heart, but whose lips are so formed that when the sigh and cry pass through them, it sounds like lovely music... And people flock around the poet and say, 'sing again soon'—that is, 'may new sufferings torment your soul, but your lips be fashioned as before, for the cry would only frighten us, but the music, that is blissful.'"* ― **Søren Kierkegaard**

*"Marry, and you will regret it; don't marry, you will also regret it; marry or don't marry, you will regret it either way. Laugh at the world's foolishness, you will regret it; weep over it, you will regret that too; laugh at the world's foolishness or weep over it, you will regret both. Believe a woman, you will regret it; believe her not, you will also regret it. Hang yourself, you will regret it; do not hang yourself, and you will regret that too; hang yourself or don't hang yourself, you'll regret it either way; whether you hang yourself or do not hang yourself, you will regret both. This, gentlemen, is the essence of all philosophy."* ― **Søren Kierkegaard**

*"One must first learn to know himself before knowing anything else. Not until a man has inwardly understood himself and then sees the course he is to take does his life gain peace and meaning; only then is he free of that irksome, sinister traveling companion—that irony of life, which manifests itself in the sphere of knowledge and invites true knowing to begin with a not-knowing. But in the waters of morality it is especially at home to those who still have not entered the tradewinds of virtue. Here it tumbles a person about in a horrible way, for a time lets him feel happy and content in his resolve to go ahead along the right path, then hurls him into the abyss of despair. Often it lulls a man to sleep with the thought, 'After all, things cannot be otherwise,' only to awaken him suddenly to a rigorous interrogation. Frequently it seems to let a veil of forgetfulness fall over the past, only to make every single trifle appear in a strong light again. When he struggles along the right path, rejoicing in having overcome temptation's power, there may come at almost the same time, right on the heels of perfect victory, an apparently insignificant external circumstance which pushes him down, like Sisyphus, from the height of the crag. Often when a person has concentrated on something, a minor external circumstance arises which destroys everything. (As in the case of a man who, weary of life, is about to throw himself into the Thames and at the crucial moment is halted by the sting of a mosquito.) Frequently a person feels his very best when the illness is the worst, as in tuberculosis. In vain he tries to resist it but he has not sufficient strength, and it is no help to him that he has gone through the same thing many times; the kind of practice acquired in this way does not apply here."* ― **Søren Kierkegaard**

*"A fire broke out backstage in a theatre. The clown came out to warn the public; they thought it was a joke and applauded. He repeated it; the acclaim was even greater. I think that's just how the world will come to an end: to general applause from wits who believe it's a joke."* ― **Søren Kierkegaard**

---

*"Whoever fights with monsters should see to it that he does not become one himself. And when you stare for a long time into an abyss, the abyss stares back into you."* ― **Friedrich Nietzsche, Beyond Good and Evil**

*"Against that positivism which stops before phenomena, saying "there are only facts," I should say: no, it is precisely facts that do not exist, only interpretations.... (There are no facts, only interpretations.)"* ― **Friedrich Nietzsche, The Portable Nietzsche**

*"Artists are not the men of great passion, whatever they may try to tell us and themselves. And that for two reasons: they have no shame before themselves (they observe themselves while they live; they lie in wait for themselves, they are too curious), and they also have no shame before great passion (they exploit it artistically). Secondly, their vampire—their talent—generally begrudges them any such squandering of energy as is involved in passion. With a talent, one is also the victim of that talent: one lives under the vampirism of one’s talent. One is not finished with one’s passion because one represents it: rather, one is finished with it when one represents it."* ― **Friedrich Nietzsche, The Portable Nietzsche**

---

*"Had I so interfered in behalf of the rich, the powerful, the intelligent, the so-called great, or in behalf of any of their friends ... it would have been all right; and every man in this court would have deemed it an act worthy of reward rather than punishment."* ― **John Brown, [John Brown's last speech (November 2, 1859)](https://en.wikipedia.org/wiki/John_Brown's_last_speech)**

*"I, John Brown, am now quite certain that the crimes of this guilty land will never be purged away but with blood. I had, as I now think, vainly flattered myself that without very much bloodshed it might be done."* ― **John Brown, [John Brown's last words (December 2, 1859)](https://en.wikipedia.org/wiki/John_Brown_(abolitionist)#Last_words,_death_and_aftermath)**

---

*"The philosophers have only interpreted the world in various ways. The point however is to change it."* ― **Carl/Karl Marx**

---

**(May 28, 2023):** The following [free speech flags](https://en.wikipedia.org/wiki/Free_Speech_Flag) belong here because [reasons](https://dolphin-emu.org/blog/6T/) ([psychological reactance](https://en.wikipedia.org/wiki/Reactance_(psychology)) / [Streisand effect](https://en.wikipedia.org/wiki/Streisand_effect)) | [update](https://www.theverge.com/2023/6/1/23745772/valve-nintendo-dolphin-emulator-steam-emails) | [aftermath](https://dolphin-emu.org/blog/6U/):

<img alt="Free Speech Flag (Wii)" src="images/free-speech-flag-wii.png" width="480">

<img alt="Free Speech Flag (Wii U)" src="images/free-speech-flag-wii-u.png" width="480">

- Wii Common Key: **EBE42A225E8593E448D9C5457381AAF7** | [bin](keys/wii-common-key.bin) | [txt](keys/wii-common-key.txt)
- Wii U Common Key: **D7B00402659BA2ABD2CB0DB27FA2B656** | [bin](keys/wii-u-common-key.bin) | [txt](keys/wii-u-common-key.txt)

## 📜 License 📜

The contents of [this project (holo-PKGBUILD)](https://gitlab.com/evlaV/holo-PKGBUILD) are licensed under the [Mozilla Public License Version 2.0 (MPL 2.0)](LICENSE), and the underlying packages are licensed under their respective license(s) (see: respective package repository, directory, and/or PKGBUILD).

The following contents are [free, unencumbered, and released into the public domain](UNLICENSE) or (equivalently) licensed under the [BSD Zero Clause License (0-clause)](LICENSE.0BSD):

- [`images/free-speech-flag-wii.svg`](images/free-speech-flag-wii.svg) ([`images/free-speech-flag-wii.png`](images/free-speech-flag-wii.png))
- [`images/free-speech-flag-wii-u.svg`](images/free-speech-flag-wii-u.svg) ([`images/free-speech-flag-wii-u.png`](images/free-speech-flag-wii-u.png))
