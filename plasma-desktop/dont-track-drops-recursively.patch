From 75489b271abca899aa6e6da2a660aa2e268465ca Mon Sep 17 00:00:00 2001
From: Marco Martin <notmart@gmail.com>
Date: Tue, 26 Sep 2023 10:22:42 +0000
Subject: [PATCH] Don't add recursively every file copied to desktop

When a folder is droppen on the desktop, every file contained in the
folder and subfolder was added to the screen mapping, polluting the
entry a lot

this because KIO::CopyJob::copyingDone is emitted for every file that's
copied, not only the single folder

Add an update script that forgets all urls in screenmapping that begin with
desktop:/ but are it its subfolders rather than files directly in desktop
---
 .../desktop/plugins/folder/foldermodel.cpp    |   4 +
 .../folderview_fix_recursive_screenmapping.js | 109 ++++++++++++++++++
 2 files changed, 113 insertions(+)
 create mode 100644 desktoppackage/contents/updates/folderview_fix_recursive_screenmapping.js

diff --git a/containments/desktop/plugins/folder/foldermodel.cpp b/containments/desktop/plugins/folder/foldermodel.cpp
index 50ad3aea6b..5e5f9b6c60 100644
--- a/containments/desktop/plugins/folder/foldermodel.cpp
+++ b/containments/desktop/plugins/folder/foldermodel.cpp
@@ -1236,6 +1236,10 @@ void FolderModel::drop(QQuickItem *target, QObject *dropEvent, int row, bool sho
      */
     connect(dropJob, &KIO::DropJob::copyJobStarted, this, [this, dropPos, dropTargetUrl](KIO::CopyJob *copyJob) {
         auto map = [this, dropPos, dropTargetUrl](const QUrl &targetUrl) {
+            // KIO::CopyJob::copyingDone is emitted recursively, ignore everything not directly in the target folder
+            if ((dropTargetUrl.path() + QStringLiteral("/") + targetUrl.fileName()) != targetUrl.path()) {
+                return;
+            }
             m_dropTargetPositions.insert(targetUrl.fileName(), dropPos);
             m_dropTargetPositionsCleanup->start();
 
diff --git a/desktoppackage/contents/updates/folderview_fix_recursive_screenmapping.js b/desktoppackage/contents/updates/folderview_fix_recursive_screenmapping.js
new file mode 100644
index 0000000000..e8e600dbda
--- /dev/null
+++ b/desktoppackage/contents/updates/folderview_fix_recursive_screenmapping.js
@@ -0,0 +1,109 @@
+/*   vim:set foldmethod=marker:
+
+    SPDX-FileCopyrightText: 2023 Marco Martin <mart@kde.org>
+
+    SPDX-License-Identifier: GPL-2.0-or-later
+*/
+
+function filterDisabled(entries) {
+    let filteredEntries = [];
+
+    // 0 = screen, 1 = activity, 2 = how many entries, 3 = desktop entry
+    let state = 0;
+    let entriesForCurrentScreen = 0;
+
+    let currentScreen = -1;
+    let currentActivity = "";
+    let currentEntrtriesNumber = 0;
+    let currentEntry = 0;
+    let currentEntries = [];
+
+    for (let e of entries) {
+        switch (state) {
+        case 0: // Screen
+            currentScreen = e;
+            state = 1;
+            break;
+        case 1: // Activity
+            currentActivity = e;
+            state = 2;
+            break;
+        case 2: // Entries number
+            currentEntrtriesNumber = Number(e);
+            state = 3;
+            break;
+        case 3: // Desktop file
+            if (e.indexOf("desktop:/") !== 0) { // User has a folderview not in desktop:/
+                currentEntries.push(e);
+                currentEntry++;
+            } else {
+                let count = (e.match(/\//g) || []).length;
+                if (count == 1) {
+                    currentEntries.push(e);
+                    currentEntry++;
+                } else {
+                    currentEntrtriesNumber--;
+                }
+            }
+
+            if (currentEntry === currentEntrtriesNumber) {
+                state = 0;
+                if (currentEntries.length > 0) {
+                    filteredEntries = filteredEntries.concat([currentScreen, currentActivity, currentEntrtriesNumber]);
+                    filteredEntries = filteredEntries.concat(currentEntries);
+                    currentEntries = [];
+                }
+            }
+            break;
+        }
+
+    }
+    return filteredEntries;
+}
+
+function filterEnabled(entries) {
+    let filteredEntries = [];
+
+    // 0 = desktop entry, 1 = screen 2 = activity
+    let state = 0;
+    let shouldDrop = false; //true when this entry should be dropped
+
+    for (let e of entries) {
+        switch (state) {
+        case 0: // Desktop file
+            if (e.indexOf("desktop:/") !== 0) { // User has a folderview not in desktop:/
+                filteredEntries.push(e);
+                shouldDrop = false;
+            } else {
+                let count = (e.match(/\//g) || []).length;
+                if (count == 1) {
+                    filteredEntries.push(e);
+                    shouldDrop = false;
+                } else {
+                    shouldDrop = true;
+                }
+            }
+            break;
+        case 1: // Screen
+        case 2: // Activity
+            if (!shouldDrop) {
+                filteredEntries.push(e);
+            }
+        }
+        state = (state + 1) % 3;
+    }
+    return filteredEntries;
+}
+
+const config = ConfigFile('plasma-org.kde.plasma.desktop-appletsrc');
+config.group = 'ScreenMapping';
+
+let entries = config.readEntry("itemsOnDisabledScreens").split(",");
+let filteredEntries = filterDisabled(entries);
+
+config.writeEntry("itemsOnDisabledScreens", filteredEntries.join(","));
+
+entries = config.readEntry("screenMapping").split(",");
+filteredEntries = filterEnabled(entries);
+
+config.writeEntry("screenMapping", filteredEntries.join(","));
-- 
GitLab

